import pandas as pd
import tweet_query
import datetime
from sqlalchemy import create_engine, MetaData, Table, Column, Integer, String, Float, DateTime

truth_dict = {
    False: 'low',
    True: 'high'
}

def connect():
    """ Connect to local database"""
    return  create_engine('postgresql://postgres:roboctopus@localhost:5432/ousiometer')


def ambient_insert(self):
    engine = connect()

    metadata = MetaData()

    ambient_table = Table(f'{self.anchor.lower()}_res_{truth_dict[self.high_res]}', metadata,
                          Column('ngram', String),
                          Column('timestamp', DateTime),
                          Column('count', Integer),
                          Column('count_no_rt', Integer)
                          )

    metadata.create_all(engine)

    ins = ambient_table.insert()

    conn = engine.connect()

    for i, counter_i in enumerate(counters.counters):
        conn.execute(ins,
                     [{'ngram': key,
                       'timestamp': counters.dates[i].to_pydatetime(),
                       'count': value['count'],
                       'count_no_rt': value['count_no_rt']}
                      for key, value in counter_i.items()]
                     )
