import os
import sys
import datetime
import numpy as np
import matplotlib.pyplot as plt
from multiprocessing import Pool
import pandas as pd
import time
import glob
import scipy.stats
from tweet_query.tweet_db_query import assemble_ambient_ngrams
from tweet_query.counters import Counters
import matplotlib.dates as mdates
from tweet_query.measurements import *

def jsd_plot(counters, dates, count_type='count', ax=None):

    print('ok')

def entropy_plot(counters, dates, count_type='count', ax=None):
    entropy_array = []
    count_array = []
    norm_entropy = []
    for counter_i in counters:
        entropy = scipy.stats.entropy([word[count_type] for word in counter_i.values()], base=2.)
        count = sum([word[count_type] for word in counter_i.values()])
        entropy_array.append(entropy)
        norm_entropy.append(entropy/np.log2(count))
        count_array.append(count)

    ax[0].plot(dates[1:], entropy_array)
    ax[1].plot(dates[1:], norm_entropy)
    ax[2].plot(dates[1:], count_array, label=count_type)
    ax[3].semilogx(count_array, entropy_array,'o')

    return ax

def entropy_plot_wrapper(word='social', mid_date=None, b_delta=50, a_delta=150, pth=""):

    start_date = mid_date - datetime.timedelta(b_delta)
    end_date = mid_date + datetime.timedelta(a_delta)
    freq = 'D'  # weekly frequency
    dates = pd.date_range(start_date, end_date, freq=freq)

    start_time = time.time()

    counters = Counters(dates, word, pth="/home/data/ambient_data")
    counters.get(save=True)
    c = assemble_ambient_ngrams(word, dates, scheme=2, lang='en', case_sensitive=False)
    #c2 = assemble_ambient_ngrams(word, dates, scheme=2, lang='en', case_sensitive=False, high_res=True)
    total_time = time.time() - start_time
    print(total_time)

    f, ax = plt.subplots(4, 1, figsize=(10,10),gridspec_kw={'height_ratios':[1,1,1,3]})
    plt.suptitle(word, y=0.95)

    ax = entropy_plot(counters.counters, dates, count_type='count', ax=ax)
    ax = entropy_plot(counters.counters, dates, count_type='count_no_rt', ax=ax)
    #ax = entropy_plot(c2,dates, count_type='count', ax=ax)
    #ax = entropy_plot(c2,dates, count_type='count_no_rt', ax=ax)

    ylabels = ['Entropy', 'Entropy \n per \n log_word', 'Words']
    myFmt = mdates.DateFormatter('%b %d')
    locator = mdates.WeekdayLocator(byweekday=(0), interval=2)
    for i in range(3):
        ax[i].tick_params(labelrotation=0)
        ax[i].set_ylabel(ylabels[i])
        ax[i].xaxis.set_major_locator(locator)
        ax[i].xaxis.set_major_formatter(myFmt)
    ax[3].set_ylabel('Entropy')
    ax[3].set_xlabel('Words')
    #f.autofmt_xdate()
    ax[2].legend()
    plt.savefig(f'{pth}entropy_plot_{word}.png')
    plt.savefig(f'{pth}entropy_plot_{word}.pdf')
    plt.show()


def load_anomolous_words(fname):
    names = ['ngram', 'mean_freq', 'date']
    df = pd.read_csv(fname, sep='\t', names=names, parse_dates=['date'])
    return df


def main():
    fname = f"../data/anomalous_words_a_thresh_15000-b_thresh_20000_a_delta_150_b_delta50.txt"
    df = load_anomolous_words(fname)
    args = [(i,pd.to_datetime(j)) for (i,j) in zip(df['ngram'].values, df['date'].values)]
    print(args)
    #with Pool(5) as p:
    entropy_plot_wrapper("today's stats", datetime.datetime(2017,8,11))
    #    p.starmap(entropy_plot_wrapper, args)

    #for index, value in df.iterrows():
    #    print(value)
    #    entropy_plot_wrapper(value['ngram'])

if __name__ == "__main__":
    main()