from . import sentiment
from . import tweet_db_query
from . import measurements
from . import counters
from . import sentiment_plot
from . import counter
from . import regexr
from . import ambient_rd_figures
from . import ambient_embeddings
from . import utils

import logging

def logging_init():
    """ Initialize logging for query """
    logger = logging.getLogger('ambientQueryLogger')
    logger.setLevel(logging.DEBUG)
    fh = logging.FileHandler(filename='../../.logs/ambient_sentiment_query.log')
    fh.setLevel(logging.DEBUG)
    fh.setFormatter(logging.Formatter('%(asctime)s -- %(message)s'))
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    logger.addHandler(fh)
    logger.addHandler(ch)