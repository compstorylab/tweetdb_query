import sys
import argparse
import subprocess
import datetime
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sentence_transformers import SentenceTransformer

import cudf, cuml
from cuml.manifold import UMAP
from cuml.cluster import HDBSCAN

from bokeh.models import CustomJS, CrosshairTool
from bokeh.layouts import column
import bokeh
from bokeh.models.widgets import TextInput
from bokeh.models import ColorBar, LogColorMapper, LinearColorMapper, CategoricalColorMapper, CDSView, GroupFilter, \
    BooleanFilter
from bokeh.plotting import figure, output_file, show, ColumnDataSource
from bokeh.io import show
from bokeh.models import CustomJS, RadioButtonGroup, OpenURL, TapTool, Panel, Tabs, Select, TextInput, DateRangeSlider, \
    DateSlider
from bokeh.palettes import Viridis5, Category20
import itertools
from matplotlib.cm import ScalarMappable
from matplotlib.colors import Normalize, rgb2hex


def valid_date(s):
    try:
        return datetime.datetime.strptime(s, "%Y-%m-%d")
    except ValueError:
        msg = "Not a valid date: '{0}'.".format(s)
        raise argparse.ArgumentTypeError(msg)


def parse_args(args):
    parser = argparse.ArgumentParser(
        description="A command line interface for creating interactive ambient tweet embedding plots",
    )
    parser.add_argument(
        '-w', '--word',
        type=str,
        help='anchor to plot'
    )
    parser.add_argument(
        '-r', '--high_res',
        help="Flag to plot high res",
        action='store_true',
    )
    parser.add_argument(
        '-b', '--begin_date',
        help="beginning of date range -- formate YYYY-MM-DD",
        type=valid_date,
        default='2020-01-01',
    )
    parser.add_argument(
        '-c', '--copy',
        help="Flag to copy plot to w3 server",
        action='store_true',
    )
    parser.add_argument(
        '--location',
        type=str,
        default=None,
        help="Include a location feild, either state or city_state",
    )
    parser.add_argument(
        '-e', '--end_date',
        help="end of date range -- format YYYY-MM-DD",
        type=valid_date,
        default='2020-12-31'
    )
    parser.add_argument(
        '-s', '--sentiment',
        help="Flag to plot sentiment",
        action='store_true',
    )
    parser.add_argument(
        '-p', '--pth',
        help="path to load embedding data",
        default="/home/michael/projects/embeddings/data/"
    )
    parser.add_argument(
        '-l', '--labeled',
        help="Flag to plot labels",
        action='store_true',
    )
    parser.add_argument(
        '-f', '--fname',
        default=None,
        help='data to plot'
    )
    return parser.parse_args(args)


def load_data(fname):
    """ Load data"""
    df = pd.read_csv(fname, sep='\t', parse_dates=['tweet_created_at'], lineterminator='\n')
    return df


def filter_NaT(df):
    index = pd.notnull(df['tweet_created_at'])
    df2 = df.loc[index]
    return df2

def subset(df, max_tweets=40000):
    """ sample dataframe"""
    frac = min(max_tweets / df.shape[0], 1)
    return df.sample(frac=frac).reset_index(drop=True)

def embed_tweets(df, n_components=16, min_cluster_size=30, cluster_method='eom'):
    #model = SentenceTransformer('paraphrase-MiniLM-L6-v2')
    model = SentenceTransformer('all-mpnet-base-v2')

    #Our sentences we like to encode
    # filter QT


    #Sentences are encoded by calling model.encode()
    embeddings = model.encode(df.Text.values)

    umap_embeddings = UMAP(n_neighbors=15,
                           n_components=n_components,
                           min_dist=0).fit_transform(embeddings)

    cluster = HDBSCAN(min_cluster_size=min_cluster_size,
                              metric='euclidean',
                              cluster_selection_method=cluster_method).fit(umap_embeddings)

    umap_data = UMAP(n_neighbors=15, n_components=2, min_dist=0,
                          random_state=42).fit_transform(embeddings)
    print(umap_data)

    df[['x','y']] = umap_data
    df['labels'] = cluster.labels_

    return df

def umap_embedding(embeddings, tweets, fname, args, n_components=16, min_cluster_size=30, cluster_method='eom'):
    umap_embeddings = umap.UMAP(n_neighbors=15,
                                n_components=n_components, min_dist=0,
                                metric='cosine').fit_transform(embeddings)

    cluster = HDBSCAN(min_cluster_size=min_cluster_size,
                              metric='euclidean',
                              cluster_selection_method=cluster_method).fit(umap_embeddings)

    umap_data = umap.UMAP(densmap=True, n_neighbors=15, n_components=2, min_dist=0, metric='cosine',random_state=42).fit_transform(embeddings)
    result = pd.DataFrame(umap_data, columns=['x', 'y'])



    return result

def plot_embedding(df, outfile, args):
    """ Create the interactive html plot with date slider """
    output_file(outfile)
    plot_type = "month_colors"

    df = subset(df)
    df = embed_tweets(df)

    df['time'] = [row.strftime("%m/%d/%Y, %H:%M:%S") if pd.notnull(row) else None for i, row in
                  df['tweet_created_at'].iteritems()]

    color_fields = []  # ToDo: date,
    color_mappers = []

    # Colors for hdbscan clusters
    c = bokeh.palettes.Category20[20]
    colors = [c[int(i) % 10] if int(i) != -1 else '#BDBDBD' for i in df.labels]
    df['hdbscan_clusters'] = colors

    colors = [c[int(i) % 10] if int(i) != -1 else '#BDBDBD' for i, lang in enumerate(df.lang)]
    df['lang_colors'] = colors

    if args.sentiment:
        color_fields = ['happs', 'month']
        color_mappers.append(
            LinearColorMapper(palette="Cividis256",
                              low=min(df['happs']) + 1,
                              high=max(df['happs']) - 1,
                              )
        )


    # Month colors
    color_fields.append('month')

    df['month'] = [row.month for i, row in df['tweet_created_at'].iteritems()]
    color_mappers.append(
        LinearColorMapper(palette="Viridis256",
                          low=min(df['month']),
                          high=max(df['month'])
                          )
    )
    if args.location:
        state_locs_df = pd.read_csv('../pkg_data/state_centers.csv')
        if args.location == 'state':
            state_loc = "LONGITUDE"
            # state_loc = "LATITUDE"

            state_locs_dict = {row['state']: row[state_loc]
                               for i, row in state_locs_df[['state', state_loc]].iterrows()}
            df[state_loc] = [state_locs_dict[state] if state in state_locs_dict else None for state in
                             df['state'].values]
            print(df[state_loc])
            color_fields.append(str(state_loc))

        else:
            raise NotImplementedError("Get city locations and save to pkg_data")

        # df[args.location] = [row for i, row in df[args.location].iteritems()]
        if state_loc == 'LONGITUDE':
            min_coord = max(-125, min(df[state_loc]))
            max_coord = min(-70, max(df[state_loc]))
        elif state_loc == 'LATITUDE':
            min_coord = max(45, min(df[state_loc]))
            max_coord = min(30, max(df[state_loc]))
        color_mappers.append(
            LinearColorMapper(palette='Viridis256',
                              low=min(df[state_loc]),
                              high=max(df[state_loc])
                              )
        )

    # adding Labeled data
    c1 = bokeh.palettes.Category10[5]
    label_dict = {d: i for i, d in enumerate(df.Label.unique())}
    colors = [c1[label_dict[i] % 5] if int(label_dict[i]) != -1 else '#BDBDBD' for i in df.Label]
    df['Label_color'] = colors


    source = ColumnDataSource(df)

    TOOLTIPS = """
        <div>
            <div>
                <span style="font-size: 10px;">@time UTC</span>
            </div>
             <div>
                <span style="font-size: 10px;">@id </span>
            </div>
            <div>
                <span style="font-size: 10px;">Language: @lang -- Verb: @verb</span>
            </div>
            <div>
                <span style="font-size: 10px;">State: @state </span>
            </div>
            <div>
                <span style="font-size: 12px;">Tweet Text:</span>
                <span style="font-size: 12px; font-weight: bold;">@Text</span>
            </div>
        </div>
    """
    figure_args = {
        'plot_width': 700, 'plot_height': 700,
        'tooltips': TOOLTIPS,
        'tools': "pan,wheel_zoom,box_zoom,reset,tap",
        'toolbar_location': "below",
        'title': f'Embedded Ambient Tweets -- Anchor: "{args.word}"',
    }

    tabs_list = []
    panels = []

    tabs_list.append(figure(plot_width=700, plot_height=700,
                            tooltips=TOOLTIPS,
                            tools="pan,wheel_zoom,box_zoom,reset,tap",
                            toolbar_location="below",
                            title=f'Embedded Ambient Tweets -- Anchor: "{args.word}"',
                            ))

    size = 4

    # date slider
    init_value = (df['tweet_created_at'].min(), df['tweet_created_at'].max())
    slider = DateSlider(start=init_value[0], end=init_value[1], value=init_value[0], step=1)

    date_filter = BooleanFilter(booleans=[True] * df.shape[0])

    slider.js_on_change('value', CustomJS(args=dict(f=date_filter, ds=source),
                                          code="""\
                                              const start = cb_obj.value;
                                              const end = start + 1000*3600*24*30*2
                                              f.booleans = Array.from(ds.data['tweet_created_at']).map(d => (d >= start && d <= end));
                                              // Needed because of https://github.com/bokeh/bokeh/issues/7273
                                              ds.change.emit();
                                          """))
    clusters = CDSView(source=source,
                       filters=[BooleanFilter([False if y == -1 else True for y in df.labels]), date_filter])
    outliers = CDSView(source=source,
                       filters=[BooleanFilter([True if y == -1 else False for y in df.labels]), date_filter])

    tabs_list[0].circle('x', 'y', size=size, source=source,
                        fill_color='#BDBDBD', fill_alpha=0.3,
                        line_color='#000000', line_alpha=0.1, view=outliers)
    tabs_list[0].circle('x', 'y', size=size, source=source,
                        fill_color="hdbscan_clusters", fill_alpha=0.7,
                        line_color='#000000', line_alpha=0.1, view=clusters)
    panels.append(Panel(child=column(tabs_list[0], slider), title='hdbscan clusters'))

    for i, color_field in enumerate(color_fields):
        tabs_list.append(
            figure(x_range=tabs_list[0].x_range,
                   y_range=tabs_list[0].y_range,
                   **figure_args)
        )

        tabs_list[i + 1].circle('x', 'y', size=5, source=source,
                                fill_color={'field': color_field, 'transform': color_mappers[i]},
                                fill_alpha=0.7, line_color=None)

        color_bar = ColorBar(color_mapper=color_mappers[i], label_standoff=12)
        tabs_list[i + 1].add_layout(color_bar, 'right')

        panels.append(Panel(child=tabs_list[i + 1], title=color_field))

    # start add Labeled data tab
    tabs_list.append(
        figure(x_range=tabs_list[0].x_range,
               y_range=tabs_list[0].y_range,
               **figure_args)
    )

    clusters1 = CDSView(source=source,
                        filters=[BooleanFilter([False if y == '#BDBDBD' else True for y in df.Label_color]),
                                 date_filter])
    outliers1 = CDSView(source=source,
                        filters=[BooleanFilter([True if y == '#BDBDBD' else False for y in df.Label_color]),
                                 date_filter])

    tabs_list[-1].circle('x', 'y', size=size, source=source,
                         fill_color='#BDBDBD', fill_alpha=0.3,
                         line_color='#000000', line_alpha=0.1, view=outliers1)
    tabs_list[-1].circle('x', 'y', size=5, source=source,
                         fill_color="Label_color", fill_alpha=0.7,
                         line_color='#000000', line_alpha=0.1, view=clusters1)
    panels.append(Panel(child=column(tabs_list[-1], slider), title='Labels'))

    # click to open link to tweet
    for p in tabs_list:
        url = "@link"
        taptool = p.select(type=TapTool)
        taptool.callback = OpenURL(url=url)

    print(tabs_list)
    print(len(tabs_list))
    show(Tabs(tabs=panels))
    return


def main(args=None):
    if args is None:
        args = sys.argv[1:]
        args = parse_args(args)


    fname = args.fname
    # try:
    df = load_data(fname)
    # except:
    #    print('on no')
    #    return
    if df['Label'].isnull().values.all():
        print(df["Label"])
        df = df.drop(['Label'], axis=1)


    print(df.head())
    print(df.tail())

    outfile = f'/home/michael/projects/embeddings/hoverplots/{args.word}_{args.high_res}_{args.location}.html'
    outfile = outfile.replace('#', '!')
    plot_embedding(df, outfile, args)

    if args.copy:
        subprocess.run(['scp', outfile, 'w3:www-root/tweet_embeddings/hoverplots/.'])


if __name__ == "__main__":
    main()