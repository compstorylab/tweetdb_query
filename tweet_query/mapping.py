import geopandas as gpd
import pandas as pd
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
import argparse

def parse_args(args):
    parser = argparse.ArgumentParser(
        description="A command line interface for ambient n-gram timeseries and zipf distributions",
        formatter_class=argparse.RawDescriptionHelpFormatter,
        epilog=textwrap.dedent('''\
         Remember to port forward if you're not on campus:
             ssh -N -f -L localhost:27016:localhost:27016 ti-89

         N-gram data format:
             ./tweet_db_query.py -w "coronavirus" -s -b 2020-01-01 -e 2020-06-15
             '''))
    parser.add_argument(
        '-w', '--word',
        type=str,
        help='ngram to plot'
    )

def main():
    attention = pd.read_csv('../data/attention_m.csv')
    usa = gpd.read_file('zip:///home/michael/Downloads/cb_2018_us_state_5m.zip')

    usa = usa.to_crs("EPSG:2163")
    usa = usa.merge(attention, left_on='STUSPS', right_on="_id")
    # Plots
    fig, ax = plt.subplots(1, 1)


    usa.plot(column='freq', ax=ax, legend=True,
             legend_kwds={'label': "Usage Rate: Covid - 2021 ",'orientation': "horizontal"});
    print(attention)
    ax.set_axis_off();
    plt.savefig('../figures/us_attention_map_2021.png', dpi=500)
    plt.savefig('../figures/us_attention_map_2021.pdf')

    plt.show()
    print(usa.columns)
    print([i for i in usa.head()])
    print(usa.head())

    return usa

if __name__ == "__main__":
    usa = main()