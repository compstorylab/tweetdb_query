import sys
from pathlib import Path
file = Path(__file__).resolve()
parent, root = file.parent, file.parents[1]
sys.path.append(str(root))

try:
    sys.path.remove(str(parent))
except ValueError:
    pass

try:
    import importlib.resources as pkg_resources
except ImportError:
    import importlib_resources as pkg_resources

import pandas as pd
import argparse
import textwrap
import numpy as np
import ujson
from pprint import pprint
import logging
from datetime import datetime
import time
import pytz
from dateutil.relativedelta import relativedelta
import pymongo
from pymongo import MongoClient
from pymongo.errors import ServerSelectionTimeoutError
from multiprocessing import Pool
import subprocess

from tweet_query.regexr import get_ngrams_parser, remove_whitespaces, ngrams
from tweet_query import counter
from tweet_query.tweet_db_query import get_credentials, tweet_connect
import matplotlib.pyplot as plt
from tqdm.contrib.concurrent import process_map
from typing import Optional


def count_ambient_tweets(query,
                         collection='drip_hose',
                         database='tweets'):
    """Function to count number of matching tweets
    :param word: ambient anchor word or n-gram
    :param dates: a pandas DatetimeIndex array
    :param lang: language to query
    :param collection: mongo collection to query
    :return: a counter object of n-grams

    Read https://docs.mongodb.com/manual/reference/operator/query/text/#mongodb-query-op.-text
    for more on querying with text indexes
    """

    username, pwd = get_credentials()
    client = tweet_connect(username, pwd, collection=collection, database=database)
    db = client[database]
    tweets = db[collection]
    try:
        delta = dates[1] - dates[0]
    except IndexError:
        print(
            "IndexError: increment date range by one unit to avoid length 1 date list \n End ranges of months missing data if freq > 1 day")
        return []
    print("Remove this function.")

    #query = {'$text': {'$search': f"\" {word} \"",
    # '$caseSensitive': case_sensitive},
    # 'tweet_created_at':
    #     {'$gte': dates[0],
    #      '$lt': dates[-1]},
    # 'fastText_lang': lang}
    return tweets.find(query).count()


def matching(anchor=None, verified: Optional[dict] = None, dates=False):
    """ Compose a matching stage for a pymongo aggregation, return as a dict"""
    match = {'$match': {}}

    if type(anchor) is str:
        match["$match"]['$text'] = {'$search': f"\" {anchor} \""}
    elif type(anchor) is list and len(anchor) > 1:
        match["$match"]['$text'] = {'$search': ' '.join(f"\" {anchor} \"")}
    elif type(anchor) is list and len(anchor) == 1:
        match["$match"]['$text'] = {'$search': f"\" {anchor[0]} \""}
    if dates:
        match['$match']['tweet_created_at'] = {'$gte': dates[0].to_pydatetime(),
                                                '$lt': dates[-1].to_pydatetime(),
                                               }
    if verified:
        match["$match"]['actor.verified'] = verified['verified']

    #pprint(match)
    return match

def location_aggregation2(args):
    """ Script to aggregate number of tweets matching a condiction by state"""
    anchor, collection, database, location, verified, dates = args

    username, pwd = get_credentials()
    client = tweet_connect(username, pwd, collection=collection, database=database, port=27015, hostname='serverus.cems.uvm.edu')
    db = client[database]
    tweets = db[collection]

    match = matching(anchor, verified=verified, dates=dates)
    match_full = matching(verified=verified, dates=dates)
    group = {'$group':
                {'_id': location,
                 'num_tweets': {'$sum':1}}
            }
    sort = {'$sort':
                {location[1:]: 1,
                 'tweet_created_at': 1},
            }

    start_time = time.time()
    pipeline1 = [match,
                 group,
                ]
    anchored_df = pd.DataFrame(list(tweets.aggregate(pipeline1, allowDiskUse=True)), columns=['_id', 'num_tweets'])
    #print(f"Finished anchor match in {time.time()-start_time:.2f}s")

    anchored_df.index = anchored_df['_id']
    anchored_df = anchored_df[['num_tweets']]
    #print(anchored_df)

    start_time = time.time()
    pipeline2 = [match_full,
                 group,
                ]
    if anchored_df.empty:
        total_df = pd.DataFrame([],  columns=['num_tweets'])
        return total_df

    else:
        total_df = pd.DataFrame(list(tweets.aggregate(pipeline2, allowDiskUse=True)))
        #print(f"Finished full match in {time.time() - start_time:.2f}s")

        total_df.index = total_df['_id']
        total_df = total_df[['num_tweets']]

    #print(total_df)

    df = anchored_df.merge(total_df, on='_id', how='right',suffixes=('_match','_total'))
    df['num_tweets_match'] = df['num_tweets_match'].fillna(0)
    df['freq'] = df['num_tweets_match']/df['num_tweets_total']
    if dates:
        df['time'] = dates[0]
    return df


def location_aggregation(anchor, collection, database, location='$state', verified: Optional[dict] = None, dates=False, lang=None):
    """ Script to aggregate number of tweets matching a condiction by state"""
    username, pwd = get_credentials()
    client = tweet_connect(username, pwd, collection=collection, database=database, port=27015, hostname='serverus.cems.uvm.edu')
    db = client[database]
    tweets = db[collection]
    print(f'Connecting to database ...')

    match = matching(anchor, verified=verified, dates=dates)
    match_full = matching(verified=verified,dates=dates)
    group = {'$group':
                {'_id': location,
                 'num_tweets': {'$sum':1}}
            }
    sort = {'$sort':
                {location[1:]: 1,
                 'tweet_created_at': 1},
            }

    start_time = time.time()
    pipeline1 = [match,
                 group,
                ]
    anchored_df = pd.DataFrame(list(tweets.aggregate(pipeline1, allowDiskUse=True)), columns=['_id', 'num_tweets'])
    anchored_df.index = anchored_df['_id']
    anchored_df = anchored_df[['num_tweets']]
    print(f"anchored_results finished in {time.time()-start_time:.2f}s")
    #print(anchored_df)
    start_time = time.time()
    pipeline2 = [match_full,
                 sort,
                 group,
                ]
    if anchored_df.empty:
        total_df = pd.DataFrame([],  columns=['num_tweets'])
        return total_df

    else:
        total_df = pd.DataFrame(list(tweets.aggregate(pipeline2, allowDiskUse=True)))
        total_df.index = total_df['_id']
        total_df = total_df[['num_tweets']]

    print(f"total_results finished in {time.time()-start_time:.2f}s")
    #print(total_df)

    df = anchored_df.merge(total_df, on='_id', how='right',suffixes=('_match','_total'))
    df['num_tweets_match'] = df['num_tweets_match'].fillna(0)
    df['freq'] = df['num_tweets_match']/df['num_tweets_total']
    if dates:
        df['time'] = dates[0]
    return df

def plot_map(df):
    pass

def location_aggregation_timeseries(anchor, collection, database, dates, location='$state', verified=False, lang='en'):
    """ Script to aggregate number of tweets matching a condition by state"""
    username, pwd = get_credentials()
    client = tweet_connect(username, pwd, collection=collection, database=database)
    db = client[database]
    tweets = db[collection]
    print('Connecting to database...')

    if verified:
        match = {"$match":
                    {'$text':
                         {'$search': f"\" {anchor} \""},
                     "actor.verified": True,
                     location[1:]: {
                         '$exists': True
                        }
                    },
                 }
    else:
        match_1 = {"$match":
                 {'$text': {'$search': f"\" {anchor} \""},
                  location[1:]: {
                      '$exists': True
                    }
                 }
                }
    """group = {'$group':
                {'_id': location,
                 'num_tweets': {'$sum':1}}
            }"""
    match_2 = {"$match": {
                    location[1:]: {
                        '$exists': True
                    }
                    }
               }
    sort = {'$sort':
                {'tweet_created_at': 1}
            }
    bucket = {'$bucket': {
                  'groupBy': "$tweet_created_at",
                  'boundaries': [i.to_pydatetime() for i in dates],
                  'default': np.nan,
                  'output': {
                      'count': {
                          '$sum': 1
                        },
                        'loc': {
                            '$push': '$state'
                        }
                    }
                }
             }
    unwind = {
              '$unwind': {
                  'path': '$loc'
                  }
             }
    group = {
        '$group': {
            '_id': {
                'loc': '$loc',
                'time': '$_id'
            },
            'num_tweets': {
                '$sum': 1
            }
        }
    }
    project = {
        '$project' : {
            '_id': 0,
            'loc': '$_id.loc',
            'time': '$_id.time',
            'num_tweets': '$count'
        }
    }

    start_time = time.time()
    pipeline1 = [match_1,
                 bucket,
                 unwind,
                 group,
                ]
    anchored_results = list(tweets.aggregate(pipeline1))
    anchored_index = pd.MultiIndex.from_tuples([(i['_id']['loc'],i['_id']['time']) for i in anchored_results], names=['loc', 'time'])
    anchored_df = pd.DataFrame([i['num_tweets'] for i in anchored_results], index=anchored_index, columns=['num_tweets_x'])

    print(f"anchored_results finished in {time.time()-start_time:.2f}s")
    pprint(anchored_df)

    # stages:
    #   * match
    #   * bucket
    #   * $unwind
    #   * group
    #
    #   match - returns document fields ['tweet_created_at']
    #   bucket -

    start_time = time.time()
    pipeline2 = [match_2,
                 sort,
                 bucket,
                 unwind,
                 group,
                ]
    total_results = list(tweets.aggregate(pipeline2, allowDiskUse=True))

    total_index = pd.MultiIndex.from_tuples([(i['_id']['loc'], i['_id']['time']) for i in total_results], names=['loc', 'time'])
    total_df = pd.DataFrame([i['num_tweets'] for i in total_results], index=total_index, columns=['num_tweets_y'])
    #total_df = total_df[]
    print(f"total_results finished in {time.time()-start_time:.2f}s")
    pprint(total_df)
    # merge dfs
    df = anchored_df.merge( total_df, how='outer', left_index=True, right_on=['loc', 'time'])
    df['num_tweets_x'] = df['num_tweets_x'].fillna(0)
    df['freq'] = df['num_tweets_x']/df['num_tweets_y']
    return df

def explain(anchor, collection, database, location='$state', verified=False):


    username, pwd = get_credentials()
    client = tweet_connect(username, pwd, collection=collection, database=database)
    db = client[database]
    tweets = db[collection]
    print('Connecting to database...')

    match = {"$match":
                 {'$text': {'$search': f"\" {anchor} \""}}
             }
    group = {'$group':
                 {'_id': location,
                  'num_tweets': {'$sum': 1}}
             }
    sort = {'$sort':
                {location[1:]: 1}
            }

    start_time = time.time()
    pipeline2 = [
                 sort,
                 group,
                ]

    pprint(db.command('aggregate', collection, pipeline=pipeline2, explain=True))
    #total_results = (tweets.explain().aggregate(pipeline2))
    print(f"total_results finished in {time.time() - start_time:.2f}s")

"""
def main(): 
    collection = '2020-04'
    database = 'tweets_segmented'
    #anchor = 'covid'
    #database = 'tweet_segmented_location'
    #collection = 'test'

    anchor = ['covid', 'covid-19', 'coronavirus', 'virus']

    start_date = datetime(2020,1,1)
    end_date = datetime(2020,2,1)
    dates = pd.date_range(start_date, end_date, freq='W')
    print(dates)


    df = pd.DataFrame()
    for i, date in enumerate(dates[:-1]):
        df_i = location_aggregation(anchor,
                      collection=collection,
                      database=database,
                      location='$state',
                      dates=(dates[i], dates[i+1])
                      )
        if df_i.empty:
            df[dates[i]] = 0
        else:
            df[dates[i]] = df_i['freq']

    return df
"""

def test():
    """grab attention timeseries by state"""
    database = 'tweet_segmented_location'
    collection = '2020-01'
    anchor = ["Nuclear"]
    verified = None
    loc_type = '$state'

    start_date = datetime(2020, 1, 1)
    end_date = datetime(2020, 1, 3)
    dates = pd.date_range(start_date, end_date, freq='D')
    i=0

    args = (anchor,
             collection,
             database,
             loc_type,
             verified,
             (dates[i],dates[i+1]))
    df = location_aggregation2(args)
    print(df)
    return df

def multiprocess_location_timeseries(dates=None):
    """ parallelize the attention timeseries by state"""
    database = 'tweet_segmented_location'
    #anchor = ['covid', 'covid-19', 'coronavirus', 'virus']
    #anchor = ['mask', 'distancing', 'lockdown', 'quanantine']
    # ToDo: "Heart Disease"
    # Done: Suicide , "Heart Disease", "stroke", ('Flu', "Pneumonia"), "Diabetes"
    anchor = ["Alcohol"]
    anchor = ['COPD', "Emphysema", "Bronchitis", "Asthma"]
    anchor = ['Speeding']
    anchor = ["Alzheimer's"]
    anchor = ["Guns"]
    anchor = ["Dui"]
    anchor = ["Homelessness"]
    # anchor = ['Drunk Driving'}
    # verified = {'verified': True}
    print(anchor)
    verified = None

    loc_type = '$state'
    if not dates:
        start_date = datetime(2016, 1, 1)
        end_date = datetime(2022, 3, 10)
        dates = pd.date_range(start_date, end_date, freq='D')

    month_indexes = []
    collections = []
    iterator = [(i.year, i.month) for i in dates]

    for (year, month) in sorted(iterator):
        collections.append(f"{year}-{month:02}")
        month_indexes.append(
            pd.DatetimeIndex(
                list(filter(lambda x: ((x.month == month) & (x.year == year)) |
                                      ((((x.year == year) & (x.month == month + 1)) | (
                                                  (x.year == year + 1) & (x.month == 1) & (month == 12)))
                                       & (x.day == 1) &
                                       ((x.hour == 0) | (x.hour == 5) | (x.hour == 4))
                                       & (x.minute == 0) &
                                       (x.second == 0)),
                            dates))
            )
        )

    args = [(anchor,
             collection,
             database,
             loc_type,
             verified,
             (dates[i],dates[i+1]),
             ) for i, (collection, subset_dates, dates_i) in enumerate(zip(collections, month_indexes, dates[:-1]))]

    #with Pool(30) as p:
    #    dfs = p.starmap(location_aggregation, args)
    dfs = process_map(location_aggregation2, args, max_workers=20)
    print(dfs)
        
    df = pd.concat(dfs, keys=dates)

    if type(anchor) == str:
        anchor = [anchor]
    fname = f"/Users/michael/projects/location_attention/anchors_{'-'.join(anchor)}_" \
            f"{start_date.strftime('%Y-%m-%d')}" \
            f"_{end_date.strftime('%Y-%m-%d')}_{loc_type}_verified_{verified}.tsv"
    df.to_csv(fname, sep='\t')

    return df

if __name__ == "__main__":
    df = multiprocess_location_timeseries()
