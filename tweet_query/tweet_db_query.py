import sys
from pathlib import Path
file = Path(__file__).resolve()
parent, root = file.parent, file.parents[1]
sys.path.append(str(root))

try:
    sys.path.remove(str(parent))
except ValueError:
    pass

try:
    import importlib.resources as pkg_resources
except ImportError:
    import importlib_resources as pkg_resources

import pandas as pd
import argparse
import textwrap
import numpy as np
import ujson
from pprint import pprint
import logging
import datetime
import time
import pytz
from dateutil.relativedelta import relativedelta
import pymongo
from pymongo import MongoClient
from pymongo.errors import ServerSelectionTimeoutError
from multiprocessing import Pool

from tweet_query.regexr import get_ngrams_parser, remove_whitespaces, ngrams
from tweet_query import counter


def get_credentials():
    """
    # ToDo: add docs
    """
    with pkg_resources.open_binary("pkg_data", 'client.json') as f:
        credentials = ujson.load(f)
        
    return credentials['username'], credentials['pwd']

def db_selection(day, high_res=True):
    """ Function to select correct database with segmented tweet collections
    :param day: datetime object
    :param high_res: Boolean flag to use ten percent collection or one_percent
    :return: db, collection - strings for tweet_connect function
    """
    # todo: adapt assemble_ambient_ngrams to use this
    db_dict = {True: '_ten_percent',
               False: ''}

    if day.year < 2020:
        db = 'tweets'
        collection = 'drip_hose'
    else:
        db = f'tweets_segmented{db_dict[high_res]}'
        collection = f"{day.year}-{day.month:02}"

    return db, collection


def tweets_per_day(day, high_res=False):
    """ Function to count tweets on a given day in the database
    :param day: datetime object
    :param high_res: Boolean flag to use ten percent collection or one_percent
    :return: db, collection - strings for tweet_connect function
    """
    db, collection = db_selection(day, high_res)
    username, pwd = get_credentials()
    client = tweet_connect(username, pwd, collection=collection, database=db)
    db = client[db]
    tweets = db[collection]
    query = {'tweet_created_at': {"$gte": day, "$lt": day + relativedelta(days=+1)}}

    count = tweets.count_documents(query)
    client.close()
    return count

def test_tweets(n=10):
    """ Return a list of n test tweets
    :param n: number of tweets to return
    :return: list of tweet objects"""
    usr, pwd = get_credentials()
    tweets = tweet_connect(usr, pwd)
    return [i for i in tweets.aggregate([{'$sample': {'size': n}}])]


def tweet_connect(username,
                  pwd,
                  database='tweets',
                  collection='drip_hose',
                  hostname='hydra.uvm.edu',
                  port='27016',
                  ):
    """ Return pymongo database collection object. Set to connect to localhost on port 27016
    :param username:
    :param pwd:
    :param database:
    :param collection:
    :return: pymongo collection object
    """
    username, pwd = get_credentials()
    try:
        client = MongoClient(
                            'mongodb://%s:%s@localhost:%s' % (username, pwd, port),
                            serverSelectionTimeoutMS=60000,
                             )
        client.server_info()

    except ServerSelectionTimeoutError:
        client = MongoClient(
                            'mongodb://%s:%s@%s:%s' % (username, pwd, hostname, port),
                            serverSelectionTimeoutMS=60000,
                             )
    return client



def process_tweet_text(text, rt_flag, ngrams_parser, ngrams, scheme=1):
    """ return counter objects for a tweet text field"""
    t = remove_whitespaces(text)
    tweet_ngrams = ngrams(t, parser=ngrams_parser, n=scheme)
    if tweet_ngrams is None:
        return counter.Storyon({})

    if rt_flag == 'retweet':
        tweet_ngrams = counter.NgramCounter(
            {
                k: counter.Storyon(count=v, count_no_rt=0) for k, v in tweet_ngrams.items()
            }
        )
    else:
        tweet_ngrams = counter.NgramCounter(
            {
                k: counter.Storyon(count=v, count_no_rt=v) for k, v in tweet_ngrams.items()
            }
        )
    return tweet_ngrams


def parse_ngrams_tweet(tweet, ngrams_parser, scheme=1, lowered=True):
    # to do: currently broken -- rewrite parsing to send "retweet" if retweet
    """ Classify a tweet's retweet type and parse ngrams
    :param t: tweet object
    :param ngrams_parser: regex parser object
    :param scheme: n in ngrams {1, 2, 3, ...}
    """
    ngrams_counter = counter.NgramCounter({})

    flags = ['pure', 'retweet']
    text_types = ['pure_text', 'rt_text']

    for i, field in enumerate(text_types):
        try:
            text = tweet[field]
            if lowered:
                text = text.lower()

            ngrams_counter += process_tweet_text(text, flags[i], ngrams_parser, ngrams, scheme)
        except KeyError:
            pass

    return ngrams_counter

def parse_df_tweet(tweet, ngrams_parser, scheme=1, lowered=True):
    # to do: currently broken -- rewrite parsing to send "retweet" if retweet
    """ Classify a tweet's retweet type and parse ngrams
    :param t: tweet object
    :param ngrams_parser: regex parser object
    :param scheme: n in ngrams {1, 2, 3, ...}
    """
    ngrams_counter = counter.NgramCounter({})

    flags = ['pure',]
    text_types = ['Text', ]

    for i, field in enumerate(text_types):
        try:
            text = tweet[field]
            if lowered:
                text = text.lower()

            ngrams_counter += process_tweet_text(text, flags[i], ngrams_parser, ngrams, scheme)
        except KeyError:
            pass

    return ngrams_counter


def ambient_ngrams_dataframe(word, dates, scheme=1, count_type='count', collection='drip_hose', lang='en',
                             database='tweets'):
    """runner to count ambient ngrams within  date range
    :param word: target word for ambient measurement
    :param dates: a pandas date array
    :param cores: number of cores
    :param count_type:
    :return: pandas dataframe of n-gram counts, rank, and frequency
    """

    counters = get_ambient_ngrams(word, dates, scheme=scheme, collection=collection, lang=lang, database=database)

    ngrams_counter = np.sum(counters, initial=counter.NgramCounter({}))

    sorted_grams = sorted(ngrams_counter.items(), key=lambda i: i[1][count_type], reverse=True)
    total_count = sum([i[count_type] for i in ngrams_counter.values()])

    print(f"Total Tweets for {word}: ", total_count)
    df = pd.DataFrame(
        [[x[0], x[1][count_type], x[1][count_type] / total_count if total_count > 0 else 0] for i, x in
         enumerate(sorted_grams)],
        columns=(f'{str(scheme)}gram', count_type, 'freq'))
    df['rank'] = df['freq'].rank(ascending=False)
    df.index = df[f'{str(scheme)}gram']
    return df[[count_type, 'rank', 'freq']]


def ambient_ngrams_dataframe_from_counter(ngrams_counter, word, scheme=1, count_type='count'):
    """ Converts a counter to dataframe
    :param ngrams_counter: ngram counter to convert to dataframe
    :param dates: a pandas date array
    :param cores: number of cores
    :param count_type:
    :return: pandas dataframe of n-gram counts, rank, and frequency
    """

    sorted_grams = sorted(ngrams_counter.items(), key=lambda i: i[1][count_type], reverse=True)
    total_count = sum([i[count_type] for i in ngrams_counter.values()])

    print(f"Total n-grams for {word}: ", total_count)
    df = pd.DataFrame(
        [[x[0], x[1][count_type], x[1][count_type] / total_count if total_count > 0 else 0] for i, x in
         enumerate(sorted_grams)],
        columns=(f'{str(scheme)}gram', count_type, 'freq'))
    df['rank'] = df['freq'].rank(ascending=False)
    df.index = df[f'{str(scheme)}gram']
    return df[[count_type, 'rank', 'freq']]


def assemble_ambient_ngrams(word, dates, scheme=1, lang='en', high_res=False, case_sensitive=False, query=None):
    """Wrapper function to combine data from date segmented tweet collections

    Args:
        word: ambient anchor word
        dates: a pandas date array
        scheme: length of n-gram to parse
        lang: language to query
        high_res: flag to use ten percent sample for 2020 instead of one percent
        case_sensitive: Bool to make lowe when parsing

    Returns:
        counters: a list of counter objects of n-gram counts
    """
    db = 'tweets_segmented'
    if high_res:
        db = 'tweets_segmented_ten_percent'
    dates = dates.tz_convert('UTC')
    # todo: multiprocess the parse
    freq = dates.freq
    # assert pd.tseries.offsets.Day() >= freq, "dates should have a frequency of one day or less"

    # first date where we have monthly collections
    switch_date1 = datetime.datetime(2019, 9, 1, tzinfo=pytz.timezone('UTC'))
    switch_date2 = datetime.datetime(2020, 1, 1, tzinfo=pytz.timezone('UTC'))
    switch_dates = {
        True: switch_date1,
        False: switch_date2,
        'Full': switch_date1
    }

    if dates[-1] <= switch_dates[high_res]:
        counters = get_ambient_ngrams(word=word,
                                      dates=dates,
                                      scheme=scheme,
                                      lang=lang,
                                      case_sensitive=case_sensitive,
                                      collection='drip_hose',
                                      database='tweets',
                                      query=query,
                                      )

        if high_res:
            print('CAUTION!')
            print("Misleading shift in resolution!!!")
            print("Best to shift to 'highres=False' for pre 2020 figures")
            raise IndexError
    else:
        if dates[0] < switch_dates[high_res]:
            pre_2020_dates = pd.DatetimeIndex(
                list(filter(lambda x: x <= switch_dates[high_res], dates)),
                ) # removed freq=freq
            counters = get_ambient_ngrams(word=word,
                                          dates=pre_2020_dates,
                                          scheme=scheme,
                                          lang=lang,
                                          case_sensitive=case_sensitive,
                                          collection='drip_hose',
                                          database='tweets',
                                          query=query,)
        else:
            counters = []
        post_2020_dates = pd.DatetimeIndex(
            list(filter(lambda x: x >= switch_dates[high_res], dates)),
            ) # removed freq=freq
        #print('post_2020', post_2020_dates)
        # create post 2020 month level slices
        month_indexes = []
        collections = []
        iterator = {(i.year, i.month) for i in post_2020_dates}

        for (year, month) in sorted(iterator):
            collections.append(f"{year}-{month:02}")
            month_indexes.append(
                pd.DatetimeIndex(
                    list(filter(lambda x: ((x.month == month) & (x.year == year)) |
                         ((((x.year == year) & (x.month == month + 1)) | ((x.year == year + 1) & (x.month == 1) & (month == 12)))
                            & (x.day == 1) &
                            ((x.hour == 0) | (x.hour == 5)| (x.hour == 4))
                            & (x.minute == 0) &
                            (x.second == 0)),
                    post_2020_dates))
                )
            )
        # append post 2020 data to counters
        for (collection, subset_dates) in zip(collections, month_indexes):
            new = get_ambient_ngrams(word=word,
                                               dates=subset_dates,
                                               scheme=scheme,
                                               lang=lang,
                                               case_sensitive=case_sensitive,
                                               collection=collection,
                                               database=db)
            counters.extend(new)
        assert len(counters) == len(dates)-1, f"Dates mismatch: Likely caused by failed database connection. Retry later \n " \
                                              f"Dates: {dates} \n" \
                                              f"{len(counters)} {len(dates)} \n" \
                                              f"{dates[-1]}, {switch_date2}, {post_2020_dates[0]}"

    return counters


def assemble_ambient_ngrams_location(word,
                                     dates,
                                     scheme=1,
                                     lang='en',
                                     case_sensitive=False,
                                     loc='state',
                                     ):
    """Wrapper function to combine data from date segmented tweet collections by state or city

    Args:
        word: ambient anchor word
        dates: a pandas date array
        scheme: length of n-gram to parse
        lang: language to query
        high_res: flag to use ten percent sample for 2020 instead of one percent
        case_sensitive: Bool to make lowe when parsing
        loc: str, choose from {'state', 'city_state'}

    Returns:
        counters: a list of counter objects of n-gram counts
    """
    db = 'tweet_segmented_location'

    dates = dates.tz_convert('UTC')
    # todo: multiprocess the parse
    freq = dates.freq
    loc_counters = {}

    # create month level slices
    month_indexes = []
    collections = []
    iterator = {(i.year, i.month) for i in dates}
    for (year, month) in sorted(iterator):
        collections.append(f"{year}-{month:02}")
        # lots of edge cases to cover end of year
        month_indexes.append(
            pd.DatetimeIndex(
                list(filter(lambda x: ((x.month == month) & (x.year == year)) |
                     ((((x.year == year) & (x.month == month + 1)) | ((x.year == year + 1) & (x.month == 1) & (month == 12)))
                        & (x.day == 1) &
                        ((x.hour == 0) | (x.hour == 5)| (x.hour == 4))
                        & (x.minute == 0) &
                        (x.second == 0)),
                    dates))
            )
        )
        
    # append data to counters for each month
    for (collection, subset_dates) in zip(collections, month_indexes):
        print(f"Querying from {collection}")
        new = get_ambient_ngrams_location_parse(word=word,
                                 dates=subset_dates,
                                 scheme=scheme,
                                 lang=lang,
                                 case_sensitive=case_sensitive,
                                 collection=collection,
                                 database=db,
                                 loc=loc,
                                 )
        for key, value in new.items():
            # add new locations to 
            if key not in loc_counters:
                loc_counters[key] = {}
                
            loc_counters[key].update(value)
            
    return loc_counters


def assemble_ambient_tweets(word,
                            dates,
                            scheme=1,
                            high_res=False,
                            lang='en',
                            case_sensitive=False,
                            limit=0,
                            project=True,
                            db='tweets_segmented',
                            hostname='hydra.uvm.edu',
                            port=27016,
                            query=None,
                            ):
    """Wrapper function to combine data from date segmented tweet collections

    Args:
        word: ambient anchor word
        dates: a pandas date array
        scheme: length of n-gram to parse
        lang: language to query
        high_res: flag to use ten percent sample for 2020 instead of one percent
        case_sensitive: Bool to make lowe when parsing
        loc: str, choose from {'state', 'city_state'}

    Returns:
        counters: a list of counter objects of n-gram counts
    """
    if high_res:
        db = 'tweets_segmented_ten_percent'

    freq = dates.freq
    tweets = []

    # create month level slices
    month_indexes = []
    collections = []
    iterator = {(i.year, i.month) for i in dates}
    for (year, month) in sorted(iterator):
        collections.append(f"{year}-{month:02}")
        # lots of edge cases to cover end of year
        month_indexes.append(
            pd.DatetimeIndex(
                list(filter(lambda x: ((x.month == month) & (x.year == year)) |
                                      ((((x.year == year) & (x.month == month + 1)) | (
                                                  (x.year == year + 1) & (x.month == 1) & (month == 12)))
                                       & (x.day == 1) &
                                       ((x.hour == 0) | (x.hour == 5) | (x.hour == 4))
                                       & (x.minute == 0) &
                                       (x.second == 0)),
                            dates))
            )
        )

    args = []
    for (collection, subset_dates) in zip(collections, month_indexes):
        args.append({"word":word,
                     "dates":subset_dates,
                     "lang":lang,
                     "case_sensitive":case_sensitive,
                     "collection":collection,
                     "database":db,
                     "limit":limit,
                     "project":project,
                     "hostname":hostname,
                     "port":port,
                     'query':query,
                     })
    with Pool(8) as p:
        tweets = p.map(get_ambient_tweets_wrapper, args)

    return [tweet for tweet_list in tweets for tweet in tweet_list]


def sample_ngrams_array(dates, scheme=1, lang='en', collection='2020-06', database='tweets_segmented', size=100):
    """ Function return an array of sampled ngram distributions over time
    :param dates: a pandas DatetimeIndex array
    :param scheme: length of n-gram to parse
    :param lang: language to query
    :param collection: mongo collection to query
    :return: a list of n-grams counter objects
    """
    date_array = [(dates[i], dates[i + 1]) for i in range(len(dates) - 1)]
    args_array = [(date, scheme, lang, collection, database, size) for date in date_array]

    with Pool(10) as p:
        sampled_ngram_array = p.starmap(sample_ngrams, args_array)

    return sampled_ngram_array


def sample_ngrams(dates, scheme=1, lang='en', collection='2020-06', database='tweets_segmented', size=100):
    """ function to Sample n-gram counter objects from tweets in a date range
    :param dates: a pandas DatetimeIndex array
    :param scheme: length of n-gram to parse
    :param lang: language to query
    :param collection: mongo collection to query
    :return: a counter object of n-grams
    """
    with pkg_resources.path('pkg_data', f"ngrams.bin") as ngrams_pth:
        ngrams_parser = get_ngrams_parser(ngrams_pth)
        
    username, pwd = get_credentials()
    client = tweet_connect(username, pwd, collection=collection, database=database)
    db = client[database]
    tweets = db[collection]
    try:
        delta = dates[1] - dates[0]
    except IndexError:
        print("IndexError: increment date range by one unit to avoid length 1 date list.  Missing data if freq > 1 day")

    counters = counter.NgramCounter({})
    for t in tweets.aggregate([{'$match': {'fastText_lang': lang,
                                           'tweet_created_at':
                                               {'$gte': dates[0],
                                                '$lt': dates[-1]}}},
                               {'$sample': {'size': size}}],
                              allowDiskUse=True):
        counters += parse_ngrams_tweet(t, ngrams_parser, scheme)
        
    client.close()
    return counters


def sample_ambient_ngrams(word, dates, scheme=1, lang='en', collection='2020-06', database='tweets_segmented',
                          size=100):
    """ function to sample n-gram counter objects from tweets including anchor word
    :param word: ambient anchor word
    :param dates: a pandas DatetimeIndex array
    :param scheme: length of n-gram to parse
    :param lang: language to query
    :param collection: mongo collection to query
    :return: a counter object of n-grams
    """

    with pkg_resources.path("pkg_data", f"ngrams.bin") as ngrams_pth:
        ngrams_parser = get_ngrams_parser(ngrams_pth)

    username, pwd = get_credentials()
    client = tweet_connect(username, pwd, collection=collection, database=database)
    db = client[database]
    tweets = db[collection]
    try:
        delta = dates[1] - dates[0]
    except IndexError:
        print("IndexError: increment date range by one unit to avoid length 1 date list.  Missing data if freq > 1 day")

    counters = counter.NgramCounter({})
    for t in tweets.aggregate([{'$match': {'fastText_lang': 'en',
                                           '$text': {'$search': f"\"{word}\""},
                                           'tweet_created_at':
                                               {'$gte': dates[0],
                                                '$lt': dates[-1]}}},
                               {'$sample': {'size': size}}],
                              allowDiskUse=True):
        counters += parse_ngrams_tweet(t, ngrams_parser, scheme)
        
    client.close()
    return counters


def get_ambient_ngrams_partial(
        word,
        dates,
        tweets,
        scheme=1,
        lang='en',
        case_sensitive=False,
        project=None,
        query=None,
        ):
    """Core function to compute n-gram counter objects
    :param word: ambient anchor word or n-gram
    :param dates: a pandas DatetimeIndex array
    :param scheme: length of n-gram to parse
    :param lang: language to query
    :param collection: mongo collection to query
    :return: a counter object of n-grams

    Read https://docs.mongodb.com/manual/reference/operator/query/text/#mongodb-query-op.-text
    for more on querying with text indexes
    """
    logger = logging.getLogger('ambientQueryLogger')

    try:
        delta = dates[1] - dates[0]
    except IndexError:
        print(
            "IndexError: increment date range by one unit to avoid length 1 date list \n End ranges of months missing data if freq > 1 day")
        return []

    if query is None:
        query = {'$text': {'$search': f"\"{word} \"",
                           '$caseSensitive': case_sensitive
                           },
                 'tweet_created_at':
                     {'$gte': dates[0],
                      '$lt': dates[-1]
                      },
                 'fastText_lang': lang
                 }
    if project is None:
        project = {'pure_text': 1,
                    'rt_text': 1,
                    'tweet_created_at': 1,
                    }
    for t in tweets.find(query,
                         project
                         ):
        yield t


def get_ambient_ngrams_parse(
                             word,
                             dates,
                             scheme=1,
                             lang='en',
                             case_sensitive=False,
                             collection='drip_hose',
                             database='tweets',
                            ):
    """ Modular standard parse for ngrams"""
    
    username, pwd = get_credentials()
    client = tweet_connect(username,
                           pwd,
                           collection=collection,
                           database=database
                           )
    db = client[database]
    tweets = db[collection]

    with pkg_resources.path("pkg_data", f"ngrams.bin") as ngrams_pth:
        ngrams_parser = get_ngrams_parser(ngrams_pth)

    counters = {i: counter.NgramCounter({}) for i in dates[:-1]}

    for t in get_ambient_ngrams_partial(
                                        word,
                                        dates,
                                        tweets,
                                        scheme=1,
                                        lang='en',
                                        case_sensitive=False,
                                        ):
        counters[
            time_tag(dates,
                     t['tweet_created_at'].replace(tzinfo=pytz.UTC)
                    )
                ] += parse_ngrams_tweet(t, ngrams_parser, scheme)

    if len(counters) != len(dates) - 1:
        print('counters', len(counters), len(dates))
    client.close()
    return [i for i in counters.values()]


def get_ambient_ngrams_location_parse(
                                      word,
                                      dates,
                                      scheme=1,
                                      lang='en',
                                      case_sensitive=False,
                                      collection='drip_hose',
                                      database='tweets',
                                      loc='state',
                                      ):
    """ Modular parse for ngrams by state or city_state
    
    Args:
        
        
    Returns: Dict of dicts, eg: `
    {'NY':
        {datetime(2020,1,1):
            {'word':
                {'count':3, 'count_no_rt':1}
            }
        }
    }`
                                
        
    """
    with pkg_resources.path("pkg_data", f"ngrams.bin") as ngrams_pth:
        ngrams_parser = get_ngrams_parser(ngrams_pth)

    
    username, pwd = get_credentials()
    client = tweet_connect(username,
                           pwd,
                           collection=collection,
                           database=database,
                           hostname='serverus.cems.uvm.edu',
                           port=27015,
                           )
    db = client[database]
    tweets = db[collection]
    print(tweets)
    locations = tweets.distinct(loc)

    # create empty location dict
    counters = {}
    index_loc = 0
    index_date = 0
    for loc_i in locations:
        for i in dates[:-1]:
            if index_date == 0:
                counters[loc_i] = {}
            counters[loc_i][i] = counter.NgramCounter({})
            index_date += 1
        index_date=0
        index_loc += 1

    project = {'pure_text': 1,
                'rt_text': 1,
                'tweet_created_at': 1,
                loc: 1,
                }

    for t in get_ambient_ngrams_partial(
            word,
            dates,
            tweets,
            scheme=1,
            lang='en',
            case_sensitive=False,
            project=project,
             ):
        counters[loc_tag(loc,t)][
                 time_tag(dates,
                         t['tweet_created_at'].replace(tzinfo=pytz.UTC)
                         )
                ] += parse_ngrams_tweet(t, ngrams_parser, scheme)
    if len(counters) != len(dates) - 1:
        print('counters', len(counters), len(dates))
    client.close()
    return counters


def loc_tag(loc, t):
    try: 
        return t[loc]
    except KeyError:
        return

def get_ambient_ngrams(
        word,
        dates,
        scheme=1,
        lang='en',
        case_sensitive=False,
        collection='drip_hose',
        database='tweets',
        hostname='hydra.uvm.edu'
        ):
    """Core function to compute n-gram counter objects
    :param word: ambient anchor word or n-gram
    :param dates: a pandas DatetimeIndex array
    :param scheme: length of n-gram to parse
    :param lang: language to query
    :param collection: mongo collection to query
    :return: a counter object of n-grams

    Read https://docs.mongodb.com/manual/reference/operator/query/text/#mongodb-query-op.-text
    for more on querying with text indexes
    """
    logger = logging.getLogger('ambientQueryLogger')
    with pkg_resources.path("pkg_data", f"ngrams.bin") as ngrams_pth:
        ngrams_parser = get_ngrams_parser(ngrams_pth)

    username, pwd = get_credentials()
    client = tweet_connect(username,
                           pwd,
                           collection=collection,
                           database=database
                           )
    db = client[database]
    tweets = db[collection]
    try:
        delta = dates[1] - dates[0]
    except IndexError:
        print(
            "IndexError: increment date range by one unit to avoid length 1 date list \n End ranges of months missing data if freq > 1 day")
        return []

    counters = {i: counter.NgramCounter({}) for i in dates[:-1]}
    query = {'$text': {'$search': f"\"{word} \"",
                       '$caseSensitive': case_sensitive
                       },
             'tweet_created_at':
                 {'$gte': dates[0],
                  '$lt': dates[-1]
                  },
             'fastText_lang': lang
             }
    for t in tweets.find(query,
                         {'pure_text': 1,
                          'rt_text': 1,
                          'tweet_created_at': 1,
                          }
                         ):
        counters[time_tag(dates,
                          t['tweet_created_at'].replace(tzinfo=pytz.UTC)
                          )
        ] += parse_ngrams_tweet(t, ngrams_parser, scheme)
    if len(counters) != len(dates) - 1:
        print('counters', len(counters), len(dates))
    client.close()
    return [i for i in counters.values()]


def parse_df_to_counters(df, dates, scheme=1):
    """ Given a dataframe of tweets return counters"""
    with pkg_resources.path("pkg_data", f"ngrams.bin") as ngrams_pth:
        ngrams_parser = get_ngrams_parser(ngrams_pth)

    counters = {i: counter.NgramCounter({}) for i in dates[:-1]}
    for i, row in df.iterrows():
        try:
            date = datetime.datetime.strptime(row['tweet_created_at'], "%Y-%m-%d %H:%M:%S")

            counters[
                    time_tag(dates,
                             date
                            )
                        ] += parse_df_tweet(row, ngrams_parser, scheme)

        except TypeError:
            pass
    return [i for i in counters.values()]


def parse_labeled_df_to_counters(df, dates, label, scheme=1):
    """ Given a dataframe of tweets classified as relevant or not, return counters for both label"""
    with pkg_resources.path("pkg_data", f"ngrams.bin") as ngrams_pth:
        ngrams_parser = get_ngrams_parser(ngrams_pth)

    counters_1 = {i: counter.NgramCounter({}) for i in dates[:-1]}
    counters_2 = {i: counter.NgramCounter({}) for i in dates[:-1]}

    for i, row in df.iterrows():
        try:
            date = datetime.datetime.strptime(row['tweet_created_at'], "%Y-%m-%d %H:%M:%S")
            if row['Label'] == label:
                counters_1[
                    time_tag(dates,
                             date
                            )
                        ] += parse_df_tweet(row, ngrams_parser, scheme)
            else:
                counters_2[
                    time_tag(dates,
                             date#.replace(tzinfo=pytz.UTC)
                            )
                        ] += parse_df_tweet(row, ngrams_parser, scheme)
        except TypeError:
            pass
    return [i for i in counters_1.values()], [i for i in counters_2.values()]


def get_ambient_tweets_wrapper(args):
    return get_ambient_tweets(**args)

def get_ambient_tweets(word, dates, lang='en',
                       case_sensitive=False,
                       collection='drip_hose',
                       database='tweets',
                       limit=0, project=True,
                       query=None,
                       hostname='hydra.uvm.edu',
                       port=27016,
):
    """Core function to compute n-gram counter objects
    :param word: ambient anchor word or n-gram, or list of keywords for query
    :param dates: a pandas DatetimeIndex array
    :param lang: language to query
    :param collection: mongo collection to query
    :return: a counter object of n-grams

    Read https://docs.mongodb.com/manual/reference/operator/query/text/#mongodb-query-op.-text
    for more on querying with text indexes
    """

    username, pwd = get_credentials()
    client = tweet_connect(username, pwd, collection=collection, database=database, hostname=hostname, port=port)
    db = client[database]
    tweets = db[collection]
    projection = {True: {'pure_text':1, 'rt_text':1, 'tweet_created_at':1},
                  False: ['pure_text', 'rt_text', 'tweet_created_at', 'id', 'actor', 'object', 'fastText_lang', 'link', 'verb', 'state', 'city_state', 'user']
                  }

    try:
        delta = dates[1] - dates[0]
    except IndexError:
        print(
            "IndexError: increment date range by one unit to avoid length 1 date list \n End ranges of months missing data if freq > 1 day")
        return []
    if lang is None:
        lang = {'$exists': True}

    tweet_list = []
    if query is None:
        if type(word) is str:
            query = {'$text': {'$search': f"\"{word} \"",
                               '$caseSensitive': case_sensitive},
                     'tweet_created_at': {'$gte': dates[0],
                                          '$lt': dates[-1]},
                     'fastText_lang': lang}
        elif type(word) is list:
            query = {'$text': {'$search': ' '.join(f"\" {word} \""),
                               '$caseSensitive': case_sensitive},
                     'tweet_created_at': {'$gte': dates[0],
                                          '$lt': dates[-1]},
                     'fastText_lang': lang}
    for t in tweets.find(query,
                         projection[project]
                         ).limit(limit=limit):
        t['_id'] = str(t['_id'])
        tweet_list.append(t)
        
    client.close()
    return tweet_list


def count_ambient_tweets(word, dates, lang='en', case_sensitive=False, collection='drip_hose',
                       database='tweets'):
    """Function to count number of matching tweets
    :param word: ambient anchor word or n-gram
    :param dates: a pandas DatetimeIndex array
    :param lang: language to query
    :param collection: mongo collection to query
    :return: a counter object of n-grams

    Read https://docs.mongodb.com/manual/reference/operator/query/text/#mongodb-query-op.-text
    for more on querying with text indexes
    """

    username, pwd = get_credentials()
    client = tweet_connect(username, pwd, collection=collection, database=database)
    db = client[database]
    tweets = db[collection]
    try:
        delta = dates[1] - dates[0]
    except IndexError:
        print(
            "IndexError: increment date range by one unit to avoid length 1 date list \n End ranges of months missing data if freq > 1 day")
        return []
    print("Remove this function.")
    return tweets.find({'$text': {'$search': f"\" {word} \"",
                                    '$caseSensitive': case_sensitive},
                          'tweet_created_at':
                              {'$gte': dates[0],
                               '$lt': dates[-1]},
                          'fastText_lang': lang}).count()


def get_ngrams(dates, scheme=1, lang='en',
               collection='drip_hose',
               database='tweets',
               threshold=0,
               hostname='hydra.uvm.edu',
               port='27016'):
    """Core function to compute n-gram counter objects
    :param dates: a pandas DatetimeIndex array
    :param scheme: length of n-gram to parse
    :param lang: language to query
    :param collection: mongo collection to query
    :param threshold: count under which to discard ngrams
    :return: a counter object of n-grams
    """

    with pkg_resources.path('pkg_data', f"ngrams.bin") as ngrams_pth:
        ngrams_parser = get_ngrams_parser(ngrams_pth)

    username, pwd = get_credentials()
    client = tweet_connect(username, pwd,
                           collection=collection,
                           database=database,
                           hostname=hostname,
                           port=port,
                           )
    db = client[database]
    tweets = db[collection]
    try:
        delta = dates[1] - dates[0]
    except IndexError:
        print("IndexError: increment date range by one unit to avoid length 1 date list")

    counters = {i: counter.NgramCounter({}) for i in dates[:-1]}

    query = {'tweet_created_at':
                {'$gte': dates[0],
                '$lt': dates[-1]
                 },
            'fastText_lang': lang
            }
    filter = {'pure_text':1, 'rt_text':1, 'tweet_created_at':1}
    for t in tweets.find(query, filter):
        counters[time_tag(dates, t['tweet_created_at'].replace(tzinfo=pytz.UTC))] += parse_ngrams_tweet(t, ngrams_parser, scheme)

    # todo: do this with a list comprehension if thresh

    result = []
    for i, counter_i in enumerate(counters.values()):
        result.append(counter.NgramCounter({key: j for key, j in counter_i.items() if j['count'] > threshold}))
    client.close()
    return result


def get_ngrams_multicore(dates, scheme=1, lang='en', collection='drip_hose', database='tweets', threshold=0, cores=2):
    """Core function to compute n-gram counter objects multiprocessed

    :param dates: a pandas DatetimeIndex array
    :param scheme: length of n-gram to parse
    :param lang: language to query
    :param collection: mongo collection to query
    :return: a counter object of n-grams
    """
    date_array = [dates[i:i + 2] for i in range(len(dates) - 1)]
    print(date_array)
    args_array = [(date, scheme, lang, collection, database, threshold) for date in date_array]
    print(args_array)

    with Pool(cores) as p:
        sampled_ngram_array = p.starmap(get_ngrams, args_array)

    print(sampled_ngram_array)
    sampled_ngram_array = [j for inner_counter in sampled_ngram_array for j in inner_counter]
    print(len(sampled_ngram_array))
    return sampled_ngram_array


def save_ambient_timeseries(counters, dates, filename, count_type='count'):
    """ Saves a csv file of a timeseries over the given range
    :param counters: list of counter objects
    :param dates: date range over which counters were computed
    :param filename: a filename to save as
    :param count_type: choose to save 'count' or 'count_no_rt'"""
    count_list = [i for i in range(len(counters))]

    for i, countr in enumerate(counters):
        if count_type[:5] == 'count':
            count_list[i] = {word: count_dict[count_type] for word, count_dict in countr.items()}
        elif count_type[:4] == 'freq':
            total = np.sum(value['count' + count_type[4:]] for value in countr.values)
            count_list[i] = {word: count_dict[count_type] / total for word, count_dict in countr.items()}

    df = pd.DataFrame(count_list, index=dates)
    df.T.to_csv(filename + "_" + count_type + ".tsv", sep='\t')

    return


def extract_timeseries(word, counters, dates, data_type='count'):
    """ extract single pandas series from ngram counter lists
    :param word: target word in ambient words
    :param counter: list of storyon counters
    :param dates: dates range
    :param data_type: return counts for pure or organic text ["count", "count_no_rt", "freq", 'freq_no_rt']
    :return: pandas.Series object for a single target word"""

    if data_type[-5:] == 'no_rt':
        suffix = '_no_rt'
    else:
        suffix = ''

    array = np.zeros(len(counters))

    for i, counter in enumerate(counters):
        if data_type[:5] == 'count':
            try:
                array[i] = counter[word][data_type]
            except KeyError:
                array[i] = 0

        elif data_type[:4] == 'freq':
            total = np.sum(value['count' + suffix] for value in counter.values())
            if total > 0:
                try:
                    array[i] = counter[word]['count' + suffix] / total
                except KeyError:
                    array[i] = 0.
            else:
                array[i] = 0.
    return pd.Series(array, index=dates, name=word)


def top_n_timeseries(counters, dates, N=100, data_type='count'):
    """ extract pandas DataFrame from ngram counter lists
    :param counter: list of storyon counters
    :param dates: dates range
    :param N: number of timeseries to extract
    :param data_type: return counts for pure or organic text ["count", "count_no_rt", "freq", 'freq_no_rt']
    :return: pandas.DataFrame object for top target words"""

    if data_type[-5:] == 'no_rt':
        suffix = 'no_rt'
    else:
        suffix = ''

    total_counter = np.sum(counters, initial=counter.NgramCounter({}))
    sorted_grams = sorted(total_counter.items(), key=lambda i: i[1]['count' + suffix], reverse=True)
    top_words = [i[0] for i in sorted_grams[:N]]

    df = pd.DataFrame(index=dates, columns=top_words)
    for word in top_words:
        df[word] = extract_timeseries(word, counters, dates, data_type).values

    return df


def time_tag(time_ranges, time_i):
    """given a time and time series, return a unique timestamp label"""
    for i, date in enumerate(time_ranges):
        if time_i < date:
            break
    return time_ranges[i - 1]


def date_index(date, dates):
    """given a date and a pandas DatetimeIndex, return the index containing said date"""
    try:
        index = (dates > date) & (dates <= date + dates.freq)
    except TypeError as e:
        print(f"TypeError: date_index {e}")
        index = (dates > date) & (dates <= date + (dates[1]-dates[0]))
        pass
    return np.argmax(index) - 1


def print_tweet_text(tweet):
    """ Debugging function to print tweet text in database processed format: e.i. pure and rt text"""
    try:
        pprint(tweet['pure_text'])
    except KeyError:
        print('No pure text')
        pass
    try:
        pprint(tweet['rt_text'])
    except KeyError:
        print("No RT Text")


def valid_date(s):
    try:
        return datetime.datetime.strptime(s, "%Y-%m-%d")
    except ValueError:
        msg = "Not a valid date: '{0}'.".format(s)
        raise argparse.ArgumentTypeError(msg)


def parse_args(args):
    parser = argparse.ArgumentParser(
        description="A command line interface for ambient n-gram timeseries and zipf distributions",
        formatter_class=argparse.RawDescriptionHelpFormatter,
        epilog=textwrap.dedent('''\
         Remember to port forward if you're not on campus:
             ssh -N -f -L localhost:27016:localhost:27016 ti-89

         N-gram data format:
             ./tweet_db_query.py -w "coronavirus" -s -b 2020-01-01 -e 2020-06-15
             '''))
    parser.add_argument(
        '-w', '--word',
        type=str,
        help='ngram to plot'
    )
    parser.add_argument(
        '-b', '--begin_date',
        help="beginning of date range -- formate YYYY-MM-DD",
        type=valid_date,
        default='2020-01-01',
    )
    parser.add_argument(
        '-e', '--end_date',
        help="end of date range -- format YYYY-MM-DD",
        type=valid_date,
        default=datetime.datetime.today().strftime('%Y-%m-%d')
    )
    parser.add_argument(
        '-s', '--save_counters',
        help="flag for saving counters for later",
        action='store_true'
    )
    parser.add_argument(
        '-p', '--pth',
        help="path to save counter objects",
        default='../data/'
    )
    parser.add_argument(
        '-f', '--freq',
        help="Frequency for datetime index ['T', 'H', 'D', '2D']",
        default='D'
    )
    return parser.parse_args(args)


def main():
    pass

if __name__ == "__main__":
    main()
