"""
Database Classes

"""

import pandas as pd
import time
import sqlalchemy
from sqlalchemy import create_engine, inspect, event
from sqlalchemy.orm import declarative_base, sessionmaker
from sqlalchemy import Column, Integer, Float, String, Date

pd.options.display.width = 0
Base = declarative_base()

def db_session(usr="postgres", pwd="roboctopus"):
    """ Generic database connection function

    Args:
        usr: username
        pwd: password
    """
    #ssl_args = {'ssl': {'ca': '../login/webdb-cacert.pem'}}


    db_engine = create_engine(
        f"postgresql://{usr}:{pwd}@localhost:5432/ambient",
        )
    Session = sessionmaker(bind=db_engine)
    db = Session()
    return db

class Records(Base):
    """ ORM class for storing start and end dates for queries stored in DBs on this database"""
    __tablename__ = "_records"

    anchor_ngram = Column(String, primary_key=True)
    start_time = Column(Date)
    end_time = Column(Date)

    def __repr__(self):
        """Defines print string for this object"""
        return f"Anchor ngram: {anchor_ngram}, \n" \
               f"start_time={start_time} \n" \
               f"end_time={end_time}"

class NgramCounts(Base):
    """ ORM base class for ngram counter tables"""

    __tablename__ = "ngram_counts"

    ngram = Column(String, primary_key=True)
    timestamp = Column(Date, primary_key=True)
    count = Column(Integer)
    count_no_rt = Column(Integer)

def ngram_table_mapper(anchor=None, res=None, tablename=None):
    """ Dynamically create ORM mapper classes"""
    if not tablename:
        tablename = f"{anchor}_res_{res}"

    attr_dict = {
        '__tablename__': f"{tablename}",
    "ngram": Column(String, primary_key=True),
    "timestamp": Column(Date, primary_key=True),
    "count": Column(Integer),
    "count_no_rt": Column(Integer),
    }

    return type(tablename, (Base,), attr_dict)
