import sys, os
import argparse
import datetime
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from bokeh.models import CustomJS, CrosshairTool
from bokeh.layouts import column
import bokeh
from bokeh.models.widgets import TextInput
from bokeh.models import ColorBar, LogColorMapper, LinearColorMapper, CategoricalColorMapper, CDSView, GroupFilter, \
    BooleanFilter
from bokeh.plotting import figure, output_file, show, ColumnDataSource
from bokeh.io import show
from bokeh.models import CustomJS, RadioButtonGroup, OpenURL, TapTool, Panel, Tabs, Select, TextInput, DateRangeSlider, \
    DateSlider
from bokeh.palettes import Viridis5, Category20
import itertools
from matplotlib.cm import ScalarMappable
from matplotlib.colors import Normalize, rgb2hex
from pprint import pprint

pd.set_option('display.max_colwidth', None)
def valid_date(s):
    try:
        return datetime.datetime.strptime(s, "%Y-%m-%d")
    except ValueError:
        msg = "Not a valid date: '{0}'.".format(s)
        raise argparse.ArgumentTypeError(msg)


def parse_args(args):
    parser = argparse.ArgumentParser(
        description="A command line interface for creating interactive ambient tweet embedding plots",
    )
    parser.add_argument(
        '-w', '--word',
        type=str,
        help='anchor to plot'
    )
    parser.add_argument(
        '-r', '--high_res',
        help="Flag to plot high res",
        action='store_true',
    )
    parser.add_argument(
        '-b', '--begin_date',
        help="beginning of date range -- formate YYYY-MM-DD",
        type=valid_date,
        default='2020-01-01',
    )
    parser.add_argument(
        '--location',
        type=str,
        default=None,
        help="Include a location feild, either state or city_state",
    )
    parser.add_argument(
        '-e', '--end_date',
        help="end of date range -- format YYYY-MM-DD",
        type=valid_date,
        default='2020-12-31'
    )
    parser.add_argument(
        '-s', '--sentiment',
        help="Flag to plot sentiment",
        action='store_true',
    )
    parser.add_argument(
        '-p', '--pth',
        help="path to load embedding data",
        default="/home/michael/projects/embeddings/data/mortality/"
    )
    parser.add_argument(
        '-f', '--fname',
        default=None,
        help='data to plot'
    )
    return parser.parse_args(args)

# remove full path
labeled_pth = '/home/michael/projects/embeddings/data/mortality_enriched/'
labeled_pth1 = '/home/michael/projects/embeddings/data/energy/'

fname_dict = {
    'Stroke_2020-01-01 00:00:00_2020-12-31 00:00:00_D_False_state_top1000.tsv':
        f'{labeled_pth}project_8_labeled_data.csv',
    'Flu_2020-01-01 00:00:00_2020-12-31 00:00:00_D_False_state_top1000.tsv':
        f'{labeled_pth}project_6_labels.csv',
    'Diabetes_2020-01-01 00:00:00_2020-12-31 00:00:00_D_False_state_top1000.tsv':
        f'{labeled_pth}project_7_labeled_data.csv',
    'Covid-19_2020-01-01 00:00:00_2020-12-31 00:00:00_D_False_state_top1000.tsv':
        f'{labeled_pth}project_9_labels.csv',
    'Solar_2020-01-01 00:00:00_2021-03-29 00:00:00_D_False_state_top1000.tsv':
        f'{labeled_pth1}Solar_labeled_data.csv',
    'Wind_2020-01-01 00:00:00_2021-03-29 00:00:00_D_False_state_top1000.tsv':
        f'{labeled_pth1}Wind_labeled_data.csv',
}

def load_data(fname):
    """ Load data"""
    df = pd.read_csv(fname, sep='\t', parse_dates=['tweet_created_at'], index_col=0)
    #df.dropna(inplace=True)
    df = df.astype({'y': 'float64', 'x': 'float64'})
    df = filter_NaT(df)

    label_fname = fname_dict[os.path.basename(fname)]
    print(fname)
    labels = pd.read_csv(label_fname, index_col=0)
    print(labels[['Text', "Label"]])
    print(df[:21])
    df.update(labels['Label'], overwrite=False)

    df.fillna(-1, inplace=True)
    print(df[:51])

    #df = df.merge(labels[['ID','Label']], how='outer', left_index=True, right_on="ID", suffixes=('_x', "") )
    return df


def filter_NaT(df):
    index = pd.notnull(df['tweet_created_at'])
    df2 = df.loc[index]
    return df2


def get_fname(args):
    anchor, start_date, end_date, freq, high_res = (args.word, args.begin_date, args.end_date, 'D', args.high_res)
    formatting = lambda x: f'_{x}' if x is not None else ''
    loc_str = f'{formatting(args.location)}'
    fname = f"{args.pth}{anchor}_{start_date}_{end_date}_{freq}_{high_res}" \
            f"{loc_str}_top1000.tsv"
    return fname


def plot_embedding(df, outfile, args):
    """ Create the interactive html plot with date slider """
    output_file(outfile)
    plot_type = "month_colors"

    print(df.labels)
    df['time'] = [row.strftime("%m/%d/%Y, %H:%M:%S") if pd.notnull(row) else None for i, row in
                  df['tweet_created_at'].iteritems()]

    color_fields = []  # ToDo: date,
    color_mappers = []

    # Colors for hdbscan clusters
    c = bokeh.palettes.Category20[20]
    colors = [c[int(i) % 20] if int(i) != -1 else '#BDBDBD' for i in df.Label]
    df['hdbscan_clusters'] = colors

    c1 = bokeh.palettes.Category10[5]
    label_dict = {d:i for i,d in enumerate(df.Label.unique())}
    colors = [c1[label_dict[i] % 5] if int(label_dict[i]) != -1 else '#BDBDBD' for i in df.Label]
    df['Label_color'] = colors

    colors = [c[int(i) % 10] if int(i) != -1 else '#BDBDBD' for i, lang in enumerate(df.lang)]
    df['lang_colors'] = colors

    if args.location:
        state_locs_df = pd.read_csv('../pkg_data/state_centers.csv')
        if args.location == 'state':
            state_loc = "LONGITUDE"
            # state_loc = "LATITUDE"

            state_locs_dict = {row['state']: row[state_loc]
                               for i, row in state_locs_df[['state', state_loc]].iterrows()}
            df[state_loc] = [state_locs_dict[state] if state in state_locs_dict else None for state in
                             df['state'].values]
            print(df[state_loc])
            color_fields.append(str(state_loc))

        else:
            raise NotImplementedError("Get city locations and save to pkg_data")

        # df[args.location] = [row for i, row in df[args.location].iteritems()]
        if state_loc == 'LONGITUDE':
            min_coord = max(-125, min(df[state_loc]))
            max_coord = min(-70, max(df[state_loc]))
        elif state_loc == 'LATITUDE':
            min_coord = max(45, min(df[state_loc]))
            max_coord = min(30, max(df[state_loc]))
        color_mappers.append(
            LinearColorMapper(palette='Viridis256',
                              low=min(df[state_loc]),
                              high=max(df[state_loc])
                              )
        )

    color_fields.append('month')
    df['month'] = [row.month for i, row in df['tweet_created_at'].iteritems()]
    color_mappers.append(
        LinearColorMapper(palette="Viridis256",
                          low=min(df['month']),
                          high=max(df['month'])
                          )
    )

    print(df)
    source = ColumnDataSource(df)

    TOOLTIPS = """
        <div>
            <div>
                <span style="font-size: 10px;">Label: @Label </span>
            </div>
            <div>
                <span style="font-size: 10px;">@time UTC</span>
            </div>
             <div>
                <span style="font-size: 10px;">@id </span>
            </div>
            <div>
                <span style="font-size: 10px;">Language: @lang -- Verb: @verb</span>
            </div>
            <div>
                <span style="font-size: 10px;">State: @state </span>
            </div>
            <div>
                <span style="font-size: 12px;">Tweet Text:</span>
                <span style="font-size: 12px; font-weight: bold;">@Text</span>
            </div>
        </div>
    """
    figure_args = {
        'plot_width': 700, 'plot_height': 700,
        'tooltips': TOOLTIPS,
        'tools': "pan,wheel_zoom,box_zoom,reset,tap",
        'toolbar_location': "below",
        'title': f'Embedded Ambient Tweets -- Anchor: "{args.word}"',
    }

    tabs_list = []
    panels = []

    tabs_list.append(figure(plot_width=700, plot_height=700,
                            tooltips=TOOLTIPS,
                            tools="pan,wheel_zoom,box_zoom,reset,tap",
                            toolbar_location="below",
                            title=f'Embedded Ambient Tweets -- Anchor: "{args.word}"',
                            ))

    size = 4

    # date slider
    init_value = (df['tweet_created_at'].min(), df['tweet_created_at'].max())
    slider = DateSlider(start=init_value[0], end=init_value[1], value=init_value[0], step=1)

    date_filter = BooleanFilter(booleans=[True] * df.shape[0])

    slider.js_on_change('value', CustomJS(args=dict(f=date_filter, ds=source),
                                          code="""\
                                              const start = cb_obj.value;
                                              const end = start + 1000*3600*24*30*2
                                              f.booleans = Array.from(ds.data['tweet_created_at']).map(d => (d >= start && d <= end));
                                              // Needed because of https://github.com/bokeh/bokeh/issues/7273
                                              ds.change.emit();
                                          """))
    clusters = CDSView(source=source,
                       filters=[BooleanFilter([False if y == -1 else True for y in df.labels]), date_filter])
    outliers = CDSView(source=source,
                       filters=[BooleanFilter([True if y == -1 else False for y in df.labels]), date_filter])

    tabs_list[0].circle('x', 'y', size=size, source=source,
                        fill_color='#BDBDBD', fill_alpha=0.3,
                        line_color='#000000', line_alpha=0.1, view=outliers)
    tabs_list[0].circle('x', 'y', size=size, source=source,
                        fill_color="hdbscan_clusters", fill_alpha=0.7,
                        line_color='#000000', line_alpha=0.1, view=clusters)
    panels.append(Panel(child=column(tabs_list[0], slider), title='hdbscan clusters'))

    for i, color_field in enumerate(color_fields):
        tabs_list.append(
            figure(x_range=tabs_list[0].x_range,
                   y_range=tabs_list[0].y_range,
                   **figure_args)
        )

        tabs_list[i + 1].circle('x', 'y', size=5, source=source,
                                fill_color={'field': color_field, 'transform': color_mappers[i]},
                                fill_alpha=0.7, line_color=None)

        color_bar = ColorBar(color_mapper=color_mappers[i], label_standoff=12)
        tabs_list[i + 1].add_layout(color_bar, 'right')

        panels.append(Panel(child=tabs_list[i + 1], title=color_field))

    tabs_list.append(
        figure(x_range=tabs_list[0].x_range,
               y_range=tabs_list[0].y_range,
               **figure_args)
    )

    clusters1 = CDSView(source=source,
                       filters=[BooleanFilter([False if y == '#BDBDBD' else True for y in df.Label_color]), date_filter])
    outliers1 = CDSView(source=source,
                       filters=[BooleanFilter([True if y == '#BDBDBD' else False for y in df.Label_color]), date_filter])

    tabs_list[-1].circle('x', 'y', size=size, source=source,
                        fill_color='#BDBDBD', fill_alpha=0.3,
                        line_color='#000000', line_alpha=0.1, view=outliers1)
    tabs_list[-1].circle('x', 'y', size=5, source=source,
                        fill_color="Label_color", fill_alpha=0.7,
                        line_color='#000000', line_alpha=0.1, view=clusters1)
    panels.append(Panel(child=column(tabs_list[-1], slider), title='Labels'))


    # click to open link to tweet
    for p in tabs_list:
        url = "@link"
        taptool = p.select(type=TapTool)
        taptool.callback = OpenURL(url=url)

    print(tabs_list)
    print(len(tabs_list))
    show(Tabs(tabs=panels))
    return


def main(args=None):
    if args is None:
        args = sys.argv[1:]
        args = parse_args(args)

    print(args.fname)
    if args.fname is None:
        fname = get_fname(args)
    else:
        fname = args.fname
    # try:
    df = load_data(fname)
    # except:
    #    print('on no')
    #    return
    if df['Label'].isnull().values.all():
        print(df["Label"])
        df = df.drop(['Label'], axis=1)


    outfile = f'/home/michael/projects/embeddings/hoverplots/{args.word}_{args.high_res}_{args.location}.html'
    outfile = outfile.replace('#', '!')
    plot_embedding(df, outfile, args)


if __name__ == "__main__":
    main()