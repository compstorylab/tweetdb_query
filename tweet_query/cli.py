import sys
import datetime
import pandas as pd
import pprint
import time
import textwrap
import tweet_query
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from tweet_query.counters import logging_init
from multiprocessing import Pool
import logging
import argparse

def get_sentiment_runner(anchor, args):
    """ wrapper to multiprocess sentiment plotting"""

    counters = get_counters(anchor, args)

    time.sleep(0.001)
    if args.caching:
        return None
    if not args.caching:
        plot_sentiment(counters)

def get_counters(anchor, args):
    """ Get counters asap and check local cache"""

    start_time = time.time()
    start_date = args.begin_date
    end_date = args.end_date

    freq = 'D'  # weekly frequency
    dates = pd.date_range(start_date, end_date, freq=freq)
    high_res = args.high_res

    counters = tweet_query.counters.Counters(dates,
                                             anchor,
                                             high_res=args.high_res,
                                             pth=args.pth,
                                             )
    if args.delete:
        try:
            counters.remove_db_entry()
            print(f'Deleting {anchor}...')
        except KeyError:
            pass
    counters.get(save=True,
                 caching=args.caching,
                 localdb=args.localdb
                 )

    print(f"Data for {anchor}: [highres={high_res}] acquired in {time.time()-start_time:.2f} s")

    if not args.caching:
        counters.aggregate(args.freq)
    return counters


def plot_sentiment(counters):
    res_dict = {True: 'High',
                False: "Low"
                }
    fig = plt.subplots( figsize=(5,5), dpi=400)
    ax,axsub = counters.plot_sentiment_timeseries(unsafe=True, fig=fig, ylims=(5.0, 7.0))
    #ax.axvline(datetime.datetime(2021, 3, 1, 12, 0, tzinfo=pytz.timezone('US/Eastern')))
    locator = mdates.MonthLocator([i * 6 + 1 for i in range(1)])
    #locator = mdates.MonthLocator([1 for i in range(3)])

    formatter = mdates.DateFormatter('%Y')
    ax.xaxis.set_major_locator(locator)
    ax.xaxis.set_major_formatter(formatter)
    ax.set_ylabel("Ambient\n Sentiment", fontsize=14)
    ax.tick_params(axis='x', labelsize=12)
    plt.setp(ax.get_xticklabels(), ha='right')
    ax.legend(fancybox=False, ncol=2)
    axsub.set_title(counters.anchor, fontsize=20)
    ax.set_axisbelow(True)
    plt.tight_layout()
    figname = f"../figures/{counters.anchor}_sentiment_{counters.dates[0].strftime('%Y-%m-%d')}" \
              f"_{counters.dates[-1].strftime('%Y-%m-%d')}" \
              f"_freq-{counters.freq}" \
              f"_res_{res_dict[counters.high_res]}"
    plt.savefig(figname+".pdf")
    plt.savefig(figname+".png")
    plt.close()



def valid_date(s):
    try:
        return datetime.datetime.strptime(s, "%Y-%m-%d")
    except ValueError:
        msg = "Not a valid date: '{0}'.".format(s)
        raise argparse.ArgumentTypeError(msg)

def text_reader(fname):
    with open(fname, 'r') as f:
        word_list = [i.strip() for i in f.readlines()]
    return word_list

def parse_args(args):
    parser = argparse.ArgumentParser(
        description="A command line interface for ambient n-gram timeseries and zipf distributions",
        formatter_class=argparse.RawDescriptionHelpFormatter,
        epilog=textwrap.dedent('''\
         Remember to port forward if you're not on campus:
             ssh -N -f -L localhost:27016:localhost:27016 ti-89

         N-gram data format:
             ./tweet_db_query.py -w "coronavirus" -s -b 2020-01-01 -e 2020-06-15
             '''))
    parser.add_argument(
        '-w', '--word',
        type=str,
        help='ngram to plot'
    )
    parser.add_argument(
        '-l', '--word_list',
        type=text_reader,
        help='textfile of words to plot'
    )
    parser.add_argument(
        '-b', '--begin_date',
        help="beginning of date range -- formate YYYY-MM-DD",
        type=valid_date,
        default='2019-09-01',
    )
    parser.add_argument(
        '-e', '--end_date',
        help="end of date range -- format YYYY-MM-DD",
        type=valid_date,
        default=(datetime.datetime.today()-datetime.timedelta(2)).strftime('%Y-%m-%d')
    )
    parser.add_argument(
        '-c', '--caching',
        help="flag for saving counters for later. skips plotting",
        action='store_true'
    )
    parser.add_argument(
        '-p', '--pth',
        help="path to save counter objects",
        default=None
    )
    parser.add_argument(
        '-f', '--freq',
        help="Frequency for datetime index ['T', 'H', 'D', '2D']",
        default='W'
    )
    parser.add_argument(
        '-r', '--high_res',
        help="Flag to plot high res",
        action='store_true',
    )
    parser.add_argument(
        '-d', '--delete',
        help="remove cached data",
        action='store_true',
    )
    parser.add_argument(
        '-n', 
        help="number of parallel threads",
        default=8,
        type=int,
    )
    parser.add_argument(
        '-s', '--save_counters',
        help="save counters to json",
        action='store_true',
    )
    parser.add_argument(
        '-t', '--test',
        help="test and print word list",
        action='store_true',
    )
    parser.add_argument(
        '--localdb',
        help="save counters to local db instead of json",
        action='store_true',
    )
    return parser.parse_args(args)


def main(args=None):
    if args is None:
        args = sys.argv[1:]
    args = parse_args(args)
    logging_init()

    if args.word:
        counters = get_counters(args.word, args)
        if not args.caching:
            plot_sentiment(counters)
    elif args.word_list:
        if args.test:
            print(args.word_list)
            exit()

        map_args = [(word,args) for word in args.word_list]
        with Pool(args.n) as p:
            _ = p.starmap(get_sentiment_runner, map_args)


if __name__ == "__main__":
    main()
