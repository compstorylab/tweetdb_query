import datetime
import pandas as pd
import pprint
import time
import tweet_query
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import logging
import pytz
from sqlalchemy import create_engine, MetaData, Table, Column, Integer, String, DateTime, asc
from sqlalchemy.orm import Session

def test_query_counters():
    """ Test the query from remote"""
    start_time = time.time()
    start_date = datetime.datetime(2019, 9, 1)
    end_date = datetime.datetime(2021, 8, 31)
    freq = 'D'  # weekly frequency
    dates = pd.date_range(start_date, end_date, freq=freq, tz='US/Eastern')
    high_res = True

    anchor = "Vaccine"
    counters = tweet_query.counters.Counters(dates, anchor, high_res=high_res)
    counters._ambient_query()
    print(f'{anchor}: highres={high_res}')
    print(f"Data acquired in {time.time() - start_time:.2f} s")
    return counters

def test_query_counters_df():
    start_time = time.time()
    start_date = datetime.datetime(2019, 9, 1)
    end_date = datetime.datetime(2021, 8, 31)
    freq = 'D'  # weekly frequency
    dates = pd.date_range(start_date, end_date, freq=freq, tz='US/Eastern')
    high_res = True

    anchor = "Vaccine"
    counters = tweet_query.counters.Counters(dates, anchor, high_res=high_res)
    counters._ambient_query_pd()
    print(f'{anchor}: highres={high_res}')
    print(f"Data acquired in {time.time() - start_time:.2f} s")
    return counters

counters = test_query_counters()

counters1 = test_query_counters_df()

print(counters)
print('ok')

print(counters1)



print(counters.counters == counters1.counters)
