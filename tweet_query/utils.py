import os
import sys
import argparse
from bson import json_util
import gzip
import json
import time
import datetime
import pandas as pd


def save_json(fname, tweets):
    """ Save tweets to disk as .json.gz"""
    with gzip.open(fname,
                   'wt') as f:
        json.dump(tweets, f, default=json_util.default)


def load_json(fname):
    """ Load a gzipped json object of the tweets"""
    with gzip.open(fname,
                   'rt') as f:
        dict_list = json.load(f, object_hook=json_util.object_hook)
        return dict_list

def valid_date(s):
    try:
        return datetime.datetime.strptime(s, "%Y-%m-%d")
    except ValueError:
        msg = "Not a valid date: '{0}'.".format(s)
        raise argparse.ArgumentTypeError(msg)

def text_reader(fname):
    with open(fname, 'r') as f:
        word_list = [i.strip() for i in f.readlines()]
    return word_list