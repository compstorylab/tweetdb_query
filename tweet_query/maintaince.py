import time
import pytz
from dateutil import tz
import os
import gzip
import datetime
import logging
from sqlalchemy import create_engine, MetaData, Table, Column, Integer, String, DateTime, asc
from orm_classes import *

def connect():
    """ Connect to local database"""
    return create_engine('postgresql://postgres:roboctopus@localhost:5432/ambient')

def find_before(start_date=datetime.datetime(2020,1,1)):
    """ Find records"""
    """engine = connect()

    metadata = MetaData()
    metadata.reflect(bind=engine)

    records = metadata.tables["_records"]
    conn = engine.connect()

    query = records.select().filter(records.c.start_time < start_date)

    rows = conn.execute(query)
    print('ok')
    anchors = []
    for row in rows:
        anchors.append(row[0])"""
    session = db_session()
    ngram_table = ngram_table_mapper("trump", 'low')
    start_date = datetime.datetime(2020,1,1)

    for i in session.query(ngram_table).filter(ngram_table.timestamp < start_date):
        print(i.timestamp)


    return anchors

def remove_before(tablename):
    session = db_session()
    ngram_table = ngram_table_mapper(tablename=tablename)
    start_date = datetime.datetime(2021, 4, 1, tzinfo=tz.gettz('America/New_York'))

    # delete all before startdate
    session.query(ngram_table).filter(ngram_table.timestamp < start_date).delete()

    # update record table

    for i in session.query(Records).filter(Records.anchor_ngram == tablename):
        i.start_time = start_date
        print(i.anchor_ngram, i.start_time)
    # commit changes

    session.commit()

    

def fix_resolution_mismatch(anchor, start_date=datetime.datetime(2020,1,1)):
    """ Fix local database with """
    x = time.time()

    anchors = find_before(start_date)

    print(anchors)
    with open('to_change.txt', 'w') as f:
        [f.write(i+'\n') for i in anchors if i[-4:] == 'high']
    print(len(anchors))
    print(f"Executed in {time.time()-x:.2f}s")

#load list of table names to fix
with open('to_change.txt', 'r') as f:
    tables = f.read().splitlines()

for tablename in tables:
    remove_before(tablename)
#fix_resolution_mismatch(anchor)