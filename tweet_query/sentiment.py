import sys
from pathlib import Path
file = Path(__file__).resolve()
parent, root = file.parent, file.parents[1]
sys.path.append(str(root))

try:
    sys.path.remove(str(parent))
except ValueError:
    pass

try:
    import importlib.resources as pkg_resources
except ImportError:
    import importlib_resources as pkg_resources
import csv
import requests
import json
import datetime
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.dates import DateFormatter
import numpy as np
from multiprocessing import Pool
from pymongo.errors import AutoReconnect
from sklearn.feature_extraction.text import CountVectorizer

from tweet_query.tweet_db_query import ambient_ngrams_dataframe, assemble_ambient_ngrams, parse_ngrams_tweet


def without_keys(d, ex):
    try:
        d.pop(ex)
    except:
        pass
    return d

def type2freq_from_text(documents):
    """ From a list of str documents return a list of parsed ngram count dictionaries"""
    count = CountVectorizer(ngram_range=(1,1), stop_words="english").fit(documents)
    words = count.get_feature_names_out()


    t = count.transform(documents).astype(np.uint32)
    df = pd.DataFrame.sparse.from_spmatrix(t, columns=words)
    x = [r[r != 0].to_dict() for _, r in df.iterrows()]
    return x


def grab_hedonometer(start_date, end_date, lang='en'):
    """ Download hedonometer sentiment timeseries from hedometer website"""
    uri = f'http://hedonometer.org/api/v1/happiness/?format=json&timeseries__title={lang}_all&date__gte={start_date.strftime("%Y-%m-%d")}&date__lte={end_date.strftime("%Y-%m-%d")}'
    r = requests.get(uri)
    df = pd.DataFrame(json.loads(r.content)['objects'])
    df['date'] = pd.to_datetime(df['date'], format='%Y-%m-%d')
    df = df.sort_values('date')
    df = df.set_index(pd.DatetimeIndex(df['date']))
    df['happiness'] = pd.to_numeric(df['happiness'])
    df = df.tz_localize('US/Eastern')
    return df['happiness']


def load_happs_scores(lang='english_v2', ):
    """Load LabMT Sentiment vectors
    :param lang: Language name to load related scores"""
    with pkg_resources.path("pkg_data", f"labMT2{lang}.txt") as happs_pth:
        happs = pd.read_csv(happs_pth, sep='\t', engine='c', quoting=csv.QUOTE_NONE,)
    try:
        happs = happs.set_index('word')
        word2score = happs['happs'].to_dict()
    except KeyError:
        happs = happs.set_index('Word')
        word2score = happs['Havg'].to_dict()

    return word2score


def get_weighted_score(type2freq, type2score):
    """
    Calculate the average score of the system specified by the frequencies
    and scores of the types in that system
    Parameters
    ----------
    type2freq: dict
        keys are types and values are frequencies
    type2score: dict
        keys are types and values are scores
    Returns:
        s_avg: An float average weighted score of system
    """
    # Check we have a vocabulary to work with
    types = set(type2freq.keys()).intersection(set(type2score.keys()))
    if len(types) == 0:
        return
    # Get weighted score and total frequency
    f_total = sum([freq for t, freq in type2freq.items() if t in types])
    s_weighted = sum([type2score[t] * freq for t, freq in type2freq.items()
                      if t in types])
    s_avg = s_weighted / f_total
    return s_avg


def get_score_distribution(counter, type2score, count_type='count'):
    """ Calculate the distribution of ambient sentiment contributing to an anchor word's score
    :param counter: A Storyon Counter of the counts of each word
    :param type2score:
    :param count_type:
    :return: tuple of the component scores for a corpus and
    """
    # Check we have a vocabulary to work with
    sum_count = sum(i[count_type] for i in counter.values())
    type2freq = {key: value[count_type] / sum_count for key, value in counter.items()}

    types = set(type2freq.keys()).intersection(set(type2score.keys()))
    if len(types) == 0:
        return
    f_total = sum([freq for t, freq in type2freq.items() if t in types])
    s_weighted = np.array([type2score[t] * freq for t, freq in type2freq.items()
                           if t in types])
    base_scores = [type2score[t] for t, freq in type2freq.items()
                   if t in types]
    return base_scores, s_weighted / f_total


def get_weighted_score_np(type2freq, type2score):
    """
    Calculate the average score of the system specified by the frequencies
    and scores of the types in that system
    Parameters
    ----------
    type2freq: dict
        keys are types and values are frequencies
    type2score: dict
        keys are types and values are scores
    Returns
    -------
    s_avg: float
        Average weighted score of system
    s_std: float
        Standard deviation of score
    """
    # Check we have a vocabulary to work with
    types = set(type2freq.keys()).intersection(set(type2score.keys()))
    if len(types) == 0:
        return None, None
    freq = [freq for t, freq in type2freq.items() if t in types]
    scores = [type2score[t] for t, freq in type2freq.items() if t in types]

    # Get weighted score and std
    try:
        s_avg = np.average(scores, weights=freq)
        variance = np.average((scores - s_avg)**2, weights=freq)
        return s_avg, np.sqrt(variance)
    except ZeroDivisionError:
        return np.nan, np.nan



def counter_to_dict(counter, count_type):
    """ Convert a Counter object to a dictionary of type scores.

    Args:
        counter: An ngram counter object
        count_type: A String to specify the timeseries type.
             Choose from {'count', 'count_no_rt'}
    Returns:
        type2freq: A dictionary of counts
    """
    sum_count = sum(i[count_type] for i in counter.values())
    type2freq = {key: value[count_type] / sum_count for key, value in counter.items()}

    return type2freq


def filter_by_scores(type2freq, type2score, stop_lens):
    """ Loads a dictionary of type scores.

    Args:
        type2freq: dict
            keys are types, values are frequencies of those types
        type2score: dict
            keys are types, values are scores associated with those types
        stop_lens: iterable of 2-tuples
            denotes intervals that should be excluded when calculating shift scores
    Returns:
        type2freq_new: A dict of filtered word frequencies.
        type2score_new: A dict of filtered word scores.
    """
    type2freq_new = dict()
    type2score_new = dict()
    stop_words = set()
    for lower_stop, upper_stop in stop_lens:
        for t in type2score:
            if ((type2score[t] < lower_stop) or (type2score[t] > upper_stop)) \
                    and t not in stop_words:
                try:
                    type2freq_new[t] = type2freq[t]
                except KeyError:
                    pass
                type2score_new[t] = type2score[t]
            else:
                stop_words.add(t)

    return (type2freq_new, type2score_new, stop_words)


def query_sentiment_timeseries(word, dates, word2score, high_res=False):
    """ Queries an array of n-grams and
     evaluates sentiment over time based on labMT scores

     # ToDo: maybe depreciate. Functionality replaced by counters class?
    # Todo: Add support for saving or plotting inside here
    # Todo: Update this to use the counters class
     """
    queried = False
    while not queried:
        try:
            counters = assemble_ambient_ngrams(word, dates, case_sensitive=True, high_res=high_res)
            queried = True
            sentiment_array, ambient_counts = sentiment_timeseries(counters, word2score, anchor=word,
                                                                   count_type='count')
            return sentiment_array
        except AutoReconnect:
            pass


def multicore_sentiment(words, dates, word2score, high_res):
    """ Multicore runner for querying sentiment timeseries
    :param words: list of strings
    :param dates: pandas DateIndex
    :param word2score: sentiment score dictionary"""
    args = [(word, dates, word2score, high_res) for word in words]
    with Pool(2) as p:
        sentiment_results = p.starmap(query_sentiment_timeseries, args)
    print("Finished Query")
    return sentiment_results


def query_ambient_sentiment(word_i, dates=None, count_type="count_no_rt", stop_lens=[(4, 6)], lang='english'):
    """ Score a Storyons Counter object with a given sentiment dictionary, return score as a float

    Args:
        word_i: anchor word to score
        dates: date range to average sentiment score over
        count_type: `count` or `count_no_rt` to include all or only organic counts
        stop_lens: range of labMT words to discard for scoring.
        lang: a labMT language to select
            Choose from {'english', 'arabic', 'chinese', 'french', 'german', ...}

    Returns:
         labMT sentiment score np.array
    """
    if dates is None:
        begin_date1 = datetime.datetime(2018, 8, 11)
        end_date1 = datetime.datetime(2020, 1, 1)
        dates = pd.date_range(begin_date1, end_date1)
    dist_dict1 = ambient_ngrams_dataframe(word_i,
                                          dates,
                                          scheme=1,
                                          count_type=count_type,
                                          collection='drip_hose')

    word2freq = dist_dict1['freq'].to_dict()
    word2score = load_happs_scores(lang=lang)

    word2freq, word2score, stop_words = filter_by_scores(word2freq, word2score, stop_lens)

    return get_weighted_score_np(word2freq, word2score)


def counter_sentiment(counter, word2score, anchor=None, count_type='count', stop_lens=[(4, 6)]):
    """ Score a Storyons Counter object with a given sentiment dictionary, return score as a float

    Args:
        counter: storyon counters
        word2score: dictionary of labMT scores
        anchor: anchor word to add to stop words
        count_type: count or count_no_rt to include all or only organic counts
        stop_lens: range of labMT words to discard for scoring.

    Returns:
        pandas.DataFrame object for top target words
        count of n-grams used to score
        std of sentiment
    """

    # remove anchor word
    if anchor is not None:
        try:
            word2score.pop(anchor)
        except KeyError:
            pass
    # compute usage rate
    sum_count = sum(i[count_type] for i in counter.values()) + 1
    word2freq = {key: value[count_type] / sum_count for key, value in counter.items()}

    # apply stop word filter
    word2freq, word2score, stop_words = filter_by_scores(word2freq, word2score, stop_lens)

    # compute number of LabMT words used to compute the sentiment score
    norm_count = sum(value[count_type] for key,value in counter.items() if key in word2score)
    sentiment, std = get_weighted_score_np(word2freq, word2score)

    return sentiment, norm_count, std


def sentiment_timeseries(counters, word2score, anchor=None, count_type='count', stop_lens=[(4, 6)]):
    """ Score a Storyons Counter list with a given sentiment dictionary, return list of scores

    Args:
        counter: A storyon n-gram counter
        word2score: A dictionary of labMT scores
        anchor: An anchor word to add to stop words
        count_type: A count or count_no_rt to include all or only organic counts
        stop_lens: A range of labMT words to discard for scoring.

    Returns:
        A list of scores, and list of # of ngrams used to score"""

    sentiment = []
    ambient_counts = []
    std = []

    # replace counters object if necessary
    if count_type == 'count_rt':
        counters = counters.count_rt()

    for counter in counters:
        sentiment_i, count_i, std_i = counter_sentiment(counter, word2score, anchor, count_type, stop_lens)
        sentiment.append(sentiment_i)
        ambient_counts.append(count_i)
        std.append(std_i)

    return sentiment, ambient_counts, std


def ambient_sentiment_compare(counters1, counters2, titles):
    """ Compare ambient sentiment contributions

    Args:
        counters1: first list of counters to score
        counters2: second list of counters to score
        titles: length 3 list of title strings
    """
    word2score = load_happs_scores(lang='english')

    x1, y1 = get_score_distribution(np.sum(counters1), word2score)
    x2, y2 = get_score_distribution(np.sum(counters2), word2score)

    f = plt.figure(figsize=(12, 12))

    ax1 = plt.subplot(411)
    ax2 = plt.subplot(312)
    ax3 = plt.subplot(414, sharey=ax1)
    ax = [ax1, ax2, ax3]

    n, bins, patches = ax[0].hist(x1, weights=y1, bins=40)
    n1, bins1, patches1 = ax[2].hist(x2, weights=y2, bins=40)
    bins = np.convolve(bins, np.ones((2,)) / 2, mode='valid')
    ax[1].plot(bins, n - n1, 'o-')
    ax[1].axhline(color='k')
    ax[1].set_yscale('symlog', linthreshy=0.0015)
    ax[0].set_yscale('log')
    ax[2].set_yscale('log')
    plt.xlabel("LabMT Sentiment")

    for i in range(3):
        ax[i].set_title(titles[i])
        if i == 1:
            ax[i].text(-0.18, 0.5, "Sentiment \n Contribution \n Difference", ha='center',
                       verticalalignment='center', transform=ax[i].transAxes, fontsize=12, color='black')
            ax[i].text(-0.18, 0.1, "More \n Sentiment \n Contribution \n↓", ha='center',
                       verticalalignment='center', transform=ax[i].transAxes, fontsize=12, color='grey')
            ax[i].text(-0.18, 0.9, "↑ \n More \n Sentiment \n Contribution", ha='center',
                       verticalalignment='center', transform=ax[i].transAxes, fontsize=12, color='grey')
        ax[i].grid(axis='y')
    plt.tight_layout()
    plt.suptitle("Ambient Sentiment Distribution Compare")


def date_index(date, dates):
    """given a date and a pandas DatetimeIndex, return the index containing said date"""
    index = (dates > date) & (dates <= date + dates.freq)
    return np.argmax(index)


def main():
    word2score = load_happs_scores(lang='english')

    print(word2score)
    return word2score
    # define date range and resolution
    start_date = datetime.datetime(2020, 1, 1)
    end_date = datetime.datetime(2020, 9, 30)
    freq = 'D'  # weekly frequency
    dates = pd.date_range(start_date, end_date, freq=freq)

    words = ['#blm']
    print(words)

    # get sentiment timeseries
    high_res = False
    sentiment_results = multicore_sentiment(words, dates, word2score, high_res)
    for i, result in enumerate(sentiment_results):
        f, ax = plt.subplots()
        plt.plot(dates, result)
        plt.title(words[i], fontsize=18)
        formatter = DateFormatter('%b-%Y')
        ax.xaxis.set_tick_params(rotation=45)
        plt.gcf().axes[0].xaxis.set_major_formatter(formatter)
        ax.set_ylabel("Ambient Sentiment", fontsize=16)
        ax.set_xlabel("Date", fontsize=16)
        plt.savefig(f"../../plots/{words[i]}_ambient_sentiment_{high_res}.png")
        plt.show()


if __name__ == '__main__':
    main()
