import sys
import argparse
import subprocess
import datetime
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from bokeh.models import CustomJS, CrosshairTool
from bokeh.layouts import column
import bokeh
from bokeh.models.widgets import TextInput
from bokeh.models import ColorBar, LogColorMapper, LinearColorMapper, CategoricalColorMapper, CDSView, GroupFilter, BooleanFilter
from bokeh.plotting import figure, output_file, show, ColumnDataSource
from bokeh.io import show
from bokeh.models import CustomJS, RadioButtonGroup, OpenURL, TapTool, Panel, Tabs, Select, TextInput, DateRangeSlider, DateSlider
from bokeh.palettes import Viridis5, Category20
import itertools
from matplotlib.cm import ScalarMappable
from matplotlib.colors import Normalize, rgb2hex


def valid_date(s):
    try:
        return datetime.datetime.strptime(s, "%Y-%m-%d")
    except ValueError:
        msg = "Not a valid date: '{0}'.".format(s)
        raise argparse.ArgumentTypeError(msg)


def parse_args(args):
    parser = argparse.ArgumentParser(
        description="A command line interface for creating interactive ambient tweet embedding plots",
    )
    parser.add_argument(
        '-w', '--word',
        type=str,
        help='anchor to plot'
    )
    parser.add_argument(
        '-r', '--high_res',
        help="Flag to plot high res",
        action='store_true',
    )
    parser.add_argument(
        '-b', '--begin_date',
        help="beginning of date range -- formate YYYY-MM-DD",
        type=valid_date,
        default='2020-01-01',
    )
    parser.add_argument(
        '-c', '--copy',
        help="Flag to copy plot to w3 server",
        action='store_true',
    )
    parser.add_argument(
        '--location',
        type=str,
        default=None,
        help="Include a location feild, either state or city_state",
    )
    parser.add_argument(
        '-e', '--end_date',
        help="end of date range -- format YYYY-MM-DD",
        type=valid_date,
        default='2020-12-31'
    )
    parser.add_argument(
        '-s', '--sentiment',
        help="Flag to plot sentiment",
        action='store_true',
    )
    parser.add_argument(
        '-p', '--pth',
        help="path to load embedding data",
        default="/home/michael/projects/embeddings/data/"
    )
    parser.add_argument(
        '-l', '--labeled',
        help="Flag to plot labels",
        action='store_true',
    )
    parser.add_argument(
        '-f', '--fname',
        default=None,
        help='data to plot'
    )
    return parser.parse_args(args)

def load_data(fname):
    """ Load data"""
    df = pd.read_csv(fname, sep='\t', parse_dates=['tweet_created_at'],  lineterminator='\n')
    return df

def filter_NaT(df):
    index = pd.notnull(df['tweet_created_at'])
    df2 = df.loc[index]
    return df2

def get_fname(args):
    labeled_dict = {False: '',
                    True: "_labeled"}
    anchor, start_date, end_date, freq, high_res = (args.word, args.begin_date, args.end_date, 'D', args.high_res)
    formatting = lambda x: f'_{x}' if x is not None else ''
    loc_str = f'{formatting(args.location)}'
    try:
        fname = f"{args.pth}{anchor}_{start_date}_{end_date}_{freq}_{high_res}" \
            f"{loc_str}_top30000{labeled_dict[args.labeled]}.tsv"
    except FileNotFoundError:
        fname = f"{args.pth}{anchor}_{start_date}_{end_date}_{freq}_{high_res}" \
                f"{loc_str}_top40000{labeled_dict[args.labeled]}.tsv"
    return fname

def plot_embedding(df, outfile, args):
    """ Create the interactive html plot """
    output_file(outfile)
    plot_type = "month_colors"


    print(df.labels)
    df['time'] = [row.strftime("%m/%d/%Y, %H:%M:%S") if pd.notnull(row) else None for i, row in df['tweet_created_at'].iteritems()]

    color_fields = ['happs', 'hour', 'month'] # ToDo: date,
    color_mappers = []

    # Colors for hdbscan clusters
    c = bokeh.palettes.Category20[20]
    colors = [c[int(i) % 10] if int(i) != -1 else '#BDBDBD' for i in df.labels]
    df['hdbscan_clusters'] = colors

    colors = [c[int(i) % 10] if int(i) != -1 else '#BDBDBD' for i,lang in enumerate(df.lang)]
    df['lang_colors'] = colors


    color_mappers.append(
                LinearColorMapper(palette="Cividis256",
                                  low=min(df['happs'])+1,
                                  high=max(df['happs'])-1,
                                  )
                        )

    # Hour colors
    df['hour'] = [row.hour for i,row in df['tweet_created_at'].iteritems()]
    color_mappers.append(
                LinearColorMapper(palette="Viridis256",
                                  low=min(df['hour']),
                                  high=max(df['hour'])
                                  )
                        )

    # Month colors
    df['month'] = [row.month for i,row in df['tweet_created_at'].iteritems()]
    color_mappers.append(
                LinearColorMapper(palette="Viridis256",
                                  low=min(df['month']),
                                  high=max(df['month'])
                                  )
                        )

    source = ColumnDataSource(df)

    TOOLTIPS = """
        <div>
            <div>
                <span style="font-size: 10px;">@time UTC</span>
            </div>
             <div>
                <span style="font-size: 10px;">@id </span>
            </div>
            <div>
                <span style="font-size: 10px;">Language: @lang -- Verb: @verb</span>
            </div>
            <div>
                <span style="font-size: 12px;">Tweet Text:</span>
                <span style="font-size: 12px; font-weight: bold;">@Text</span>
            </div>
        </div>
    """
    figure_args = {
       'plot_width':700, 'plot_height':700,
       'tooltips':TOOLTIPS,
       'tools':"pan,wheel_zoom,box_zoom,reset,tap",
       'toolbar_location':"below",
       'title':f'Embedded Ambient Tweets -- Anchor: "{args.word}"',
    }

    tabs_list = []
    panels = []

    tabs_list.append( figure( plot_width=700, plot_height=700,
                       tooltips=TOOLTIPS,
                       tools="pan,wheel_zoom,box_zoom,reset,tap",
                       toolbar_location="below",
                       title=f'Embedded Ambient Tweets -- Anchor: "{args.word}"',
    ))

    clusters = CDSView(source=source, filters=[BooleanFilter([False if y == -1 else True for y in df.labels])])
    outliers = CDSView(source=source, filters=[BooleanFilter([True if y == -1 else False for y in df.labels])])
    size = 4
    tabs_list[0].circle('x', 'y', size=size, source=source,
                        fill_color='#BDBDBD', fill_alpha=0.3 ,
                        line_color='#000000', line_alpha=0.1, view=outliers)
    tabs_list[0].circle('x', 'y', size=size, source=source,
                        fill_color="hdbscan_clusters", fill_alpha=0.7 ,
                        line_color='#000000', line_alpha=0.1, view=clusters)
    panels.append(Panel(child=tabs_list[0], title='hdbscan clusters'))


    for i, color_field in enumerate(color_fields):
        tabs_list.append(
            figure(x_range=tabs_list[0].x_range,
                   y_range=tabs_list[0].y_range,
                   **figure_args)
        )

        tabs_list[i+1].circle('x', 'y', size=5, source=source,
                              fill_color={'field': color_field, 'transform': color_mappers[i]},
                              fill_alpha=0.7, line_color=None)

        color_bar = ColorBar(color_mapper=color_mappers[i], label_standoff=12)
        tabs_list[i+1].add_layout(color_bar, 'right')

        panels.append(Panel(child=tabs_list[i+1], title=color_field))


    # click to open link to tweet
    for p in tabs_list:
        url = "@link"
        taptool = p.select(type=TapTool)
        taptool.callback = OpenURL(url=url)

    print(tabs_list)
    print(len(tabs_list))
    show(Tabs(tabs=panels))
    return


def plot_embedding2(df, outfile, args):
    """ Create the interactive html plot """
    output_file(outfile)

    plot_type = "month_colors"

    df['time'] = [row.strftime("%m/%d/%Y, %H:%M:%S") for i, row in df['tweet_created_at'].iteritems()]

    color_fields = ['happs'] # ToDo: date,
    color_mappers = []

    # Colors for hdbscan clusters
    c = bokeh.palettes.Category20[20]
    colors = [c[int(i) % 20] if int(i) != -1 else '#BDBDBD' for i in df.labels]
    df['hdbscan_clusters'] = colors


    color_mappers.append(
                LinearColorMapper(palette="Cividis256",
                                  low=min(df['happs'])+1,
                                  high=max(df['happs'])-1,
                                  )
                        )

    # Hour colors
    df['hour'] = [row.hour for i,row in df['tweet_created_at'].iteritems()]
    color_mappers.append(
                LinearColorMapper(palette="Viridis256",
                                  low=min(df['hour']),
                                  high=max(df['hour'])
                                  )
                        )

    # Month colors
    df['month'] = [row.month for i,row in df['tweet_created_at'].iteritems()]
    color_mappers.append(
                LinearColorMapper(palette="Viridis256",
                                  low=min(df['month']),
                                  high=max(df['month'])
                                  )
                        )

    source = ColumnDataSource(df)

    TOOLTIPS = """
        <div>
            <div>
                <span style="font-size: 10px;">@time UTC</span>
            </div>
             <div>
                <span style="font-size: 10px;">@id </span>
            </div>
            <div>
                <span style="font-size: 12px;">Tweet Text:</span>
                <span style="font-size: 12px; font-weight: bold;">@Text</span>
            </div>
        </div>
    """
    figure_args = {
       'plot_width':1000, 'plot_height':1000,
       'tooltips':TOOLTIPS,
       'tools':"pan,wheel_zoom,box_zoom,reset,tap",
       'toolbar_location':"below",
       'title':f'Embedded Ambient Tweets -- Anchor: "{args.word}"',
    }

    p = figure(plot_width=1000, plot_height=1000,
               tooltips=TOOLTIPS,
               tools="pan,wheel_zoom,box_zoom,reset,tap",
               toolbar_location="below",
               title=f'Embedded Ambient Tweets -- Anchor: "{args.word}"',
               )
    color_field = 'happs'

    cir = p.circle('x', 'y', size=5, source=source,
             fill_color={'field': color_field, 'transform': color_mappers[0]},
             fill_alpha=0.7, line_color=None)

    color_bar = ColorBar(color_mapper=color_mappers[0], label_standoff=12)
    p.add_layout(color_bar, 'right')

    cb_cselect = bokeh.models.CustomJS(args=dict(cir=cir, csource=source,
                                                 color_bar=color_bar,
                                                ), code="""
        console.log('select: value=' + this.value, this.toString())                                       
        var selected_color = cb_obj.value;
        cir.glyph.fill_color.field = selected_color;
        var low = Math.min.apply(Math,csource.data[cb_obj.value]);
        var high = Math.max.apply(Math,csource.data[cb_obj.value]);
        
        color_bar.color_mapper.low = low;
        color_bar.color_mapper.high = high;
        color_bar.color_mapper.palette = palette;
        csource.change.emit();
    """)
    color_select = bokeh.models.Select(title="Select colors", value="happs",
                                       options=['happs', "month", 'hour'])
    color_select.js_on_change('value', cb_cselect)

    # click to open link to tweet
    url = "@link"
    taptool = p.select(type=TapTool)
    taptool.callback = OpenURL(url=url)
    p.axis.visible = False
    #p.grid.visible = False
    show(column(p, color_select))
    return


def plot_embedding3(df, outfile, args):
    """ Create the interactive html plot """
    output_file(outfile)

    plot_type = "month_colors"

    df['time'] = [row.strftime("%m/%d/%Y, %H:%M:%S") for i, row in df['tweet_created_at'].iteritems()]

    color_fields = ['happs']  # ToDo: date,
    color_mappers = []

    # Colors for hdbscan clusters
    c = bokeh.palettes.Category20[20]
    colors = [c[int(i) % 20] if int(i) != -1 else '#BDBDBD' for i in df.labels]
    df['hdbscan_clusters'] = colors


    # create happs colors

    sm = ScalarMappable(norm=Normalize(vmin=min(df['happs']), vmax=max(df['happs'])),
                        cmap='viridis')
    df['labMT Sentiment'] = [rgb2hex(sm.to_rgba(obs)) for obs in df['happs'].values]
    print(df['labMT Sentiment'])
    print(df['hdbscan_clusters'])

    """color_mappers.append(
        LinearColorMapper(palette="Cividis256",
                          low=min(df['happs']) + 1,
                          high=max(df['happs']) - 1,
                          )
    )"""


    # Hour colors
    df['hour'] = [row.hour for i, row in df['tweet_created_at'].iteritems()]
    color_mappers.append(
        LinearColorMapper(palette="Viridis256",
                          low=min(df['hour']),
                          high=max(df['hour'])
                          )
    )

    # Month colors
    df['month'] = [row.month for i, row in df['tweet_created_at'].iteritems()]
    color_mappers.append(
        LinearColorMapper(palette="Viridis256",
                          low=min(df['month']),
                          high=max(df['month'])
                          )
    )

    source = ColumnDataSource(df)

    TOOLTIPS = """
        <div>
            <div>
                <span style="font-size: 10px;">@time UTC</span>
            </div>
             <div>
                <span style="font-size: 10px;">@id </span>
            </div>
            <div>
                <span style="font-size: 12px;">Tweet Text:</span>
                <span style="font-size: 12px; font-weight: bold;">@Text</span>
            </div>
        </div>
    """
    figure_args = {
        'plot_width': 1000, 'plot_height': 1000,
        'tooltips': TOOLTIPS,
        'tools': "pan,wheel_zoom,box_zoom,reset,tap",
        'toolbar_location': "below",
        'title': f'Embedded Ambient Tweets -- Anchor: "{args.word}"',
    }

    p = figure(plot_width=1000, plot_height=1000,
               tooltips=TOOLTIPS,
               tools="pan,wheel_zoom,box_zoom,reset,tap",
               toolbar_location="below",
               title=f'Embedded Ambient Tweets -- Anchor: "{args.word}"',
               )
    color_field = 'labMT Sentiment'

    cir = p.circle('x', 'y', size=5, source=source,
                   fill_color={'field': color_field, 'transform': color_mappers[0]},
                   fill_alpha=0.7, line_color=None)

    color_bar = ColorBar(color_mapper=color_mappers[0], label_standoff=12)
    p.add_layout(color_bar, 'right')

    cb_cselect = bokeh.models.CustomJS(args=dict(cir=cir, csource=source,
                                                 color_bar=color_bar,
                                                 ), code="""
        console.log('select: value=' + this.value, this.toString())                                       
        var selected_color = cb_obj.value;
        cir.glyph.fill_color.field = selected_color;
        var low = Math.min.apply(Math,csource.data[cb_obj.value]);
        var high = Math.max.apply(Math,csource.data[cb_obj.value]);

        color_bar.color_mapper.low = low;
        color_bar.color_mapper.high = high;
        
        csource.change.emit();
    """)
    color_select = bokeh.models.Select(title="Select colors", value="happs",
                                       options=['happs', "month", 'hour'])
    color_select.js_on_change('value', cb_cselect)


    #color_select = TextInput(value="labMT Sentiment", title="Label:")
    #color_select.js_on_change("value", cb_cselect)
    # click to open link to tweet

    url = "@link"
    taptool = p.select(type=TapTool)
    taptool.callback = OpenURL(url=url)
    p.axis.visible = False
    # p.grid.visible = False
    show(column(p, color_select))
    return


def plot_embedding4(df, outfile, args):
    """ Create the interactive html plot """
    output_file(outfile)
    plot_type = "month_colors"


    print(df.labels)
    df['time'] = [row.strftime("%m/%d/%Y, %H:%M:%S") if pd.notnull(row) else None for i, row in df['tweet_created_at'].iteritems()]

    color_fields = ['happs', 'hour', 'month'] # ToDo: date,
    color_mappers = []

    # Colors for hdbscan clusters
    c = bokeh.palettes.Category20[20]
    colors = [c[int(i) % 10] if int(i) != -1 else '#BDBDBD' for i in df.labels]
    df['hdbscan_clusters'] = colors

    colors = [c[int(i) % 10] if int(i) != -1 else '#BDBDBD' for i,lang in enumerate(df.lang)]
    df['lang_colors'] = colors


    color_mappers.append(
                LinearColorMapper(palette="Cividis256",
                                  low=min(df['happs'])+1,
                                  high=max(df['happs'])-1,
                                  )
                        )

    # Hour colors
    df['hour'] = [row.hour for i,row in df['tweet_created_at'].iteritems()]
    color_mappers.append(
                LinearColorMapper(palette="Viridis256",
                                  low=min(df['hour']),
                                  high=max(df['hour'])
                                  )
                        )

    # Month colors
    df['month'] = [row.month for i,row in df['tweet_created_at'].iteritems()]
    color_mappers.append(
                LinearColorMapper(palette="Viridis256",
                                  low=min(df['month']),
                                  high=max(df['month'])
                                  )
                        )

    source = ColumnDataSource(df)

    TOOLTIPS = """
        <div>
            <div>
                <span style="font-size: 10px;">@time UTC</span>
            </div>
             <div>
                <span style="font-size: 10px;">@id </span>
            </div>
            <div>
                <span style="font-size: 10px;">Language: @lang -- Verb: @verb</span>
            </div>
            <div>
                <span style="font-size: 12px;">Tweet Text:</span>
                <span style="font-size: 12px; font-weight: bold;">@Text</span>
            </div>
        </div>
    """
    figure_args = {
       'plot_width':700, 'plot_height':700,
       'tooltips':TOOLTIPS,
       'tools':"pan,wheel_zoom,box_zoom,reset,tap",
       'toolbar_location':"below",
       'title':f'Embedded Ambient Tweets -- Anchor: "{args.word}"',
    }

    tabs_list = []
    panels = []

    tabs_list.append( figure( plot_width=700, plot_height=700,
                       tooltips=TOOLTIPS,
                       tools="pan,wheel_zoom,box_zoom,reset,tap",
                       toolbar_location="below",
                       title=f'Embedded Ambient Tweets -- Anchor: "{args.word}"',
    ))


    size = 4

    # date slider
    init_value = (df['tweet_created_at'].min(), df['tweet_created_at'].max())
    slider = DateRangeSlider(start=init_value[0], end=init_value[1], value=init_value)

    date_filter = BooleanFilter(booleans=[True] * df.shape[0])

    slider.js_on_change('value', CustomJS(args=dict(f=date_filter, ds=source),
                                          code="""\
                                              const [start, end] = cb_obj.value;
                                              f.booleans = Array.from(ds.data['tweet_created_at']).map(d => (d >= start && d <= end));
                                              // Needed because of https://github.com/bokeh/bokeh/issues/7273
                                              ds.change.emit();
                                          """))
    clusters = CDSView(source=source, filters=[BooleanFilter([False if y == -1 else True for y in df.labels]), date_filter])
    outliers = CDSView(source=source, filters=[BooleanFilter([True if y == -1 else False for y in df.labels]), date_filter])

    tabs_list[0].circle('x', 'y', size=size, source=source,
                        fill_color='#BDBDBD', fill_alpha=0.3 ,
                        line_color='#000000', line_alpha=0.1, view=outliers)
    tabs_list[0].circle('x', 'y', size=size, source=source,
                        fill_color="hdbscan_clusters", fill_alpha=0.7 ,
                        line_color='#000000', line_alpha=0.1, view=clusters)
    panels.append(Panel(child=column(tabs_list[0],slider), title='hdbscan clusters'))


    for i, color_field in enumerate(color_fields):
        tabs_list.append(
            figure(x_range=tabs_list[0].x_range,
                   y_range=tabs_list[0].y_range,
                   **figure_args)
        )

        tabs_list[i+1].circle('x', 'y', size=5, source=source,
                              fill_color={'field': color_field, 'transform': color_mappers[i]},
                              fill_alpha=0.7, line_color=None)

        color_bar = ColorBar(color_mapper=color_mappers[i], label_standoff=12)
        tabs_list[i+1].add_layout(color_bar, 'right')

        panels.append(Panel(child=tabs_list[i+1], title=color_field))


    # click to open link to tweet
    for p in tabs_list:
        url = "@link"
        taptool = p.select(type=TapTool)
        taptool.callback = OpenURL(url=url)

    print(tabs_list)
    print(len(tabs_list))
    show(Tabs(tabs=panels))
    return


def plot_embedding5(df, outfile, args):
    """ Create the interactive html plot with date slider """
    output_file(outfile)
    plot_type = "month_colors"


    print(df.labels)
    df['time'] = [row.strftime("%m/%d/%Y, %H:%M:%S") if pd.notnull(row) else None for i, row in df['tweet_created_at'].iteritems()]

    color_fields = ['hour', 'month'] # ToDo: date,
    color_mappers = []

    # Colors for hdbscan clusters
    c = bokeh.palettes.Category20[20]
    colors = [c[int(i) % 10] if int(i) != -1 else '#BDBDBD' for i in df.labels]
    df['hdbscan_clusters'] = colors

    colors = [c[int(i) % 10] if int(i) != -1 else '#BDBDBD' for i,lang in enumerate(df.lang)]
    df['lang_colors'] = colors

    if args.sentiment:
        color_fields = ['happs', 'hour', 'month']
        color_mappers.append(
                LinearColorMapper(palette="Cividis256",
                                  low=min(df['happs'])+1,
                                  high=max(df['happs'])-1,
                                  )
                            )

    # Hour colors
    df['hour'] = [row.hour for i,row in df['tweet_created_at'].iteritems()]
    color_mappers.append(
                LinearColorMapper(palette="Viridis256",
                                  low=min(df['hour']),
                                  high=max(df['hour'])
                                  )
                        )

    # Month colors
    df['month'] = [row.month for i,row in df['tweet_created_at'].iteritems()]
    color_mappers.append(
                LinearColorMapper(palette="Viridis256",
                                  low=min(df['month']),
                                  high=max(df['month'])
                                  )
                        )
    if args.location:
        color_fields.append(str(args.location))
        df[args.location] = [row for i, row in df[args.location].iteritems()]

        color_mappers.append(
            CategoricalColorMapper(palette=Category20[20],
                                    factors=list(df[args.location].unique()),
                                    )
        )
        print(args.location)
        print(color_mappers)

    source = ColumnDataSource(df)

    TOOLTIPS = """
        <div>
            <div>
                <span style="font-size: 10px;">@time UTC</span>
            </div>
             <div>
                <span style="font-size: 10px;">@id </span>
            </div>
            <div>
                <span style="font-size: 10px;">Language: @lang -- Verb: @verb</span>
            </div>
            <div>
                <span style="font-size: 12px;">Tweet Text:</span>
                <span style="font-size: 12px; font-weight: bold;">@Text</span>
            </div>
        </div>
    """
    figure_args = {
       'plot_width':700, 'plot_height':700,
       'tooltips':TOOLTIPS,
       'tools':"pan,wheel_zoom,box_zoom,reset,tap",
       'toolbar_location':"below",
       'title':f'Embedded Ambient Tweets -- Anchor: "{args.word}"',
    }

    tabs_list = []
    panels = []

    tabs_list.append( figure( plot_width=700, plot_height=700,
                       tooltips=TOOLTIPS,
                       tools="pan,wheel_zoom,box_zoom,reset,tap",
                       toolbar_location="below",
                       title=f'Embedded Ambient Tweets -- Anchor: "{args.word}"',
    ))


    size = 4

    # date slider
    init_value = (df['tweet_created_at'].min(), df['tweet_created_at'].max())
    slider = DateSlider(start=init_value[0], end=init_value[1], value=init_value[0], step=1 )

    date_filter = BooleanFilter(booleans=[True] * df.shape[0])

    slider.js_on_change('value', CustomJS(args=dict(f=date_filter, ds=source),
                                          code="""\
                                              const start = cb_obj.value;
                                              const end = start + 1000*3600*24*30*2
                                              f.booleans = Array.from(ds.data['tweet_created_at']).map(d => (d >= start && d <= end));
                                              // Needed because of https://github.com/bokeh/bokeh/issues/7273
                                              ds.change.emit();
                                          """))
    clusters = CDSView(source=source, filters=[BooleanFilter([False if y == -1 else True for y in df.labels]), date_filter])
    outliers = CDSView(source=source, filters=[BooleanFilter([True if y == -1 else False for y in df.labels]), date_filter])

    tabs_list[0].circle('x', 'y', size=size, source=source,
                        fill_color='#BDBDBD', fill_alpha=0.3 ,
                        line_color='#000000', line_alpha=0.1, view=outliers)
    tabs_list[0].circle('x', 'y', size=size, source=source,
                        fill_color="hdbscan_clusters", fill_alpha=0.7 ,
                        line_color='#000000', line_alpha=0.1, view=clusters)
    panels.append(Panel(child=column(tabs_list[0],slider), title='hdbscan clusters'))


    for i, color_field in enumerate(color_fields):
        tabs_list.append(
            figure(x_range=tabs_list[0].x_range,
                   y_range=tabs_list[0].y_range,
                   **figure_args)
        )

        tabs_list[i+1].circle('x', 'y', size=5, source=source,
                              fill_color={'field': color_field, 'transform': color_mappers[i]},
                              fill_alpha=0.7, line_color=None)

        color_bar = ColorBar(color_mapper=color_mappers[i], label_standoff=12)
        tabs_list[i+1].add_layout(color_bar, 'right')

        panels.append(Panel(child=tabs_list[i+1], title=color_field))


    # click to open link to tweet
    for p in tabs_list:
        url = "@link"
        taptool = p.select(type=TapTool)
        taptool.callback = OpenURL(url=url)

    print(tabs_list)
    print(len(tabs_list))
    show(Tabs(tabs=panels))
    return



def plot_embedding6(df, outfile, args):
    """ Create the interactive html plot with date slider """
    output_file(outfile)
    plot_type = "month_colors"


    print(df.labels)
    df['time'] = [row.strftime("%m/%d/%Y, %H:%M:%S") if pd.notnull(row) else None for i, row in df['tweet_created_at'].iteritems()]

    color_fields = [] # ToDo: date,
    color_mappers = []

    # Colors for hdbscan clusters
    c = bokeh.palettes.Category20[20]
    colors = [c[int(i) % 10] if int(i) != -1 else '#BDBDBD' for i in df.labels]
    df['hdbscan_clusters'] = colors

    colors = [c[int(i) % 10] if int(i) != -1 else '#BDBDBD' for i,lang in enumerate(df.lang)]
    df['lang_colors'] = colors

    if args.sentiment:
        color_fields = ['happs', 'month']
        color_mappers.append(
                LinearColorMapper(palette="Cividis256",
                                  low=min(df['happs'])+1,
                                  high=max(df['happs'])-1,
                                  )
                            )

    """ 
   # Hour colors
    df['hour'] = [row.hour for i,row in df['tweet_created_at'].iteritems()]
    color_mappers.append(
                LinearColorMapper(palette="Viridis256",
                                  low=min(df['hour']),
                                  high=max(df['hour'])
                                  )
                        )
    """
    # Month colors
    color_fields.append('month')

    df['month'] = [row.month for i,row in df['tweet_created_at'].iteritems()]
    color_mappers.append(
                LinearColorMapper(palette="Viridis256",
                                  low=min(df['month']),
                                  high=max(df['month'])
                                  )
                        )
    if args.location:
        state_locs_df = pd.read_csv('../pkg_data/state_centers.csv')
        if args.location == 'state':
            state_loc = "LONGITUDE"
            #state_loc = "LATITUDE"

            state_locs_dict = {row['state']: row[state_loc]
                               for i, row in state_locs_df[['state', state_loc]].iterrows()}
            df[state_loc] = [state_locs_dict[state] if state in state_locs_dict else None for state in df['state'].values]
            print(df[state_loc])
            color_fields.append(str(state_loc))

        else:
            raise NotImplementedError("Get city locations and save to pkg_data")

        #df[args.location] = [row for i, row in df[args.location].iteritems()]
        if state_loc == 'LONGITUDE':
            min_coord = max(-125, min(df[state_loc]))
            max_coord = min(-70, max(df[state_loc]))
        elif state_loc == 'LATITUDE':
            min_coord = max(45, min(df[state_loc]))
            max_coord = min(30, max(df[state_loc]))
        color_mappers.append(
            LinearColorMapper(palette='Viridis256',
                              low=min(df[state_loc]),
                              high=max(df[state_loc])
                             )
        )

    source = ColumnDataSource(df)

    TOOLTIPS = """
        <div>
            <div>
                <span style="font-size: 10px;">@time UTC</span>
            </div>
             <div>
                <span style="font-size: 10px;">@id </span>
            </div>
            <div>
                <span style="font-size: 10px;">Language: @lang -- Verb: @verb</span>
            </div>
            <div>
                <span style="font-size: 10px;">State: @state </span>
            </div>
            <div>
                <span style="font-size: 12px;">Tweet Text:</span>
                <span style="font-size: 12px; font-weight: bold;">@Text</span>
            </div>
        </div>
    """
    figure_args = {
       'plot_width':700, 'plot_height':700,
       'tooltips':TOOLTIPS,
       'tools':"pan,wheel_zoom,box_zoom,reset,tap",
       'toolbar_location':"below",
       'title':f'Embedded Ambient Tweets -- Anchor: "{args.word}"',
    }

    tabs_list = []
    panels = []

    tabs_list.append( figure( plot_width=700, plot_height=700,
                       tooltips=TOOLTIPS,
                       tools="pan,wheel_zoom,box_zoom,reset,tap",
                       toolbar_location="below",
                       title=f'Embedded Ambient Tweets -- Anchor: "{args.word}"',
    ))


    size = 4

    # date slider
    init_value = (df['tweet_created_at'].min(), df['tweet_created_at'].max())
    slider = DateSlider(start=init_value[0], end=init_value[1], value=init_value[0], step=1 )

    date_filter = BooleanFilter(booleans=[True] * df.shape[0])

    slider.js_on_change('value', CustomJS(args=dict(f=date_filter, ds=source),
                                          code="""\
                                              const start = cb_obj.value;
                                              const end = start + 1000*3600*24*30*2
                                              f.booleans = Array.from(ds.data['tweet_created_at']).map(d => (d >= start && d <= end));
                                              // Needed because of https://github.com/bokeh/bokeh/issues/7273
                                              ds.change.emit();
                                          """))
    clusters = CDSView(source=source, filters=[BooleanFilter([False if y == -1 else True for y in df.labels]), date_filter])
    outliers = CDSView(source=source, filters=[BooleanFilter([True if y == -1 else False for y in df.labels]), date_filter])

    tabs_list[0].circle('x', 'y', size=size, source=source,
                        fill_color='#BDBDBD', fill_alpha=0.3 ,
                        line_color='#000000', line_alpha=0.1, view=outliers)
    tabs_list[0].circle('x', 'y', size=size, source=source,
                        fill_color="hdbscan_clusters", fill_alpha=0.7 ,
                        line_color='#000000', line_alpha=0.1, view=clusters)
    panels.append(Panel(child=column(tabs_list[0],slider), title='hdbscan clusters'))


    for i, color_field in enumerate(color_fields):
        tabs_list.append(
            figure(x_range=tabs_list[0].x_range,
                   y_range=tabs_list[0].y_range,
                   **figure_args)
        )

        tabs_list[i+1].circle('x', 'y', size=5, source=source,
                              fill_color={'field': color_field, 'transform': color_mappers[i]},
                              fill_alpha=0.7, line_color=None)

        color_bar = ColorBar(color_mapper=color_mappers[i], label_standoff=12)
        tabs_list[i+1].add_layout(color_bar, 'right')

        panels.append(Panel(child=tabs_list[i+1], title=color_field))


    # click to open link to tweet
    for p in tabs_list:
        url = "@link"
        taptool = p.select(type=TapTool)
        taptool.callback = OpenURL(url=url)

    print(tabs_list)
    print(len(tabs_list))
    show(Tabs(tabs=panels))
    return

def main(args=None):
    if args is None:
        args = sys.argv[1:]
        args = parse_args(args)
        
    if args.fname is None:
        fname = get_fname(args)
    else:
        fname = args.fname
    #try:
    df = load_data(fname)
    #except:
    #    print('on no')
    #    return
    if df['Label'].isnull().values.all():
        print(df["Label"])
        df = df.drop(['Label'], axis=1)

    df.dropna(inplace=True)
    df = df.astype({'y': 'float64', 'x': 'float64'})
    df = filter_NaT(df)
    print(df.head())
    print(df.tail())

    outfile = f'/home/michael/projects/embeddings/hoverplots/{args.word}_{args.high_res}_{args.location}.html'
    outfile = outfile.replace('#', '!')
    plot_embedding6(df, outfile, args)

    if args.copy:
        subprocess.run(['scp', outfile, 'w3:www-root/tweet_embeddings/hoverplots/.'])



if __name__ == "__main__":
    main()