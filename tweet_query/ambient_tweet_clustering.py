from tweet_query.tweet_db_query import get_ambient_tweets, assemble_ambient_tweets
from tweet_query.sentiment import type2freq_from_text, load_happs_scores, get_weighted_score_np

from sentence_transformers import SentenceTransformer
import numpy as np
from sklearn.feature_extraction.text import CountVectorizer

import os
import sys
import argparse
import subprocess
from bson import json_util
import gzip
import json
import hdbscan
import umap
import time
import datetime
import pandas as pd
import matplotlib.pyplot as plt
from pprint import pprint
from multiprocessing import Pool

from tweet_query import ambient_embeddings
# Todo: adapt code to process in batches.

def c_tf_idf(documents, m, ngram_range=(1, 1)):
    count = CountVectorizer(ngram_range=ngram_range, stop_words="english").fit(documents)
    t = count.transform(documents).toarray()
    w = t.sum(axis=1)
    tf = np.divide(t.T, w)
    sum_t = t.sum(axis=0)
    idf = np.log(np.divide(m, sum_t)).reshape(-1, 1)
    tf_idf = np.multiply(tf, idf)

    return tf_idf, count

def extract_top_n_words_per_topic(tf_idf, count, docs_per_topic, n=20):
    words = count.get_feature_names()
    labels = list(docs_per_topic.Topic)
    tf_idf_transposed = tf_idf.T
    indices = tf_idf_transposed.argsort()[:, -n:]
    top_n_words = {label: [(words[j], tf_idf_transposed[i][j]) for j in indices[i]][::-1] for i, label in enumerate(labels)}
    return top_n_words

def extract_topic_sizes(df):
    topic_sizes = (df.groupby(['Topic'])
                     .Doc
                     .count()
                     .reset_index()
                     .rename({"Topic": "Topic", "Doc": "Size"}, axis='columns')
                     .sort_values("Size", ascending=False))
    return topic_sizes

def print_tweets(tweets):

    for tweet in tweets:
        for key, value in tweet.items():
            if key == 'rt_text' or \
                    key == 'pure_text' or \
                    key == 'tweet_created_at':
                pprint(f"{key}: {tweet[key]}", compact=True)

        print()


def label_tweets(tweets):

    for tweet in tweets:
        for key, value in tweet.items():
            if key == 'rt_text' or \
                    key == 'pure_text' or \
                    key == 'tweet_created_at':
                pprint(f"{key}: {tweet[key]}", compact=True)

        print()


def filter_retweets(tweets):
    """ Remove retweets"""
    return [tweet for tweet in tweets if 'verb' in tweet and tweet['verb'] == 'post']

def filter_NaT(df):
    index = pd.notnull(df['tweet_created_at'])
    df2 = df[index]
    df2 = df2.reset_index(drop=True)
    return df2

def embed_tweets(tweets, count_type='pure_text'):
    #model = SentenceTransformer('paraphrase-MiniLM-L6-v2')
    model = SentenceTransformer('all-mpnet-base-v2')

    #Our sentences we like to encode
    # filter QT
    tweets = [tweet for tweet in tweets if 'verb' in tweet and tweet['verb'] == 'post']

    text = [tweet[count_type] for tweet in tweets if count_type in tweet]
    #Sentences are encoded by calling model.encode()
    embeddings = model.encode(text)

    #Print the embeddings
    printing=False
    if printing:
        for sentence, embedding in zip(text, embeddings):
            print("Sentence:", sentence)
            print("Embedding:", embedding)
            print("")
    return embeddings, text


def umap_embedding(embeddings, tweets, fname, args, n_components=16, min_cluster_size=30, cluster_method='eom'):
    umap_embeddings = umap.UMAP(n_neighbors=15,
                                n_components=n_components, min_dist=0,
                                metric='cosine').fit_transform(embeddings)

    cluster = hdbscan.HDBSCAN(min_cluster_size=min_cluster_size,
                              metric='euclidean',
                              cluster_selection_method=cluster_method).fit(umap_embeddings)

    umap_data = umap.UMAP(densmap=True, n_neighbors=15, n_components=2, min_dist=0, metric='cosine',random_state=42).fit_transform(embeddings)
    result = pd.DataFrame(umap_data, columns=['x', 'y'])


    result['labels'] = cluster.labels_


    # add tweet_text
    tweets2 = [tweet for tweet in tweets if 'pure_text' in tweet]
    result['lang'] = [tweet['fastText_lang'].split(':')[-1] for tweet in tweets2 ]
    result['verb'] = [tweet['verb'] if 'verb' in tweet else None for tweet in tweets2]
    result['link'] = [tweet['link'] for tweet in tweets2]
    result['id'] = [tweet['id'] for tweet in tweets2]



    #result['pure_text'] = [tweet['pure_text'] for tweet in tweets2]
    result['Text'] = [tweet['pure_text'] for tweet in tweets2]

    # add sentiment score
    if args.sentiment:
        count_type = 'pure_text'
        type2freq_list = type2freq_from_text(result['Text'].values)
        word2score = load_happs_scores()
        happs,happs_std = zip(*[get_weighted_score_np(type2freq, word2score) for type2freq in type2freq_list])
        result['happs'] = happs

    if args.location:
        result[args.location] = [tweet[args.location] if args.location in tweet else None for tweet in tweets2]


    # ToDo: add ousimeter scores

    # add timestamp
    result['tweet_created_at'] = [tweet['tweet_created_at'] for tweet in tweets2]



    result = filter_NaT(result)
    print(result)
    result['Label'] = None

    # add embeddings
    columns = [f"x_{i}" for i in range(embeddings.shape[1])]
    embeddings_df = pd.DataFrame(embeddings, columns=columns)
    result = pd.concat([result,
                        embeddings_df],
                       axis=1)
    # save
    result.loc[:, ~result.columns.isin(columns)].to_csv(args.pth+os.path.basename(fname)+'.tsv', sep='\t')

    # shuffle and subset if too large
    max_tweets = 30000
    frac = min(max_tweets / result.shape[0] , 1)
    result = result.sample(frac=frac).reset_index(drop=True)
    print(result.shape)

    result.to_csv(args.pth + os.path.basename(fname) + '+embeddings.tsv', sep='\t')

    # save_data
    result.loc[:, ~result.columns.isin(columns)].to_csv(args.pth+os.path.basename(fname)+'_top30000.tsv', sep='\t')
    result.loc[:1000, ~result.columns.isin(columns)].to_csv(args.pth+os.path.basename(fname)+'_top1000.tsv', sep='\t')





    # Visualize clusters
    if args.plot == 'quick':
        fig, ax = plt.subplots(figsize=(20, 10))
        outliers = result.loc[result.labels == -1, :]
        clustered = result.loc[result.labels != -1, :]
        plot_type = 'd'
        if plot_type == 'time_color':
            dates = [tweet['tweet_created_at'].timestamp() for tweet in tweets if 'pure_text' in tweet]
            plt.scatter(result.x, result.y, c=dates, s=0.1, cmap='viridis')

        elif plot_type == 'happs_color':
            plt.scatter(result.x, result.y, c=happs, s=0.1, cmap='viridis')

        else:
            plt.scatter(outliers.x, outliers.y, color='#BDBDBD', s=1)
            plt.scatter(clustered.x, clustered.y, c=clustered.labels, s=1, cmap='hsv_r')

        plt.colorbar()
        plt.title(n_components)
        fname = args.pth + f'/figures/min_cluster_size/{n_components}_{min_cluster_size}_{cluster_method}_{os.path.basename(fname).split(".")[0]}.png'
        plt.savefig(fname)
    return cluster


def save_json(fname, tweets):
    """ Save tweets to disk as .json.gz"""
    with gzip.open(fname,
                   'wt') as f:
        json.dump(tweets, f, default=json_util.default)


def load_json(fname):
    """ Load a gzipped json object of the tweets"""
    with gzip.open(fname,
                   'rt') as f:
        dict_list = json.load(f, object_hook=json_util.object_hook)
        return dict_list


def grab_ambient_tweets(anchor, args):
    """ Get tweets"""
    start_date, end_date, high_res = (args.begin_date, args.end_date, args.high_res)
    start_time = time.time()

    freq = 'D'  # daily frequency
    dates = pd.date_range(start_date, end_date, freq=freq)
    formatting = lambda x: f'_{x}' if x is not None else ''
    loc_str = f'{formatting(args.location)}'
    fname = args.pth +f"/{anchor}_{start_date}_{end_date}_{freq}_{high_res}" \
            f"{loc_str}.json.gz"

    if os.path.isfile(fname):
        tweets = load_json(fname)
    else:
        if args.location:
            tweets = assemble_ambient_tweets(anchor, dates, high_res=high_res, project=False, limit=0,
                                             db='tweet_segmented_location',
                                             hostname='serverus.cems.uvm.edu',
                                             port=27015,
                                             )

        else:
            tweets = assemble_ambient_tweets(anchor, dates, high_res=high_res, project=False, limit=0)
        print(f" Num Tweets: {len(tweets)}")
        save_json(fname, tweets)

    print(f"Data acquired for '{anchor}' in {time.time() - start_time:.2f} s")

    fname = fname.split('.')[0]
    return tweets, fname


def test_n_components(word, high_res=False):
    """ """
    tweets, fname = grab_ambient_tweets(word, high_res=high_res)
    filtered_tweets = filter_retweets(tweets)
    embeddings, text = embed_tweets(filtered_tweets)

    dimensions = [i**2 for i in range(2,7)]
    for i in dimensions:
        cluster = umap_embedding(embeddings, filtered_tweets, fname,
                                 min_cluster_size=30,
                                 n_components=i, cluster_method='eom')
    return


def test_min_cluster_size(word, high_res=False):
    """ """
    tweets, fname = grab_ambient_tweets(word, high_res=high_res)
    filtered_tweets = filter_retweets(tweets)
    embeddings, text = embed_tweets(filtered_tweets)

    dimensions = [i**2 for i in range(2,7)]
    dimensions = [i**2 for i in range(2,7)]

    for i in dimensions:
        for j in dimensions:
            cluster = umap_embedding(embeddings, filtered_tweets,
                                     fname, min_cluster_size=i,
                                     n_components=j,cluster_method='eom')
    return


def valid_date(s):
    try:
        return datetime.datetime.strptime(s, "%Y-%m-%d")
    except ValueError:
        msg = "Not a valid date: '{0}'.".format(s)
        raise argparse.ArgumentTypeError(msg)

def text_reader(fname):
    with open(fname, 'r') as f:
        word_list = [i.strip() for i in f.readlines()]
    return word_list


def parse_args(args):
    parser = argparse.ArgumentParser(
        description="A command line interface for creating interactive ambient tweet embedding plots",
    )
    parser.add_argument(
        '-w', '--word',
        type=str,
        help='anchor to plot'
    )
    parser.add_argument(
        '-l', '--word_list',
        type=text_reader,
        help='textfile of words to plot'
    )
    parser.add_argument(
        '-r', '--high_res',
        help="Flag to plot high res",
        action='store_true',
    )
    parser.add_argument(
        '-s', '--sentiment',
        help="Flag to plot sentiment",
        action='store_true',
    )
    parser.add_argument(
        '--location',
        type=str,
        default=None,
        help="Include a location feild, either state or city_state",
    )
    parser.add_argument(
        '-c', '--cached',
        help="Flag to load precomputed embeddings",
        action='store_true',
    )
    parser.add_argument(
        '-b', '--begin_date',
        help="beginning of date range -- formate YYYY-MM-DD",
        type=valid_date,
        default='2020-01-01',
    )
    parser.add_argument(
        '-e', '--end_date',
        help="end of date range -- format YYYY-MM-DD",
        type=valid_date,
        default='2020-12-31'
    )
    parser.add_argument(
        '-p', '--pth',
        help="path to save embedding data",
        default="/home/michael/projects/embeddings/data/"
    )
    parser.add_argument(
        '--plot', choices=['quick', 'web'],
        help="make a quick plot or and interactive html file",
        default="web"
    )
    parser.add_argument(
        '--merge',
        help="Flag to merge labeled data in",
        action='store_true'
    )
    return parser.parse_args(args)

#test_min_cluster_size('Flu', False)

words = ['Tokyo', 'Delhi', 'Shanghai', 'São Paulo', 'Mexico City', 'Cairo', 'Mumbai', 'Beijing', 'Dhaka', 'Osaka',
         'New York', 'Karachi', 'Buenos Aires', 'Chongqing', 'Istanbul', 'Kolkata', 'Manila', 'Lagos',
         'Rio de Janeiro', 'Tianjin', 'Kinshasa', 'Guangzhou', 'Los Angeles', 'Moscow', 'Shenzhen', 'Lahore',
         'Bangalore', 'Paris', 'Bogotá', 'Jakarta', 'Chennai', 'Lima', 'Bangkok', 'Seoul', 'Nagoya', 'Hyderabad',
         'London', 'Tehran', 'Chicago', 'Chengdu', 'Nanjing', 'Wuhan', 'Ho Chi Minh City', 'Luanda', 'Ahmedabad',
         'Kuala Lumpur', 'Hong Kong', 'Dongguan', 'Hangzhou', 'Foshan', 'Shenyang', 'Riyadh', 'Baghdad',
         'Santiago', 'Surat', 'Madrid', 'Suzhou', 'Pune', 'Harbin', 'Houston', 'Dallas', 'Toronto', 'Dar es Salaam',
         'Miami', 'Belo Horizonte', 'Singapore', 'Philadelphia', 'Atlanta', 'Fukuoka', 'Khartoum', 'Barcelona',
         'Johannesburg', 'Saint Petersburg', 'Qingdao', 'Dalian', 'Washington', 'Yangon',
         'Alexandria', 'Jinan', 'Guadalajara']
#words = ['Stroke', 'Diabetes', 'Covid', 'Flu', 'Pneumonia', 'Fentanyl', 'Overdose', 'Car Accident', 'Bus', 'Bike', 'Transit', 'Parking', 'Speeding', 'DUI', 'Suicide', 'Cancer', 'Heart Disease', 'Heart Attack', 'Alzheimer', 'Alzheimers', "Alzheimer's", 'Kidney', 'Dialysis', 'Smoking', 'Alcohol', 'Gun', 'Guns']


def create_embeddings(anchor, args):
    """ """
    if not args.cached:
        #try:
        #if args.random:
        if False:
            pass
        #    tweets, fname = grab_tweets(args)
        else:
            tweets, fname = grab_ambient_tweets(anchor, args)
        #except :
        #    print(f'{anchor} - error tokenizing data, buffer overflow caught')
        #    return
        filtered_tweets = filter_retweets(tweets)
        print(len(filtered_tweets))
        if len(filtered_tweets) > 73_000:
            if args.sentiment:
                print('quitting')
                return

        embeddings, text = embed_tweets(filtered_tweets)
        try:
            cluster = umap_embedding(embeddings, filtered_tweets, fname, args)
        except TypeError:
            pass

    if args.plot == 'web':
        args.word = anchor
        args.copy = True
        ambient_embeddings.main(args)


def main(args=None):
    """Create"""
    if args is None:
        args = sys.argv[1:]
    args = parse_args(args)
    print(args)

    if args.word:
        create_embeddings(args.word, args)

    elif args.word_list:
        map_args = [(word, args) for word in args.word_list]
        for arg in map_args:
            create_embeddings(*arg)


    elif args.test:
        return

if __name__ == "__main__":
    main()
"""
anchor = 'Los Angeles'
tweets,fname  = grab_ambient_tweets(anchor)

filtered_tweets = filter_retweets(tweets)
embeddings, text = embed_tweets(filtered_tweets)
cluster = umap_embedding(embeddings, filtered_tweets, fname)
print(anchor)
exit()

docs_df = pd.DataFrame(text, columns=["Doc"])
docs_df['Topic'] = cluster.labels_
docs_df['Doc_ID'] = range(len(docs_df))
docs_per_topic = docs_df.groupby(['Topic'], as_index = False).agg({'Doc': ' '.join})

tf_idf, count = c_tf_idf(docs_per_topic.Doc.values, m=len(tweets))

top_n_words = extract_top_n_words_per_topic(tf_idf, count, docs_per_topic, n=20)
topic_sizes = extract_topic_sizes(docs_df);
print(topic_sizes.head(10))

for i,x in enumerate(topic_sizes['Topic'].values):
    if i < 30 and x >0:
        print(x)
        print(top_n_words[x][:10])
print(len(tweets))
"""
#
