import os
import sys
from pathlib import Path

import sqlalchemy.exc

file = Path(__file__).resolve()
parent, root = file.parent, file.parents[1]
sys.path.append(str(root))

try:
    sys.path.remove(str(parent))
except ValueError:
    pass

try:
    import importlib.resources as pkg_resources
except ImportError:
    import importlib_resources as pkg_resources

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime, timedelta
from dateutil import tz
import pytz
import json
import time
import gzip
import logging

import tweet_query
from tweet_query.counter import NgramCounter
from tweet_query.tweet_db_query import time_tag, extract_timeseries, date_index, assemble_ambient_ngrams, top_n_timeseries
from tweet_query.sentiment import load_happs_scores, counter_to_dict, filter_by_scores, sentiment_timeseries, get_weighted_score_np
from tweet_query.sentiment_plot import general_sentiment_shift, sentiment_timeseries_plot, sentiment_timeseries_plot_v2, sentiment_timeseries_plot_simple,sentiment_variance_ridge
#from tweet_query.ambient_rd_figures import ambient_rank_divergence

from sqlalchemy import create_engine, MetaData, Table, Column, Integer, String, DateTime, asc, inspect, text

#  ToDo: implement elementwise addition and subtraction for counter class
#  ToDo: make a Superclass Counters and a Class Ambient counters that inherits
#   from it for different kinds of queries
#  ToDo: Make a remote query option to avoid local timeout
# ToDo: add options to plot in different timezones
# Todo: add new labmt reference
# todo: General statistics function for ambient query
#  (number of tweets over time)
#  (average number of tweets)
#  (Percentiles max min)
#  (Variance)
#  check what functions thayer has for language database

pwd = 'roboctopus'
tz_dict = {
    'UTC': tz.gettz('UTC'),
    'EST': tz.gettz('America/New_York')
}
pytz_dict = {
    'UTC': 'UTC',
    'EST': 'America/New_York'
}

truth_dict = {
    False: 'low',
    True: 'high',
    'Full': 'Full',
}

lang_dict = {
            'en': 'english_v2',
            'ru': 'russian',
            'de': 'german',
            'es': 'spanish',
            'fr': 'french',
            'ko': 'korean',
            'id': 'indonesian',
            'ps': 'pashto',
            'pt': 'portuguese',
            'ur': 'urdu',
            'ukr': 'ukraine_from_russian',
            'ar': 'arabic',
            'ms': 'malay_from_english',
            'fa': 'farsi_from_english',
            'fi': 'finnish_from_english',
            'it': 'italian_from_english',
            'pl': 'polish_from_english',
            'tl': 'tagalog_from_english',
            'nl': 'dutch_from_english',
        }

def matching_dates(dates):
    return all(date.equals(dates[0]) for date in dates)


def connect():
    """ Connect to local database"""
    return create_engine('postgresql://postgres:roboctopus@localhost:5432/ambient')


def database_exists():
    try:
        engine = connect()
        _ = engine.table_names()
    except:
        pass
        return False

    return True


def logging_init(fname='logs/ambient_sentiment_query.log'):
    """ Initialize logging for query """
    logger = logging.getLogger('ambientQueryLogger')
    logger.setLevel(logging.DEBUG)
    if not os.path.exists(fname):
        os.makedirs(os.path.dirname(fname))

    fh = logging.FileHandler(filename=fname)
    fh.setLevel(logging.DEBUG)
    fh.setFormatter(logging.Formatter('%(asctime)s -- %(message)s'))
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    logger.addHandler(fh)
    logger.addHandler(ch)


def combine(counters_list, omit_anchor=True):
    """ Given a list of counters, aggregate elementwise, and return a new counter class object

    # ToDo: have this return a new counter object which has a list of anchors as an attribute.
    Args:
        counters_list: A list of counters
    """

    # Test that dates are the same
    dates = [counter_i.dates for counter_i in counters_list]
    assert matching_dates(dates)
    date_index = counters_list[0].dates

    if omit_anchor:
        for i, counter_i in enumerate(counters_list):
            del counters_list[i][counter_i.anchor]

    agg_counters = []

    for d_index, date in enumerate(date_index[:-2]):
        aligned_counters = [ambient_counter_i.counters[
                                ambient_counter_i.dates.get_loc(date)
                            ]
                            for ambient_counter_i in counters_list
                            if (date in ambient_counter_i.dates) and (
                                    len(ambient_counter_i.counters) > ambient_counter_i.dates.get_loc(date))]
        try:
            agg_counters.append(np.sum(aligned_counters,
                                       initial=NgramCounter({})
                                       ))
        except TypeError:
            print("Aligned Counters:")
            print(aligned_counters)

            # if (date in ambient_counter_i.dates) and (len(ambient_counter_i.counters) > ambient_counter_i.dates.get_loc(date))

    counter = Counters(date_index,
                       anchor="",
                       counters=agg_counters,
                       scheme=counters_list[0].scheme,
                       high_res=counters_list[0].high_res
                       )
    counter.freq = date_index.freq
    return counter


class Counters:
    """ Class to work with array of storyon counter objects.

    Attributes:
            dates: A pandas.DatatimeIndex date index for counters
            anchor: A str anchor word used to query
            counters: A list of n-grams counter objects for each time interval
            scheme: An int specifying what n-grams to parse
            high_res: A bool flag to search from `driphose` (False) or `ten_percent_sample` (True)
                Higher resolution only available for 2020 on.
            lang: A str FastText language code for initial query
            case_sensitive: A bool to query on a case sensitive anchor
            pth: An optional path for local cache
    """

    def __init__(self,
                 dates,
                 anchor,
                 counters=None,
                 scheme=1,
                 high_res=False,
                 lang='en',
                 case_sensitive=False,
                 pth=None,
                 timezone='UTC',
                 ):

        """ Initialize Counters object with all metadata needed to reproduce query """
        if pth is None:
            root_dir = os.path.dirname(os.path.abspath(__file__))
            pth = os.path.join(root_dir, "../data/ambient/")

        assert '/' not in anchor, "Please don't query for links, it messes with the save path"
        freq = dates.freq
        if dates.tz is None:
            dates = dates.tz_localize(tz_dict[timezone])

        self.timezone = timezone
        self.freq = freq
        self.dates = dates
        self.anchor = anchor
        self.scheme = scheme
        self.high_res = high_res

        high_res_start_date = datetime(2019, 9, 1, tzinfo=tz_dict['UTC'])
        if high_res == True:
            assert self.dates[0] >= high_res_start_date, \
            "10% sample data begins at 2019-09-01, make sure 'start_date' is after this."

        self.lang = lang
        self.case_sensitive = case_sensitive
        self.table_name = f"{self.anchor.lower()}_res_{truth_dict[self.high_res]}"
        if case_sensitive is False:
            anchor = self.anchor.lower()
        if lang == 'en':
            self.fname = f"{pth}{anchor}_{self.dates[0].strftime('%Y-%m-%d')}" \
                     f"_{self.dates[-1].strftime('%Y-%m-%d')}" \
                     f"_ten_percent-{self.high_res}_freq_{str(self.freq)}_ngrams_{self.scheme}.json"
        else:
            self.fname = f"{pth}{anchor}_{self.dates[0].strftime('%Y-%m-%d')}" \
                         f"_{self.dates[-1].strftime('%Y-%m-%d')}" \
                         f"_ten_percent-{self.high_res}_freq_{str(self.freq)}_lang_{lang}_ngrams_{self.scheme}.json"

        if counters is not None:
            self.counters = counters
        return

    def __delitem__(self, key):
        for i, counter_i in enumerate(self.counters):
            try:
                del self.counters[i][key]
            except KeyError:
                pass

    def __repr__(self):
        rep = f"Counters object: \n" \
              f"    Anchor - {self.anchor} \n" \
              f"    Dates - {self.dates[0]} to {self.dates[-1]}\n" \
              f"    Scheme - {self.scheme}grams\n" \
              f"    Lang - {self.lang}\n" \
              f"    Save Path - {self.fname}"
        return rep

    def _ambient_insert(self, subcounters=None):
        """ Insert counters from database

        Args:
            subcounters: a tuple of a counters subset and corresponding dates to insert
        """
        if subcounters is None:
            counters = self.counters
            dates = self.dates
            update = False
        else:
            counters, dates = subcounters
            update = True

        engine = connect()

        metadata = MetaData()

        ambient_table = Table(f'{self.table_name}', metadata,
                              Column('ngram', String),
                              Column('timestamp', DateTime),
                              Column('count', Integer),
                              Column('count_no_rt', Integer)
                              )
        records = Table(f'_records', metadata,
                        Column('anchor_ngram', String),
                        Column('start_time', DateTime(timezone=True), default=datetime.utcnow),
                        Column('end_time', DateTime(timezone=True), default=datetime.utcnow)
                        )

        metadata.create_all(engine)

        ins = ambient_table.insert()

        conn = engine.connect()

        for i, counter_i in enumerate(counters):
            conn.execute(ins,
                         [{'ngram': key,
                           'timestamp': dates[i].to_pydatetime(),
                           'count': value['count'],
                           'count_no_rt': value['count_no_rt'],
                           }
                          for key, value in counter_i.items()]
                         )
        conn.close()

        exists = self._record_exists()
        if not exists:
            ins = records.insert()

            conn = engine.connect()
            conn.execute(ins,
                         [{'anchor_ngram': self.table_name,
                           'start_time': dates[0].to_pydatetime(),
                           'end_time': dates[-1].to_pydatetime(),
                           }]
                         )
            conn.close()

        if update:
            start_time = min(self.dates[0].to_pydatetime(), dates[0].to_pydatetime())
            end_time = max(self.dates[-1].to_pydatetime(), dates[1].to_pydatetime())
            upd = records.update(). \
                where(records.c.anchor_ngram == self.table_name). \
                values(start_time=start_time,
                       end_time=end_time)

            conn = engine.connect()
            conn.execute(upd)
            conn.close()

    def _infer_endpoints(self):

        engine = connect()

        metadata = MetaData()
        metadata.reflect(bind=engine)

        ambient_table = metadata.tables[self.table_name]
        conn = engine.connect()

        results = ambient_table.select(). \
            where(ambient_table.c.ngram == 'the'). \
            order_by(asc(ambient_table.c.timestamp))

        x = list(conn.execute(results))

        conn.close()
        return x[0].timestamp, x[-1].timestamp + timedelta(days=2)

    def _record_exists(self):
        """ Check if a record entry exists for this word"""
        engine = connect()

        metadata = MetaData()
        metadata.reflect(bind=engine)

        records = metadata.tables["_records"]
        conn = engine.connect()

        query = records.select().where(records.c.anchor_ngram == self.table_name)

        rows = conn.execute(query)

        index = 0
        for row in rows:
            start_time, end_time = row._mapping[records.c.start_time], row._mapping[records.c.end_time]
            index += 1

        if index == 0:
            conn.close()
            return False
        else:
            conn.close()
            return True
        conn.close()

    def _ambient_update(self):
        """ Add new data for new dates to an ambient ngram table and it's entry in the records table"""

        engine = connect()

        query = text(
            f"""SELECT * FROM "_records" WHERE _records.anchor_ngram = '{self.table_name}';""")

        """metadata = MetaData()
        metadata.reflect(bind=engine)

        records = metadata.tables["_records"]
        conn = engine.connect()"""
        conn = engine.connect()
        #query = records.select().where(records.c.anchor_ngram == self.table_name)

        rows = conn.execute(query)

        index = 0
        for row in rows:
            start_time, end_time = row[1], row[2]
            index += 1

        if index == 0:
            conn.close()
            self.remove_db_entry()
            return

        if index > 1:
            conn.close()
            self.remove_db_entry()
            return

        # make subcounter and dates
        before = self.dates < start_time
        after = self.dates > end_time

        before_subcounters, after_subcounters = [], []
        # insert older data
        if len(self.dates[before]) > 0:
            print(f"Querying {len(self.dates[before])} older days from remote...")
            before_subcounters = self.query_update(self.dates[before])
            self._ambient_insert(subcounters=(before_subcounters, self.dates[before]))

        # insert newer data
        if len(self.dates[after]) > 0:
            print(f"Querying {len(self.dates[after])} newer days from remote...")
            after_subcounters = self.query_update(self.dates[after])
            self._ambient_insert(subcounters=(after_subcounters, self.dates[after]))

        conn.close()
        return before_subcounters, after_subcounters

    def _ambient_query_pd(self):
        """ Query existing counters from local database using pandas and update self.counters"""
        engine = connect()
        # need to read start and end dates from records table
        query = f"SELECT * FROM \"_records\" WHERE anchor_ngram = '{self.table_name}'"
        records = pd.read_sql(
            query,
            con=engine,
            parse_dates=['start_time', 'end_time'],
        )

        # convert dates to UTC
        try:
            start_date_records = records['start_time'].dt.tz_convert(tz_dict['UTC'])[0].to_pydatetime()
            end_date_records = records['end_time'].dt.tz_convert(tz_dict['UTC'])[0].to_pydatetime()
        except TypeError:
            try:
                start_date_records = records['start_time'].dt.tz_localize(tz_dict['UTC'])[0].to_pydatetime()
                end_date_records = records['end_time'].dt.tz_localize(tz_dict['UTC'])[0].to_pydatetime()
                print("_ambient_query_pd datetime localized")
                pass
            except IndexError:
                print(self.anchor)
                print(records['start_time'].dt.tz_localize(tz_dict['UTC']))
                pass

        # if select correct startpoint
        if start_date_records <= self.dates[0]:
            # if our local database has an earlier first date, select query start date
            start_date = self.dates[0].tz_convert(tz_dict['UTC']).to_pydatetime()
        else:  # otherwise, start local database query from the earliest available date
            start_date = start_date_records
        if end_date_records > self.dates[-1]:  # if local database has later records, select query end timestamp
            end_date = self.dates[-1].tz_convert(tz_dict['UTC']).to_pydatetime()
            # (self.dates[-1] is the endpoint, last database timestamp will be self.dates[-2])
        else:
            end_date = end_date_records

        date_index = pd.date_range(start_date, end_date, freq='D')

        # query local database for dates in the records range
        query = f"SELECT * FROM \"{self.table_name}\" WHERE timestamp >= '{start_date.strftime('%Y-%m-%d')}' AND timestamp <='{end_date.strftime('%Y-%m-%d')}'"
        df = pd.read_sql(
            query,
            con=engine,
            parse_dates=['timestamp'],
        )

        # split into list by timestamp
        df_list = [df[
                       (df['timestamp'] >= date_index[i].to_pydatetime().replace(tzinfo=None))
                       & (df['timestamp'] < date_index[i + 1].to_pydatetime().replace(tzinfo=None))
                       ]
                   for i, date in enumerate(date_index[:-1])]

        _ = [df_i.set_index('ngram', inplace=True) for df_i in df_list]
        try:
            self.load_from_df(df_list)
        except ValueError as e:
            print(e)
            self.remove_db_entry()
            print(f"Removing localdb entry for {self.anchor}. Try Again.")
            sys.exit(0)

    def _ambient_query(self):
        """ query existing counters from local database and update self.counters"""
        engine = connect()

        metadata = MetaData()
        metadata.reflect(bind=engine)

        ambient_table = metadata.tables[self.table_name]
        # records = metadata.tables['_records']
        conn = engine.connect()

        query = ambient_table.select() \
            .where(ambient_table.c.timestamp >= self.dates[0], ambient_table.c.timestamp <= self.dates[-1]) \
            .order_by(asc(ambient_table.c.timestamp))

        results = conn.execute(query)
        # convert query results into counters list
        counters = [{}]

        datetimeindex = pd.date_range(self.dates[0], self.dates[-1], freq=self.freq)

        utc = pytz.timezone('UTC')
        index = 0
        for row in results:
            new_day = utc.localize(row._mapping[ambient_table.c.timestamp])

            # add a new counter until we get to the correct date index
            while new_day > datetimeindex[index]:
                index += 1
                counters.append({})

            # update a counter with this word at timestamp _index
            counters[index][row._mapping[ambient_table.c.ngram]] = \
                {'count': row._mapping[ambient_table.c.count],
                 'count_no_rt': row._mapping[ambient_table.c.count_no_rt],
                 }

        self.counters = [NgramCounter(i)
                         for i in counters]
        conn.close()
        return

    def remove_db_entry(self):
        """ Remove an ambient table and record from the records table"""
        engine = connect()

        metadata = MetaData()
        metadata.reflect(bind=engine)

        ambient_table = metadata.tables[self.table_name]
        conn = engine.connect()

        # drop table
        try:
            ambient_table.drop(engine)
        except sqlalchemy.exc.NoSuchTableError:
            print(f'No {self.table_name} to delete...')
            conn.close()
            pass
        # remove row in records
        records = metadata.tables["_records"]

        to_delete = records.delete() \
            .where(records.c.anchor_ngram == self.table_name)

        conn.execute(to_delete)
        conn.close()
        return

    def save(self, json=False, localdb=True):
        """ Try to save to database, else save to json"""
        if json:
            self.save_json()
            print(f"Saved json at {self.fname}")

        elif localdb:
            engine = connect()

            # check if table exists
            if inspect(engine).has_table(self.table_name):
                # if exists, do nothing
                pass

            # update ambient table for times outside end points
            elif database_exists():
                # if table doesn't exist create
                print("Inserting counters for first time.")
                self._ambient_insert()

        else:
            self.save_json()
            print(f"Saved json at {self.fname}")

    def load_db(self, caching=False):
        """ Try to load from database"""

        engine = connect()
        # check if table exists
        if inspect(engine).has_table(self.table_name):
            # query for dates on either side of database
            before, after = self._ambient_update()
            if caching:
                return

            # get counters from database
            self._ambient_query_pd()

            # assemble new counters
            self.counters = before + self.counters + after

    def save_json(self):
        """ Save a counter object to disk as .json.gz"""
        with gzip.open(self.fname + ".gz",
                       'wt') as f:
            json.dump(self.counters, f)

    def load_json(self):
        """ Load a gzipped json object of the counters"""
        with gzip.open(self.fname + '.gz',
                       'rt') as f:
            dict_list = json.load(f)
            self.counters = [NgramCounter(i)
                             for i in dict_list]

    def load_from_df(self, df_list):
        """ Load a counters object from a list of pandas dataframes

        Args:
            df_list: A list of Pandas DataFrames containing word counts
        """
        self.counters = [NgramCounter(df[['count', 'count_no_rt']].to_dict(orient="index"))
                         for df in df_list]

    def get(self, save=False, json=False, caching=False, localdb=True, clear=False):
        """ Try to load, if not available query

        Args:
            save: A bool flag to save data if queried from remote server.
        """
        start_time = time.time()
        logger = logging.getLogger('ambientQueryLogger')
        engine = connect()
        if clear:
            self.remove_db_entry()
            
        loaded = False
        if os.path.isfile(self.fname + '.gz'):
            logger.info(f"Loading file from: \n {self.fname}\n \n ")
            self.load_json()
            logger.info(f"Loaded {self.anchor} from local cache in {time.time() - start_time:.2f}s")
            loaded = True

        elif localdb and inspect(engine).has_table(self.table_name):
                #engine.dialect.has_table(engine, self.table_name):
            # skip local database loading if just caching
            logger.info(f"Loading {self.anchor} from counters db...")
            self.load_db(caching=caching)
            if caching:
                logger.info(f"Already cached. Verified in {time.time() - start_time:.2f}s")
                return None
            else:
                logger.info(f"Loaded {self.anchor} from counters db in {time.time() - start_time:.2f}s")

        else:
            logger.info(f"Querying {self.anchor} - [{self.lang}] from tweet db...")
            self.query()
            logger.info(f"Returned {self.anchor} from tweet db in {time.time() - start_time:.2f}s")

        if save:
            self.save(json=json, localdb=localdb)

    def query(self):
        """ Query the tweet database for new ambient counters"""
        try:
            self.counters = assemble_ambient_ngrams(f'{self.anchor}',
                                                    self.dates,
                                                    self.scheme,
                                                    self.lang,
                                                    self.high_res,
                                                    self.case_sensitive
                                                    )
        except KeyError as e:
            print(e)
            print(f"{self.fname}")
            print('\nIf you specified high_res="Full" '
                  ',and see this error message, '
                  'the full res data file cant be found by the system. \n'
                  'Make sure the data is in the correct location.\n')
            raise KeyError

    def query_update(self, dates):
        """ Query the tweet database for missing dates in the counters database"""
        counters = assemble_ambient_ngrams(f'{self.anchor}',
                                           dates,
                                           self.scheme,
                                           self.lang,
                                           self.high_res,
                                           self.case_sensitive
                                           )
        return counters

    def collapse(self, counters=None):
        """ Sum an array of counters and return a single counter"""
        if counters is None:
            counters = self.counters
        collapsed_counter = np.sum(counters, initial=NgramCounter({}))
        return collapsed_counter

    def aggregate(self, freq, full=True, inplace=True):
        """ Function to aggregate counters into longer time intervals

        Args:
            freq: freqency to pass to pandas.date_range
                Allowable values of freq:

                {'S', 'T', 'H', 'D', 'W', 'M', 'Y'}
                secondly, minutely, hourly, daily, weekly, monthly and yearly frequency.

                Multiples are also supported, i.e. '12H' for a half daily frequency.

                https://pandas.pydata.org/pandas-docs/stable/user_guide/timeseries.html#timeseries-offset-aliases

            full: Boolean to discard empty ending date range if True,
                Otherwise keep as is.

        """
        dates = pd.date_range(self.dates[0], self.dates[-1], freq=freq)
        old_dates = self.dates
        assert len(dates) > 1, "Selected frequency will collapse to one interval"

        assert (dates[1] - dates[0]) > (old_dates[1] - old_dates[0]), \
            "Must aggregate to a lower frequency"

        assert len(self.counters) <= len(old_dates), "Dates mismatch"

        counters = {i: NgramCounter({}) for i in dates[:-1]}

        c_i = np.zeros(len(dates))

        for i, counter_i in enumerate(self.counters):

            # clip data outside the new date range
            # x > o and
            if old_dates[i] < dates[-1] and old_dates[i] >= dates[0]:
                counters[time_tag(dates, old_dates[i])] += counter_i
                c_i[date_index(old_dates[i], dates)] += 1

        # convert counters from dict to list
        counters = [i for i in counters.values()]

        # cut off last time interval if not full
        if full:
            if c_i[-1] < c_i[0]:
                dates = dates[:-1]
                counters = counters[:-1]
        
        if inplace:
            self.dates = dates
            self.counters = counters
            self.freq = self.dates.freq

            assert len(self.dates) == len(self.counters) + 1, f"Date index mismatch! \n" \
                                                          f"Try to increment your end date by one\n {self.dates}"
            return
        else:
            return dates,

    def date_index(self, date):
        """ returns counter index for a given date"""
        return date_index(date, self.dates)

    def count_rt(self):
        """ returns 'count_rt' list for each counter in a counters list

        By default an n-gram counter stores only
        counts for all tweets and organic tweets,
        this computes the n-gram counts found only in retweets.

        Returns:
            A list of count_rt dictionaries
        """
        count_rt = []
        for i, counter_i in enumerate(self.counters):
            count_rt.append({})
            for key, value in counter_i.items():
                count_rt[i][key] = {'count_rt': value['count'] - value['count_no_rt']}

        return count_rt

    def to_series(self, word, count_type='count'):
        """ Returns a Pandas Series of the count of a single word with a datetime index

            Args:
                word: A string to extract from the counter object
                count_type: A String to specify the timeseries to return.
                Choose from {'freq', 'freq_no_rt', 'count', 'count_no_rt'}

            Returns:
                Pandas Series object with DatetimeIndex
        """
        return extract_timeseries(
            word,
            self.counters,
            self.dates,
            data_type=count_type)

    def to_timeseries_dataframe(self, count_type='count', N=1000):
        """ Returns a Pandas DataFrame of the count of the top N with a datetime index

            Args:
                count_type: A String to specify the timeseries to return.
                Choose from {'freq', 'freq_no_rt', 'count', 'count_no_rt'}
                N: Top N n-grams to include.
                    N = -1 to return all. However, this is expensive, and not recommended.

            Returns:
                Pandas Series object with DatetimeIndex
        """
        return top_n_timeseries(
            self.counters,
            self.dates[:-1],
            N=N,
            data_type=count_type)

    def to_zipf_dataframe(self, index):
        """ Returns a Pandas DataFrame of a single counter's word usage rate and the rank

        Args:
            index: An integer to select which counter to convert
        """
        return pd.DataFrame(self.counters[index]).T

    def happs_series(self, anchor, count_type='count', lang='en'):
        """Returns a Pandas Dataframe of the Sentiment timeseries with a datetime index

        Args:
            anchor: Specifies the anchor word, which is removed from the words used to score sentiment
            count_type: A String to specify the timeseries to return.
            Choose from {'freq', 'freq_no_rt', 'count', 'count_no_rt'}

        Returns:
            A tuple of Pandas DataFrames.
            The first entry is sentiment scores for each date range.
            The second entry is the count of n-grams used to take the measurement.
            The third is the standard error in the sentiment
        """
        word2score = load_happs_scores(lang=lang_dict[lang])
        sentiment_array = sentiment_timeseries(self.counters, word2score, anchor=anchor, count_type=count_type)
        self.happs_values, self.happs_counts, self.happs_std = pd.DataFrame(sentiment_array[0],
                                                                            index=self.dates[:len(self.counters)],
                                                                            columns=[anchor]
                                                                            )[anchor], \
                                                               pd.DataFrame(sentiment_array[1],
                                                                            index=self.dates[:len(self.counters)],
                                                                            columns=[anchor]
                                                                            )[anchor], \
                                                               pd.DataFrame(sentiment_array[2],
                                                                            index=self.dates[:len(self.counters)],
                                                                            columns=[anchor]
                                                                            )[anchor]

        return pd.DataFrame(sentiment_array[0],
                            index=self.dates[:len(self.counters)],
                            columns=[anchor]
                            ), \
               pd.DataFrame(sentiment_array[1],
                            index=self.dates[:len(self.counters)],
                            columns=[anchor]
                            ), \
               pd.DataFrame(sentiment_array[2],
                            index=self.dates[:len(self.counters)],
                            columns=[anchor]
                            )

    def plot_ambient_timeseries(self, word, ax=None, count_type='freq', logy=True, alone=False, label=None):
        """ Plot a timeseries of usage within the ambient corpus

        Args:
            word: The word to be plotted and for the Title
            ax: Optional; A matplotlib Axis if this will be part of a larger multi-axis figure
            count_type: A String to specify the timeseries type.
             Choose from {'freq', 'freq_no_rt', 'count', 'count_no_rt'}
            logy: A bool to choose to log scale the y axis
        """
        label_dict = {

            'count': "Counts",
            'count_no_rt': "Counts (no retweets)",
            'freq': "Usage Rate",
            'freq_no_rt': "Usage Rate (no retweets)"
        }

        if ax is None:
            alone = True
            f, ax = plt.subplots(1, 1, figsize=(8, 5))
        data = extract_timeseries(word, self.counters,
                                  self.dates,
                                  data_type=count_type
                                  )
        min = np.min(data.values[np.nonzero(data.values)])

        data[data != 0].plot(ax=ax)
        ax.plot(data[data == 0].index, data[data == 0] + min / 3, '.', color=ax.get_lines()[-1].get_color(), ms=1)

        ax.set_ylabel(label_dict[count_type])
        ax.set_title(f'Ambient ngram: "{word}"---Anchor ngram: "{self.anchor}"')

        if logy is True:
            ax.set_yscale("symlog", linthresh=min)

        if alone:
            plt.show()

        return ax

    def plot_sentiment_timeseries(self,
                                  fig=None,
                                  count_type='count',
                                  ylims=(5, 7.0),
                                  unsafe=False,
                                  count_log=True,
                                  lang='en',
                                  save_pth=None,
                                  hedonometer=True,
                                  label='',
                                  color=None,):
        """ A method to plot the ambient sentiment timeseries

        Args:
            fig: Optional;  A matplotlib tuple output of plt.subplots (figure, axs)
            count_type: A String to specify the timeseries type.
             Choose from {'freq', 'freq_no_rt', 'count', 'count_no_rt'}
            ylims: ylims for sentiment timeseries
            unsafe: flag to allow plotting even when bins have less than 1000 words.
             Don't use unsafe plots for papers!
        """
        # ToDo: make sure timezones line up for reference twitter sentiment timeseries
        # todo: make timezone ET
        df, count_df, std_df = self.happs_series(self.anchor, count_type=count_type, lang=lang)

        if not unsafe:
            assert count_df[self.anchor].median() > 1000, "Aggregate further for meaningful sentiment measurements"

        axs = sentiment_timeseries_plot_v2(df, count_df, std_df, self.anchor, fig=fig, ylims=ylims,
                                           count_log=count_log, lang=lang,
                                           count_type=count_type, hedonometer=hedonometer, label=label, color=color
                                           )
        
        if save_pth:
            count_df['sentiment'] = df[self.anchor]
            count_df['std'] = std_df[self.anchor]
            count_df['counts'] = count_df[self.anchor]
            count_df = count_df[['sentiment', 'counts', 'std']]

    
            fname = f"{self.anchor.lower()}_sentiment_timeseries_{self.dates[0].strftime('%Y-%m-%d')}" \
                    f"_{self.dates[-1].strftime('%Y-%m-%d')}" \
                    f"_freq_{str(self.freq)}_lang_{lang}.tsv"
            if type(save_pth) == str:
                fname = save_pth + fname
            else:
                fname = f'../data/sentiment/' + fname
            count_df.to_csv(fname, sep='\t', index=True)
            
        return axs

    def plot_sentiment_timeseries_simple(self,
                                         fig=None,
                                         count_type='count',
                                         ylims=(5, 7.0),
                                         unsafe=False,
                                         errors=True,
                                         **kwargs):
        """ A method to plot the ambient sentiment timeseries

        Args:
            fig: Optional;  A matplotlib tuple output of plt.subplots (figure, axs)
            count_type: A String to specify the timeseries type.
             Choose from {'freq', 'freq_no_rt', 'count', 'count_no_rt'}
            ylims: ylims for sentiment timeseries
            unsafe: flag to allow plotting even when bins have less than 1000 words.
             Don't use unsafe plots for papers!
        """
        # ToDo: make sure timezones line up for reference twitter sentiment timeseries
        # todo: make timezone ET
        df, count_df, std_df = self.happs_series(self.anchor, count_type=count_type)

        if not unsafe:
            assert count_df[self.anchor].median() > 1000, "Aggregate further for meaningful sentiment measurements"

        axs = sentiment_timeseries_plot_simple(df, count_df, std_df, self.anchor, fig=fig, ylims=ylims, errors=errors, **kwargs)
        print(df.min(), df.index.values[df.argmin()])
        print(df.max(), df.index.values[df.argmax()])


        return axs

    def plot_sentiment_shift_dates(self,
                                   date1,
                                   date2,
                                   count_type='count',
                                   titles=None,
                                   type2score_1='labMT_English',
                                   stop_lens=[(4, 6)],
                                   timezone="UTC",
                                   lang='en',
                                   ):
        """ Compare two counters within the same counters object, indexed by date

        Args:
            date1: A datetime index for first counter
            date2: A datetime index for second counter
            count_type: A String to specify the timeseries type.
             Choose from {'freq', 'freq_no_rt', 'count', 'count_no_rt'}
        """
        type2score = load_happs_scores(lang=lang_dict[lang])
        if date1.tzinfo is None:
            date1 = pytz.timezone(pytz_dict['UTC']).localize(date1)
        if date2.tzinfo is None:
            date2 = pytz.timezone(pytz_dict['UTC']).localize(date2)
        # look up index by date
        date1_index = date_index(date1, self.dates)
        date2_index = date_index(date2, self.dates)

        # format selected date counters
        type2freq_1 = counter_to_dict(self.counters[date1_index], count_type)
        type2freq_2 = counter_to_dict(self.counters[date2_index], count_type)

        # filter
        type2freq_1, type2score_new1, stop_words = filter_by_scores(type2freq_1, type2score, stop_lens)
        type2freq_2, type2score_new2, stop_words = filter_by_scores(type2freq_2, type2score, stop_lens)

        # set default title
        if titles is None:
            titles = [f"Anchor:  {self.anchor} \n {date1.strftime('%Y-%m-%d')}", f"{date2.strftime('%Y-%m-%d')}"]

        try:
            reference_value = get_weighted_score_np(type2freq_1)
            general_sentiment_shift(type2freq_1, type2freq_2, titles=titles, type2score_1=type2score,
                                    reference_value=reference_value, stop_lens=stop_lens)
        except ZeroDivisionError:
            print('No ngrams counted for this time rage. Please select another counter, or check your dates')
            pass


    def plot_allotax_shift_dates(
                                self,
                                date1,
                                date2,
                                count_type='count',
                                titles=None,
                                timezone="UTC",
                                matlab='matlab -nosplash -nodesktop',
                                filter=None,

                                ):
            """ Compare two counters within the same counters object, indexed by date

            Args:
                date1: A datetime index for first counter
                date2: A datetime index for second counter
                count_type: A String to specify the timeseries type.
                 Choose from {'freq', 'freq_no_rt', 'count', 'count_no_rt'}
            """
            if date1.tzinfo is None:
                date1 = pytz.timezone(pytz_dict[timezone]).localize(date1)
            if date2.tzinfo is None:
                date2 = pytz.timezone(pytz_dict[timezone]).localize(date2)
            # look up index by date
            date1_index = date_index(date1+timedelta(hours=5), self.dates)
            date2_index = date_index(date2+timedelta(hours=5), self.dates)

            # format selected date counters
            type2freq_1 = self.counters[date1_index]
            type2freq_2 = self.counters[date2_index]


            # set default title
            if titles is None:
                titles = [f"Anchor:  {self.anchor} \n {date1.strftime('%Y-%m-%d')}", f"{date2.strftime('%Y-%m-%d')}"]

            print((self.dates[date1_index], self.dates[date1_index+1]))
            print((self.dates[date2_index], self.dates[date2_index + 1]))
            tweet_query.ambient_rd_figures.ambient_rank_divergence(type2freq_1,
                                    type2freq_2,
                                    self.anchor,
                                    self.anchor,
                                    (self.dates[date1_index], self.dates[date1_index+1]),
                                    (self.dates[date2_index], self.dates[date2_index+1]),
                                    matlab=matlab,)


    def plot_sentiment_shift_changepoint(self,
                                         date1,
                                         lang='en',
                                         count_type='count',
                                         titles=None,
                                         range_indexes=None,
                                         timezone="UTC",
                                         stop_lens=[(4, 6)],
                                         top_n=30,
                                         ax=None,
                                         ):
        """ Compare a selected counter within the same counters object to the average, indexed by date
        Args:
            date1: A datetime index for first counter
            count_type: A String to specify the timeseries type.
             Choose from {'freq', 'freq_no_rt', 'count', 'count_no_rt'}
             range_indexes: None or a tuple. e.g. range_indexes=( datetime(2020,1,1), datetime(2021,1,1) )
        """
        type2score = load_happs_scores(lang=lang_dict[lang])

        if date1.tzinfo is None:
            date1 = pytz.timezone(pytz_dict['UTC']).localize(date1)

        # look up index by date
        date1_index = date_index(date1, self.dates)
        if range_indexes is not None:
            start_index = date_index(range_indexes[0], self.dates)
            end_index = date_index(range_indexes[0], self.dates)
        else:
            start_index = 0
            end_index = -1


        # format selected date counters
        type2freq_1 = counter_to_dict(self.collapse(self.counters[start_index : date1_index]), count_type)
        type2freq_2 = counter_to_dict(self.collapse(self.counters[date1_index : end_index]), count_type)

        # filter
        type2freq_1, type2score_new1, stop_words = filter_by_scores(type2freq_1, type2score, stop_lens)
        type2freq_2, type2score_new2, stop_words = filter_by_scores(type2freq_2, type2score, stop_lens)

        # set default title
        if titles is None:
            if lang == 'en':
                titles = [f"Anchor:  {self.anchor} \n Changepoint: {date1.strftime('%Y-%m-%d')}\n Before", f"After"]
            else:
                titles = [f"Anchor:  {self.anchor} [{lang}] \n Changepoint: {date1.strftime('%Y-%m-%d')}\n Before", f"After"]

        try:
            reference_value = get_weighted_score_np(type2freq_1)
            general_sentiment_shift(type2freq_1, type2freq_2, titles=titles, type2score_1=type2score,
                                    reference_value=reference_value, stop_lens=stop_lens, top_n=top_n, ax=ax)
        except ZeroDivisionError:
            print('No ngrams counted for this time rage. Please select another counter, or check your dates')
            pass


    def plot_allotax_shift_changepoint(self,
                                       change_date,
                                       count_type='count',
                                       titles=None,
                                       range_indexes=None,
                                       timezone="UTC",
                                       matlab='matlab',
                                       ):
            """ Compare a selected counter within the same counters object to the average, indexed by date

            Args:
                change_date: A datetime index for first counter
                count_type: A String to specify the timeseries type.
                 Choose from {'freq', 'freq_no_rt', 'count', 'count_no_rt'}
                 range_indexes: None or a tuple. e.g. range_indexes=( datetime(2020,1,1), datetime(2021,1,1) )
            """
            if change_date.tzinfo is None:
                change_date = pytz.timezone(pytz_dict[timezone]).localize(change_date)

            # look up index by date
            date1_index = date_index(change_date, self.dates)
            if range_indexes is not None:
                start_index = date_index(range_indexes[0], self.dates)
                end_index = date_index(range_indexes[1], self.dates)
                start_date, end_date = range_indexes
            else:
                start_index = 0
                end_index = -1
                start_date, end_date = self.dates[0], self.dates[-1]

            # format selected date counters
            type2freq_1 = self.collapse(self.counters[start_index: date1_index])
            type2freq_2 = self.collapse(self.counters[date1_index: end_index])

            # set default title
            if titles is None:
                titles = [f"Anchor:  {self.anchor} \n Changepoint: {change_date.strftime('%Y-%m-%d')}\n Before", f"After"]

            tweet_query.ambient_rd_figures.ambient_rank_divergence(type2freq_1,
                                    type2freq_2,
                                    self.anchor,
                                    self.anchor,
                                    (start_date, change_date),
                                    (change_date, end_date),
                                    matlab=matlab, )


    def plot_sentiment_shift_vs_collapsed(self,
                                          date1,
                                          count_type='count',
                                          titles=None,
                                          type2score_1='labMT_English',
                                          stop_lens=[(4, 6)],
                                          ax=None,
                                          top_n=50,
                                          timezone="UTC",
                                          lang='en',
                                          ):
        """ compare a counter to the entire time range

        Args:

            date1: date1 to select correct counter
        """
        type2score = load_happs_scores(lang=lang_dict[lang])

        if date1.tzinfo is None:
            date1 = pytz.timezone(pytz_dict['UTC']).localize(date1)
        # look up index by date
        date1_index = date_index(date1, self.dates)

        # compare total counter array to selected date
        type2freq_1 = counter_to_dict(self.collapse(), count_type)
        type2freq_2 = counter_to_dict(self.collapse(self.counters[date1_index]), count_type)

        # filter
        type2freq_1, type2score_new1, stop_words = filter_by_scores(type2freq_1, type2score, stop_lens)
        type2freq_2, type2score_new2, stop_words = filter_by_scores(type2freq_2, type2score, stop_lens)

        if titles is None:
            titles = [f"Anchor:  {self.anchor} \n ", f"{date1.strftime('%Y-%m-%d')}"]

        try:
            reference_value = get_weighted_score_np(type2freq_1)
            sentiment_shift = general_sentiment_shift(type2freq_1,
                                                      type2freq_2,
                                                      titles=titles,
                                                      type2score_1=type2score_1,
                                                      reference_value=reference_value,
                                                      stop_lens=stop_lens,
                                                      top_n=top_n,
                                                      ax=ax)
            return sentiment_shift
        except ZeroDivisionError:
            print('No ngrams counted for this time rage. Please select another counter, or check your dates')
            pass

    def plot_allotax_shift_vs_collapsed(self,
                                        date1,
                                        count_type='count',
                                        titles=None,
                                        ax=None,
                                        top_n=50,
                                        timezone="UTC",
                                        matlab='matlab',
                                        ):
        """ compare a counter to the entire time range

        Args:

            date1: date1 to select correct counter
        """
        if date1.tzinfo is None:
            date1 = pytz.timezone(pytz_dict['UTC']).localize(date1)
        # look up index by date
        date1_index = date_index(date1, self.dates)

        # compare total counter array to selected date
        type2freq_1 = self.collapse()
        type2freq_2 = self.counters[date1_index]

        if titles is None:
            titles = [f"Anchor:  {self.anchor} \n ", f"{date1.strftime('%Y-%m-%d')}"]

        date_delta = self.dates[1] - self.dates[0]
        tweet_query.ambient_rd_figures.ambient_rank_divergence(type2freq_1,
                                type2freq_2,
                                self.anchor,
                                self.anchor,
                                (self.dates[0], self.dates[-1]),
                                (date1, date1+date_delta),
                                matlab=matlab, )


    def plot_sentiment_ridge(self, stop_lens=[(4, 6)], lens=False):
        """
            
            
        """
        type2score = load_happs_scores()

        if lens:
            type2freq_1 = counter_to_dict(self.collapse(), 'count')
            type2freq_1, type2score, stop_words = filter_by_scores(type2freq_1, type2score, stop_lens)

        sentiment_variance_ridge(self, type2score, self.anchor)


def main():
    logging_init()
    start_time = time.time()
    start_date = datetime(2020, 1, 1)
    end_date = datetime(2022, 1, 25)
    freq = 'D'  # weekly frequency
    dates = pd.date_range(start_date, end_date, freq=freq)
    high_res = True
    anchor = "N95"
    counters = tweet_query.counters.Counters(dates,
                                             anchor,
                                             high_res=high_res,
                                             pth="/home/data/ambient_data/")
    counters.get(save=True, localdb=True, json=False)
    print(f'{anchor}: highres={high_res}')
    print(f"Data acquired in {time.time() - start_time:.2f} s")
    print(counters)

if __name__ == "__main__":
    main()