import datetime
import pandas as pd
from tweet_query.counters import Counters

start_date = datetime.datetime(2020, 1, 1)
end_date = datetime.datetime(2021, 2,28)
freq = 'D' 
dates = pd.date_range(start_date, end_date, freq=freq)

anchor = "#blm"

counters = Counters(dates, anchor)
counters.get(save=True)

counters.aggregate('4W')

counters.plot_sentiment_timeseries()

date1 = datetime.datetime(2020, 6, 1)
date2 = datetime.datetime(2021, 1, 6)
#counters.plot_sentiment_shift_dates(date1, date2,)
