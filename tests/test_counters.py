import os
import unittest
import pandas as pd
import pytz
from datetime import datetime
from tweet_query.counters import Counters
import matplotlib.pyplot as plt


class TweetDB_Testing(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        pth = os.path.dirname(os.path.abspath(__file__))

        self.start_date =datetime(2020, 1, 2)
        self.end_date = datetime(2021, 1, 1)
        self.lang = 'en'
        self.freq = 'D'  # weekly frequency
        self.dates = pd.date_range(self.start_date, self.end_date, freq=self.freq)
        self.high_res = False
        self.anchor = "#blacklivesmatter"

        self.api = Counters(self.dates, self.anchor, high_res=self.high_res, pth=pth, timezone='UTC')


    def test_query(self):
        self.api.query()
        queried_counters = self.api.counters

        self.api.load_json()
        cached_counters = self.api.counters

        print("Queried length: ", len(queried_counters))
        print("Cached length: ", len(cached_counters))
        print("Dates length: ", len(self.api.dates))

        for i,x in enumerate(queried_counters):
            print(self.api.dates[i])
            print(x == cached_counters[i])
        assert queried_counters == cached_counters


    def test_aggregate(self):
        self.api.get()
        self.api.aggregate('W')

        assert len(self.api.counters) == 52


    def test_collaspe(self):
        self.api.get()
        assert len(self.api.collapse()) == 111297


    def test_happs_series(self):
        self.api.get()
        df, df_count = self.api.happs_series(self.anchor)
        assert not df.empty


    def test_sentiment_plot(self):
        self.api.get()
        self.api.aggregate('W')
        self.api.plot_sentiment_timeseries(unsafe=True)
        plt.show()
        plt.close('all')


    def test_plot_sentiment_shift(self):
        self.api.get()
        self.api.aggregate('M')

        est = pytz.timezone("US/Eastern")
        date1 = est.localize(datetime(2020,6,1))
        date2 = est.localize(datetime(2021,6,2))
        self.api.plot_sentiment_shift_dates(
            date1,
            date2
        )
        plt.show()
        plt.close('all')


if __name__ == '__main__':
    unittest.main()
