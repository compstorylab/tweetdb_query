# TweetDB Query

Tools for working with Twitter data stored in MongoDB, with a focus on ambient n-gram timeseries extraction. 

Core class `Counters()` handles:

* Database Connections
* Local Caching
* Query Metadata
* Aggregation
* Timeseries Plotting
* Sentiment Timeseries
* Sentiment Shifts 
* and many more to come...


# Installation


You can install the latest verion by cloning the repo and running
`setup.py` script in your terminal.

Ideally you'll create a new conda environment to contain all this using something like:

```conda create -n tweet_query --channel default --channel conda-forge```
and activate it using:
```conda activate tweet_query```


## Install Development Version




    git clone https://gitlab.com/compstorylab/tweetdb_query.git
    cd tweetdb_query
    pip install -e .

# Connecting to the database

You'll need a vpn connection if you're not on the UVM network to get started. 

Additionally, you'll need an anaconda distribution with pymongo installed

A minimal python script to test the database connection:

```python
from pprint import pprint
from pymongo import MongoClient

def tweet_connect(username, pwd, database='tweets', collection='geotweets'):
    """ Return pymongo database collection object. Set to connect to localhost on port 27016
    :param username:
    :param pwd:
    :param database:
    :param collection:
    :return: pymongo collection object
    """
    client = MongoClient('mongodb://%s:%s@hydra.uvm.edu:27016' % (username, pwd))
    db = client[database]
    tweets = db[collection]
    return tweets


geotweets = tweet_connect('guest', 'roboctopus')
pprint(geotweets.find_one())

```

# Getting started

Most user interactions will revolve around the Counter() class object.

Let's create an instance of that:


```python

import datetime
import pandas as pd
import tweet_query

start_date = datetime.datetime(2020, 1, 1)
end_date = datetime.datetime(2021, 1, 1)
freq = 'D' 
dates = pd.date_range(start_date, end_date, freq=freq)

anchor = "#blacklivesmatter"

counters = tweet_query.counters.Counters(dates, anchor)
counters.get(save=True)
```

The minimal information needed to initialize this class is a date range and an anchor string.
The date range requires with a start date, an end date, and some given frequency. Smaller is better here, because we can always aggregate later. Choose from {'D', 'H', 'M'}.

The anchor is the keyword used to match tweets. This could be any string, including higher order n-grams. 
E.g.`"Black Lives Matter"` will parse all tweets containing this phrase.

Then we can initialize the class, which assigns these attributes, and call the get method.
`get()` will query the remote database if you haven't searched for this anchor on this date range, but if you have previously it will load from a local cache.

## Aggregation

A common operation is to aggregate n-gram counters to longer date intervals. This operation is a simple class method which just takes a string to specify the datetime frequency.

```python
counter.aggregate('W')
```

The new aggregated counters will be returned in-place.

## Exporting data

The n-gram counters are a good minimal data structure, but we can also easily export data to Pandas Series or DataFrames. 

For single ambient timeseries, use the `counters.to_series()` method to return a pandas Series.

**E.g.**
```python
counters.to_series(word='#blm', count_type='freq')
```

**Expected Output**

|                     |      #blm |
|:--------------------|----------:|
| 2020-01-05 00:00:00 | 0.0225989 |
| 2020-01-12 00:00:00 | 0.0291262 |
| 2020-01-19 00:00:00 | 0.0188679 |
| 2020-01-26 00:00:00 | 0.0143678 |
| 2020-02-02 00:00:00 | 0.029703  |

To return a large number of timeseries as a DataFrame, use `counters.to_dataframe()`. 

**E.g.**
```python
counters.to_dataframe(count_type='freq', N=1000)
```
Specify how many of the most used words you want to return. You could return all by passing `N=-1`, but this is not recommended.

**Expected Output**

|                     |          . |        the |          , |      #blm |
|:--------------------|-----------:|-----------:|-----------:|----------:|
| 2020-01-05 00:00:00 | 0.0282486  | 0.0225989  | 0.0225989  | 0.0225989 |
| 2020-01-12 00:00:00 | 0.0291262  | 0.0145631  | 0.0145631  | 0.0291262 |
| 2020-01-19 00:00:00 | 0.0503145  | 0.0125786  | 0.0314465  | 0.0188679 |
| 2020-01-26 00:00:00 | 0.0287356  | 0.0431034  | 0.0229885  | 0.0143678 |
| 2020-02-02 00:00:00 | 0.049505   | 0.019802   | 0.019802   | 0.029703  |
| 2020-02-09 00:00:00 | 0.0617284  | 0.0277778  | 0.0277778  | 0.0216049 |

## Visualization

A few core visualizations are implimented as method classes.

### Sentiment 
To plot sentiment over time, use 
```python
counters.plot_sentiment_timeseries()
```

To plot a sentiment shift between two date ranges, use:

```python
date1 = datetime.datetime(2020,12,10)
date2 = datetime.datetime(2021,1,6)
counters.plot_sentiment_shift_dates(date1, date2)
```
